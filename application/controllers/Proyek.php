<?php

    /******************************************
    *      Codeigniter 3 Simple Login         *
    *   Developer  :  rudiliucs1@gmail.com    *
    *        Copyright © 2017 Rudi Liu        *
    *******************************************/

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Proyek extends CI_Controller {

    public function __Construct() {
        parent::__Construct();
        if(!$this->session->userdata('logged_in')) {
            redirect(base_url());
        }

        // if($this->session->userdata('roleid') != '1'){
        //     redirect(base_url());
        // }

        $this->load->model('proyek_model');
    }
    

    private function ajax_checking(){
        if (!$this->input->is_ajax_request()) {
            redirect(base_url());
        }
    }

    public function proyek_list(){

        $data = array(
            'formTitle' => 'Proyek Management',
            'title' => 'Proyek Management',
            'proyeks' => $this->proyek_model->get_proyek_list(),
        );

        $this->load->view('frame/header_view');
        $this->load->view('frame/sidebar_nav_view');
        $this->load->view('proyek/proyek_list', $data);

    }

    function ajax_proyek_list(){
        $get_data = $this->proyek_model->ajax_proyek_list();
        for ($i=0; $i < count($get_data); $i++) { 
            $get_data[$i]['projectValid'] = date("d-F-Y", strtotime($get_data[$i]['start_proyek']))." - ".date("d-F-Y", strtotime($get_data[$i]['end_proyek']));
            $get_data[$i]['listTeam'] = 
                        '<button type="button" class="btn btn-block btn-primary btn-sm" data-toggle="modal" data-target="#modal-viewTeam" onclick="getListTeamByProyek('.$get_data[$i]['id'].')">List team</button>';
            $get_data[$i]['action'] = 
                        '<table>
                          <tr>
                            <td><button type="button" class="btn btn-block btn-warning btn-sm" onclick="getDataEdit(\''.$get_data[$i]['id_proyek'].'\')"><i class="glyphicon glyphicon-pencil"></i></button></td>
                            <td><button type="button" class="btn btn-block btn-danger btn-sm" onclick="dataDeletion('.$get_data[$i]['id'].', \''.$get_data[$i]['nama_proyek'].'\')"><i class="glyphicon glyphicon-trash"></i></button></td>
                          </tr>
                        </table>';
        }
        $data = array(
            'row' => count($get_data),
            'listProyek' => $get_data
        );

        echo json_encode($data);
    }

    function ajax_proyek_list_byIdPegawai(){
        $get_data = $this->proyek_model->ajax_proyek_list_byIdPegawai($this->session->userdata('id'));
        $data = array(
            'row' => count($get_data),
            'listProyek' => $get_data
        );

        echo json_encode($data);
    }

    function get_list_project_byApprover(){
        $this->ajax_checking();

        $get_data = $this->proyek_model->get_list_project_byApprover($this->session->userdata('id'));
        for ($i=0; $i < count($get_data); $i++) { 
            $get_data[$i]['action'] = 
                        '<table>
                          <tr>
                            <td><button type="button" class="btn btn-block btn-primary btn-sm" onclick="get_list_team_inProject('.$get_data[$i]['id'].', \''.$get_data[$i]['id_proyek'].'\', \''.$get_data[$i]['nama_proyek'].'\')"> Detail</i></button></td>
                          </tr>
                        </table>';
        }
        $data = array(
            'row' => count($get_data),
            'list_project_byApprover' => $get_data
        );

        echo json_encode($data);
    }

    function ajax_get_proyek_data(){
        $this->ajax_checking();

        $postData = json_decode($this->input->post('sendData'));
        $update = $this->proyek_model->get_proyek_by_id($postData->id_proyek)[0];
        $update['projectValid'] = $update['start_proyek']." - ".$update['end_proyek'];
        $this->load->model('pegawai_model');
        $get_raw_team = $this->pegawai_model->get_pegawai_team_by_idProyek_edit_menu($update['id']);
        $front_end = array();
        $back_end = array();
        $tester = array();
        $business_analyst = array();
        foreach ($get_raw_team as $key) {
            if ($key['idProjek'] == null) {
                $data = array(
                    'id' => $key['id_pegawai'],
                    'text' => $key['nama_pegawai'],
                );
            } else {
                $data = array(
                    'id' => $key['id_pegawai'],
                    'text' => $key['nama_pegawai'],
                    'selected' => true
                );
            }
            if ($key['jabatan'] == "Front End Programmer") {
                array_push($front_end, $data);
            } else if ($key['jabatan'] == "Back End Programmer"){
                array_push($back_end, $data);
            } else if ($key['jabatan'] == "Business Analyst"){
                array_push($business_analyst, $data);
            } else if ($key['jabatan'] == "Tester"){
                array_push($tester, $data);
            }
        }
        $update['team'] = array(
            [
                'text' => 'Front End Programmer',
                'children' => $front_end,
            ],
            [
                'text' => 'Back End Programmer',
                'children' => $back_end,
            ],
            [
                'text' => 'Business Analyst',
                'children' => $business_analyst,
            ],
            [
                'text' => 'Tester',
                'children' => $tester,
            ], 
        );

        echo json_encode($update);
    }

    function add_proyek(){
        if($this->session->userdata('roleid') != '1'){
            redirect(base_url());
        }
        $this->ajax_checking();

        $postData = json_decode($this->input->post('sendData'));
        $insert = $this->proyek_model->insert_proyek($postData);
        if($insert['status'] == "success")
            $this->session->set_flashdata('success', 'User '.$postData->id_proyek.' has been successfully created!');

        echo json_encode($insert);
    }

    function ajax_proyek_new(){
        if($this->session->userdata('roleid') != '1'){
            redirect(base_url());
        }
        $this->ajax_checking();

        $postData = json_decode($this->input->post('sendData'));
        $rawValidDate = explode("-", $postData->valid_date);
        $rawStartDate = str_replace('/', '-', $rawValidDate[0]);
        $postData->startProject = date("Y-m-d", strtotime($rawStartDate));
        $rawEndDate = str_replace('/', '-', $rawValidDate[1]);
        $postData->endProject = date("Y-m-d", strtotime($rawEndDate));
        $postData->id_proyek = "ATIC-PROJEK-".$this->proyek_model->get_last_id($postData)['id'];
        $insert = $this->proyek_model->insert_proyek($postData);

        echo json_encode($insert);
    }

    function ajax_proyek_update(){
        if($this->session->userdata('roleid') != '1'){
            redirect(base_url());
        }
        $this->ajax_checking();

        $postData = json_decode($this->input->post('sendData'));
        $rawValidDate = explode("-", $postData->valid_date);
        $rawStartDate = str_replace('/', '-', $rawValidDate[0]);
        $postData->startProject = date("Y-m-d", strtotime($rawStartDate));
        $rawEndDate = str_replace('/', '-', $rawValidDate[1]);
        $postData->endProject = date("Y-m-d", strtotime($rawEndDate));
        // $postData->id_proyek = "ATIC-PROJEK-".$this->proyek_model->get_last_id($postData)['id'];
        $insert = $this->proyek_model->update_proyek($postData);

        echo json_encode($insert);
    }

    function update_proyek_details(){
        if($this->session->userdata('roleid') != '1'){
            redirect(base_url());
        }
        $this->ajax_checking();

        $postData = $this->input->post();
        $update = $this->proyek_model->update_proyek_details($postData);
        if($update['status'] == 'success')
            $this->session->set_flashdata('success', 'User '.$postData['id_proyek'].'`s details have been successfully updated!');

        echo json_encode($update);
    }

    function deactivate_proyek($id_proyek,$id){
        if($this->session->userdata('roleid') != '1'){
            redirect(base_url());
        }
        $this->ajax_checking();

        $update = $this->proyek_model->deactivate_proyek($id_proyek,$id);
        if($update['status'] == 'success')
            $this->session->set_flashdata('success', 'User '.$id_proyek.' has been successfully deleted!');

        echo json_encode($update);
    }

    function ajax_deactivate_proyek(){
        if($this->session->userdata('roleid') != '1'){
            redirect(base_url());
        }
        $this->ajax_checking();
        $postData = json_decode($this->input->post('sendData'));

        $update = $this->proyek_model->ajax_deactivate_proyek($postData);

        echo json_encode($update);
    }

    // function reset_user_password($email,$id){
    //     $this->ajax_checking();

    //     $update = $this->admin_model->reset_user_password($email,$id);
    //     if($update['status'] == 'success')
    //         $this->session->set_flashdata('success', 'User '.$email.'`s password has been successfully reset!');

    //     echo json_encode($update);
    // }

    // function activity_log(){
    //     $data = array(
    //         'formTitle' => 'Activity Log',
    //         'title' => 'Activity Log',
    //     );
    //     $this->load->view('frame/header_view');
    //     $this->load->view('frame/sidebar_nav_view');
    //     $this->load->view('admin/activity_log', $data);

    // }

    // function get_activity_log(){
    //     $this->ajax_checking();
    //     echo  json_encode( $this->admin_model->get_activity_log() );
    // }

//  public function pegawai_list(){

//         $data = array(
//             'formTitle' => 'Pegawai Management',
//             'title' => 'Pegawai Management',
//             'pegawais' => $this->admin_model->get_pegawai_list(),
//         );

//         $this->load->view('frame/header_view');
//         $this->load->view('frame/sidebar_nav_view');
//         $this->load->view('admin/pegawai_list', $data);

//     }

}

/* End of file */
