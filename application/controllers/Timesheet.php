<?php

    /******************************************
    *      Codeigniter 3 Simple Login         *
    *   Developer  :  rudiliucs1@gmail.com    *
    *        Copyright © 2017 Rudi Liu        *
    *******************************************/

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Timesheet extends CI_Controller {

    public function __Construct() {
        parent::__Construct();
        if(!$this->session->userdata('logged_in')) {
            redirect(base_url());
        }

        if($this->session->userdata('roleid') != '2'){
            redirect(base_url());
        }
        $this->load->model('timesheet_model', 'timesheet');
    }

    private function ajax_checking(){
        if (!$this->input->is_ajax_request()) {
            redirect(base_url());
        }
    }

    function get_status_weekly_timesheet()
    {
        $this->ajax_checking();
        $postData = json_decode($this->input->post('sendData'));
        // $status = $this->timesheet->get_weekly_timesheet_byIdWeeklyTimehseet($postData->idTimesheetWeek)[0];
        // echo json_encode($status);
        $get_inside_week_timesheet = $this->timesheet->get_timesheet_detail_list($postData->idTimesheetWeek);
        $get_status_week_timesheet = $this->timesheet->get_weekly_timesheet_byIdWeeklyTimehseet($postData->idTimesheetWeek)[0]['status'];
        $stats = 'none';
        $approved_count = 0;
        $count_inside_week_timesheet = count($get_inside_week_timesheet);
        if($get_status_week_timesheet == 1){
            foreach ($get_inside_week_timesheet as $key) {
                if ($key['status'] == 3){
                    $stats = 'reject';
                    break;
                } else if ($key['status'] == 1){
                    $stats = 'submited';
                    break;
                } else if ($key['status'] == 2){
                    $approved_count++;
                }
                // cek approved all
                if ($approved_count === $count_inside_week_timesheet){
                    $stats = 'approved';
                }
            }
        }
        $data = array(
                    'status' => $stats,
                );
        echo json_encode($data);
    }

    function get_list_weekly_timesheet() {
        $this->load->model('pegawai_model', 'pegawai');
        $id_pegawai = $this->session->userdata('id'); 
        // must contain array with index listWeeklyTimesheet
        $get_data = $this->timesheet->get_list_weekly_timesheet($id_pegawai);
        $this->load->helper('url');
        for ($i=0; $i < count($get_data); $i++) { 
            $get_inside_week_timesheet = $this->timesheet->get_timesheet_detail_list($get_data[$i]['id']);
            $stats = 'none';
            $approved_count = 0;
            $count_inside_week_timesheet = count($get_inside_week_timesheet);
            foreach ($get_inside_week_timesheet as $key) {
                if ($key['status'] == 3){
                    $stats = 'reject';
                    break;
                } else if ($key['status'] == 2){
                    $approved_count++;
                }
                // cek approved all
                if ($approved_count === $count_inside_week_timesheet){
                    $stats = 'approved';
                }
            }

            $start = date('d-m-Y', strtotime($get_data[$i]['start_date']));
            $end = date('d-m-Y', strtotime($get_data[$i]['end_date']));
            $get_data[$i]['periode'] = date('d-F-Y', strtotime($get_data[$i]['start_date']))." - ".date('d-F-Y', strtotime($get_data[$i]['end_date']));
            if ($get_data[$i]['status'] == 0) {
                $get_data[$i]['action'] = '<a href="'.base_url('dashboard/timesheet_detail/'.$get_data[$i]['id']).'/'.$start.'/'.$end.'"><button type="button" class="btn btn-block btn-sm">Edit Timesheet</button></a>';
            // } else if ($get_data[$i]['status'] == 2){
            } else if ($stats == 'approved'){
                $get_data[$i]['action'] = '<a href="'.base_url('dashboard/timesheet_detail/'.$get_data[$i]['id']).'/'.$start.'/'.$end.'"><button type="button" class="btn btn-block btn-success btn-sm">Approved</button></a>';
            // } else if ($get_data[$i]['status'] == 3) {
            } else if ($stats == 'reject') {
                $get_data[$i]['action'] = '<a href="'.base_url('dashboard/timesheet_detail/'.$get_data[$i]['id']).'/'.$start.'/'.$end.'"><button type="button" class="btn btn-block btn-danger btn-sm">Rejected</button></a>';
            } else if ($get_data[$i]['status'] == 1){
                $get_data[$i]['action'] = '<a href="'.base_url('dashboard/timesheet_detail/'.$get_data[$i]['id']).'/'.$start.'/'.$end.'"><button type="button" class="btn btn-block btn-primary btn-sm">Submited</button></a>';
            }
        }
        $return = array(
                        'listWeeklyTimesheet' => $get_data , 
                );
        echo json_encode($return);
    }

    // new
    function get_list_week_timesheet_pegawai_proyek($id_pegawai, $id_proyek) {
        $get_data = $this->timesheet->get_list_weekly_timesheet_byidPegawai_idProyek($id_pegawai, $id_proyek);
        if ($get_data == null) {
            $get_data['periode'] = null;
            $get_data['action'] = null;
        } else {
            $this->load->model('pegawai_model', 'pegawai');
            $get_data_pegawai = $this->pegawai->get_pegawai_by_id_pegawai($id_pegawai)[0];
            for ($i=0; $i < count($get_data); $i++) { 
                if ($get_data[$i]['status'] == 0) {
                    unset($get_data[$i]);
                } else {
                    $get_data[$i]['nama_pegawai'] = $get_data_pegawai['nama_pegawai'];
                    $get_data[$i]['nik'] = $get_data_pegawai['nik'];
                    $start = date('d-m-Y', strtotime($get_data[$i]['start_date']));
                    $end = date('d-m-Y', strtotime($get_data[$i]['end_date']));
                    $get_data[$i]['periode'] = date('d-F-Y', strtotime($get_data[$i]['start_date']))." - ".date('d-F-Y', strtotime($get_data[$i]['end_date']));
                    // dashboard/apporver_timesheet_week_day_detail/id_pegawai/id_proyek/id_week_timesheet/start/end).'
                    $get_data[$i]['action'] = '<a href="'.base_url('dashboard/apporver_timesheet_week_day_detail/'.$id_pegawai.'/'.$id_proyek.'/'.$get_data[$i]['id'].'/'.$start.'/'.$end).'"><button type="button" class="btn btn-primary">Detail Timesheet</button></a>';
                }
            }
        }
        $return = array(
                        'list_week_timesheet_pegawai_proyek' => $get_data , 
                );
        echo json_encode($return);
    }

    function get_timesheet_detail_list($id_week_timesheet) {
        $this->ajax_checking();
        $this->load->model('pegawai_model', 'pegawai');
        $get_data = $this->timesheet->get_timesheet_detail_list($id_week_timesheet);
        for ($i=0; $i < count($get_data) ; $i++) { 
            if ($get_data[$i]['category'] == 1) {
                $get_data[$i]['category'] = "Billable";
            } else if ($get_data[$i]['category'] == 3){
                $get_data[$i]['category'] = "Sick";
            } else if ($get_data[$i]['category'] == 4){
                $get_data[$i]['category'] = "Permission";
            } else if ($get_data[$i]['category'] == 2){
                $get_data[$i]['category'] = "Non Billable";
            }

            if ($get_data[$i]['stage'] == 1) {
                $get_data[$i]['stage'] = "Development";
            } else if ($get_data[$i]['stage'] == 2){
                $get_data[$i]['stage'] = "Document";
            } else if ($get_data[$i]['stage'] == 3){
                $get_data[$i]['stage'] = "Maintenance";
            } else if ($get_data[$i]['stage'] == 4){
                $get_data[$i]['stage'] = "Meeting";
            } else if ($get_data[$i]['stage'] == 5){
                $get_data[$i]['stage'] = "Leave";
            }
            // action button
            $reason = '';
            $edit = '';
            $delete = '';
            $button = '';
            if ($get_data[$i]['status'] == 3) {
                $reason = '<td><button type="button" class="btn btn-block btn-warning btn-sm" onclick="getReason('.$get_data[$i]['id'].')">Reason</button></td>';
                $edit = '<td><button type="button" class="btn btn-block btn-primary btn-sm" onclick="getDataEdit('.$get_data[$i]['id'].')">Edit</button></td>';
                $delete = '<td><button type="button" class="btn btn-block btn-danger btn-sm" onclick="dataDeletion('.$get_data[$i]['id'].')"><i class="glyphicon glyphicon-trash"></i></button></td>';
            }
            
            if ($get_data[$i]['status'] == 0) {
                $edit = '<td><button type="button" class="btn btn-block btn-primary btn-sm" onclick="getDataEdit('.$get_data[$i]['id'].')">Edit</button></td>';
                $delete = '<td><button type="button" class="btn btn-block btn-danger btn-sm" onclick="dataDeletion('.$get_data[$i]['id'].')"><i class="glyphicon glyphicon-trash"></i></button></td>';
            }
            
            if ($get_data[$i]['status'] == 1) {
                $button = '<td><button type="button" class="btn btn-block btn-primary btn-sm">Submited</button></td>';
            }

            if ($get_data[$i]['status'] == 2) {
                $button = '<td><button type="button" class="btn btn-block btn-success btn-sm">Approved</button></td>';
            }

            if ($get_data[$i]['id_project'] == -99) {
                $get_data[$i]['action'] = 
                        '<table>
                          <tr></tr>
                        </table>';
            } else {
                $get_data[$i]['action'] = 
                            '<table>
                              <tr>
                                '.$edit.$reason.$delete.$button.'
                              </tr>
                            </table>';
            }
            
        }
        $return = array(
                'row' => count($get_data),
                'list_week_timesheet' => $get_data, 
            );
        echo json_encode($return);
    }

    function get_reason_by_timesheet_day($id_day_timesheet) {
        $this->ajax_checking();
        $get_data = $this->timesheet->get_reason_by_timesheet_day($id_day_timesheet)[0];
        $return = array(
                'reason' => $get_data['reason']
            );
        echo json_encode($return);
    }

    // for approver
    function get_timesheet_detail_list_approver($id_week_timesheet, $id_pegawai, $id_proyek) {
        $this->ajax_checking();
        $this->load->model('pegawai_model', 'pegawai');
        $get_data = $this->timesheet->get_timesheet_detail_list_approver($id_week_timesheet, $id_pegawai, $id_proyek);
        for ($i=0; $i < count($get_data) ; $i++) { 
            if ($get_data[$i]['category'] == 1) {
                $get_data[$i]['category'] = "Billable";
            } else if ($get_data[$i]['category'] == 3){
                $get_data[$i]['category'] = "Sick";
            } else if ($get_data[$i]['category'] == 4){
                $get_data[$i]['category'] = "Permission";
            } else if ($get_data[$i]['category'] == 2){
                $get_data[$i]['category'] = "Non Billable";
            }

            if ($get_data[$i]['stage'] == 1) {
                $get_data[$i]['stage'] = "Development";
            } else if ($get_data[$i]['stage'] == 2){
                $get_data[$i]['stage'] = "Document";
            } else if ($get_data[$i]['stage'] == 3){
                $get_data[$i]['stage'] = "Maintenance";
            } else if ($get_data[$i]['stage'] == 4){
                $get_data[$i]['stage'] = "Meeting";
            } else if ($get_data[$i]['stage'] == 5){
                $get_data[$i]['stage'] = "Leave";
            }
            // action button
            $button='';
            if ($get_data[$i]['status'] == 2) {
                $button .= '<td><button type="button" class="btn btn-block btn-success btn-sm" disabled> Approved</button></td>';
            } else if ($get_data[$i]['status'] == 3){
                $button .= '<td><button type="button" class="btn btn-block btn-danger btn-sm" disabled> Rejected</button></td>';
            } else {
                $button .= '<td><button type="button" class="btn btn-block btn-success btn-sm" onclick="approveDayTimesheet('.$get_data[$i]['id'].', '.$get_data[$i]['id_timesheet_week'].')"> Approve</button></td>'; 
                $button .= '<td><button type="button" class="btn btn-block btn-danger btn-sm" onclick="rejectDayTimesheet('.$get_data[$i]['id'].', '.$get_data[$i]['id_timesheet_week'].')"> Reject</button></td>'; 
            }
            

            $get_data[$i]['action'] = 
                        '<table>
                          <tr>
                            '.$button.'
                          </tr>
                        </table>';
            
        }
        $return = array(
                'row' => count($get_data),
                'list_detail_timesheet_approver' => $get_data, 
            );
        echo json_encode($return);
    }

    function get_timesheet_detail($id_day_timesheet) {
        $this->ajax_checking();
        $get_data = $this->timesheet->get_timesheet_detail($id_day_timesheet)[0];
        $return = array(
                'row' => count($get_data),
                'list_week_timesheet' => $get_data, 
            );
        echo json_encode($return);
    }
    
    
    function check_timesheet_byIdPegawai () {
        $this->ajax_checking();
        $this->load->model('pegawai_model', 'pegawai');
        //get id pegawai by data id in session
        $id_pegawai = $this->session->userdata('id');  
        // get data from model timesheet
        if ($this->timesheet->check_last_timesheet($id_pegawai)) {
            $get_data = array('status' => 'negative', 'message' => '<div class=row>
                <div class="col-md-4 pull-left">
                  <strong>Minggu ini anda belum membuat timesheet mingguan, buat?</strong>
                </div>
                <div class="col-md-1 pull-left">
                  <button type="button" class="btn btn-block btn-warning btn-sm" onclick="goCreateWeeklyTimesheet()"><i class="glyphicon glyphicon-pencil"></i></button>                  
                </div>
              </div>');
        } else {
            $get_data = array('status' => 'positive', );
        }

        echo json_encode($get_data);
    }

    function create_weekly_timesheet (){
        $this->ajax_checking();
        $this->load->model('timesheet_model', 'timesheet');
        $this->load->model('pegawai_model', 'pegawai');
        $id_pegawai = $this->session->userdata('id'); 
        //get id pegawai by data id in session
        $get_data = $this->timesheet->add_weekly_timesheet($id_pegawai);

        echo json_encode($get_data);

    }

    function ajax_add_day_timesheet (){
        $this->ajax_checking();
        $postData = json_decode($this->input->post('sendData'));
        $postData->id_pegawai = $this->session->userdata('id'); 
        $get_data = $this->timesheet->ajax_add_day_timesheet($postData);

        echo json_encode($get_data);

    }

    function ajax_add_day_timesheet_fromImport ($postData){
        $postData->id_pegawai = $this->session->userdata('id'); 
        $get_data = $this->timesheet->ajax_add_day_timesheet_fromCutiImport($postData);

        return "success";

    }

    function ajax_update_day_timesheet (){
        $this->ajax_checking();
        $postData = json_decode($this->input->post('sendData'));
        $this->load->model('pegawai_model', 'pegawai');
        $postData->id_pegawai = $this->session->userdata('id'); 
        $get_data = $this->timesheet->ajax_update_day_timesheet($postData);

        echo json_encode($get_data);

    }

    function ajax_delete_record_timesheetDay()
    {
        $this->ajax_checking();
        $postData = json_decode($this->input->post('sendData'));
        $get_data = $this->timesheet->ajax_delete_day_timesheet($postData);
        echo json_encode($get_data);
    }

    function submit_week_timesheet()
    {
        $this->ajax_checking();
        $postData = json_decode($this->input->post('sendData'));
        $get_data = $this->timesheet->ajax_submit_week_timesheet($postData);
        echo json_encode($get_data);
    }
    
    function approve_day_timesheet()
    {
        $this->ajax_checking();
        $postData = json_decode($this->input->post('sendData'));
        $result = $this->timesheet->approve_day_timesheet($postData);
        echo json_encode($result);
    }

    function reject_day_timesheet()
    {
        $this->ajax_checking();
        $postData = json_decode($this->input->post('sendData'));
        $result = $this->timesheet->reject_day_timesheet($postData);
        echo json_encode($result);
    }

    function ajax_import_leave(){
        $this->load->model('cuti_model', 'cuti');
        $this->ajax_checking();
        $postData = json_decode($this->input->post('sendData'));
        $postData->id_pegawai = $this->session->userdata('id');
        $get_timesheet_cuti = $this->timesheet->get_cuti_timesheet($postData);
        foreach ($get_timesheet_cuti as $key) {
            $createTimesheetDay = new stdClass();
            $createTimesheetDay->idTimesheetWeek = $postData->idTimesheetWeek;
            $createTimesheetDay->id_pegawai = $key['id_pegawai'];
            $createTimesheetDay->pid = -99;
            $createTimesheetDay->category = $key['category'];
            $createTimesheetDay->stage = $key['stage'];
            $createTimesheetDay->tanggal = $key['date'];
            $createTimesheetDay->task = $key['task'];
            $createTimesheetDay->output = $key['output'];
            $createTimesheetDay->startTime = $key['start_time'];
            $createTimesheetDay->endTime = $key['end_time'];

            $this->ajax_add_day_timesheet_fromImport($createTimesheetDay);

            $this->cuti->delete_cuti_timesheet($key['id']);
        }

        $return = array('status' => 'success', 'message' => 'Leave has been deleted');
        echo json_encode($return);
    }


}

/* End of file */
