<?php

    /******************************************
    *      Codeigniter 3 Simple Login         *
    *   Developer  :  rudiliucs1@gmail.com    *
    *        Copyright © 2017 Rudi Liu        *
    *******************************************/

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Authentication extends CI_Controller {

    public function __Construct() {
        parent::__Construct();
        $this->load->model("authentication_model");
    }

    public function index() {
        
        if($this->session->userdata('logged_in')) {
            redirect(base_url("dashboard"));
        }else {
            $data = array('alert' => false);
            $this->load->view('v2/login',$data);
        }
    }

    private function ajax_checking(){
        if (!$this->input->is_ajax_request()) {
            redirect(base_url());
        }
    }

    public function login(){
        $postData = $this->input->post();
        $validate = $this->authentication_model->validate_login($postData);
        if ($validate){
            $newdata = array(
                'username'     => $validate[0]->username,
                // 'nama_pegawai' => $validate[0]->nama_pegawai,
                'roleid' => $validate[0]->roleid,
                'id' => $validate[0]->id_pegawai,
                'logged_in' => TRUE,
              
            );
            $this->session->set_userdata($newdata);
            if ($validate[0]->roleid == 1) {
                // redirect(base_url("dashboard/admin")); 
                redirect(base_url("dashboard"));
            } else if ($validate[0]->roleid == 2) {
                // add function get jabatan
                $this->load->model("pegawai_model", "pegawai");
                $get_jabatan = $this->pegawai->get_pegawai_by_id_user($validate[0]->id_pegawai)[0];
                redirect(base_url("dashboard"));
                // if ($get_jabatan['jabatan'] != 'Project Manager') {
                //     redirect(base_url("dashboard/pegawai"));
                // } else {
                //     redirect(base_url("dashboard/timesheet_approval"));
                // }
                
            } else {
                echo "error";
            }
        }
        else{
            $data = array('alert' => true);
            $this->load->view('v2/login',$data);
        }
     
    }

    function change_password(){
        $this->ajax_checking();

        $postData = $this->input->post();
        $update = $this->authentication_model->change_password($postData);
        if($update['status'] == 'success')
            $this->session->set_flashdata('success', 'Your password has been successfully changed!');

        echo json_encode($update);
    }

    public function logout() {
        $this->session->sess_destroy();
        redirect(base_url());
    }


}

/* End of file */
