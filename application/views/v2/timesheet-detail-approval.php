<!DOCTYPE html>

<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Application Anabatic</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/dist/css/AdminLTE.min.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect. -->
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/dist/css/skins/skin-blue.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/bower_components/select2/dist/css/select2.min.css">
  <!-- clockpicker -->
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/dist/clockpicker/bootstrap-clockpicker.min.css">

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>


<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <!-- Main Header -->
  <header class="main-header">
    <!-- Logo -->
    <a href="<?=base_url()?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><img class="logoAnabatic" src="<?=base_url()?>assets/web-v2/dist/img/logo-anabatic-lg.png"></img></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Anabatic</b> APP</span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <li><a href="#"><span class="h4">Access Level : <?php echo $role_string; ?></span></a></li>
        </ul>
      </div>

    </nav>
  
  </header>

  <input type="hidden"  id="base-url" value="<?=base_url()?>"/>
  <input type="hidden"  id="id_week_timesheet" value="<?php echo $id_week_timesheet; ?>"/>
  <input type="hidden"  id="id_pegawai" value="<?php echo $id_pegawai; ?>"/>
  <input type="hidden"  id="id_proyek" value="<?php echo $id_proyek; ?>"/>
  <input type="hidden"  id="start_date" value="<?php echo $start_date; ?>"/>
  <input type="hidden"  id="end_date" value="<?php echo $end_date; ?>"/>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar Menu -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MENU</li>
        <li class="active"><a href="#"><i class="glyphicon glyphicon-list-alt"></i> <span>Approval Timesheet</span></a></li>
        <li><a href="<?=base_url("dashboard/cuti_approval")?>"><i class="glyphicon glyphicon-plane"></i> <span>Approval Cuti</span></a></li>
        <li class="treeview">
          <a href="#">
            <i class="glyphicon glyphicon-pencil"></i>
            <span>Ubah Data</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url("dashboard/edit_detailPegawai")?>"><i class="glyphicon glyphicon-list-alt"></i> Data Detail</a></li>
            <li><a href="<?=base_url("dashboard/edit_passwordPegawai")?>"><i class="glyphicon glyphicon-cog"></i> Password</a></li>
          </ul>
        </li>
        <li><a href="<?=base_url("authentication/logout")?>"><i class="glyphicon glyphicon-log-out"></i> <span>Logout</span></a></li>
       
      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>

  <!--------------------------
    | yhouga content |
  -------------------------->

 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><small></small></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">
      <!--------------------------
        | Alert |
      -------------------------->
      <div class="row">
        <div class="col-md-12">
          <div class="alert alert-success" id="alert-success" style="display: none;">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong id="success-response"></strong>
          </div>
          <div class="alert alert-warning" id="alert-warning" style="display: none;">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong id="warning-response"></strong>
          </div>
        </div>
      </div>
      <!--------------------------
        | Your Page Content Here |
        -------------------------->
      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-body">
              <div class="col-md-12">
                <div class="box-body">
                  <div class="row">
                    <div class="col-md-12">
                      <table style="text-align: center;" id="list-week-timesheet" class="table table-bordered table-hover">
                        <thead>
                          <tr>
                            <th>Date</th>
                            <th>Category</th>
                            <th>PID</th>
                            <th>Project</th>
                            <th>Stage</th>
                            <th style="width: 15%">Task</th>
                            <th style="width: 15%">Output</th>
                            <th>Duration</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                      </table> 
                    </div>
                    <div class="col-md-4">
                        <a href="<?php echo base_url('dashboard/apporver_timesheet_week_detail/'.$id_pegawai.'/'.$id_proyek); ?>"><button type="button" class="btn btn-primary">Back</button></a>
                    </div>
                  </div>
                </div>
              </div>

              <!-- MODAL PEGAWAI -->
              <div class="modal fade" id="modal-reason">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-body">
                        <form role="form" id="addPegawai">
                          <input type="hidden"  id="reason-idTimesheetDetail" value=""/>
                          <input type="hidden"  id="reason-idTimesheetWeek" value=""/>
                          <div class="box-body">
                            <div class="form-group col-md-12">
                              <label for="label-reason">Reason to reject</label>
                              <textarea id="input-reason" class="form-control" rows="2" placeholder="Reason"></textarea>
                            </div>
                          </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                      <div class="form-group col-md-4 ">
                        <button type="button" class="btn btn-success pull-left" id="submit-reason">Submit</button>
                      </div>       
                    </div>
                  </div>
                  <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
              </div>
              <!-- /.END MODAL PEGAWAI -->

            </div>
          </div>  
        </div>
      </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
      Anything you want
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2019 <a href="#">Company</a>.</strong> All rights reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane active" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:;">
              <i class="menu-icon fa fa-birthday-cake bg-red"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                <p>Will be 23 on April 24th</p>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:;">
              <h4 class="control-sidebar-subheading">
                Custom Template Design
                <span class="pull-right-container">
                    <span class="label label-danger pull-right">70%</span>
                  </span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">General Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Report panel usage
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Some information about this general settings option
            </p>
          </div>
          <!-- /.form-group -->
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
  immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 3 -->
<script src="<?=base_url()?>assets/web-v2/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?=base_url()?>assets/web-v2/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="<?=base_url()?>assets/web-v2/dist/js/adminlte.min.js"></script>
<!-- Select2 -->
<script src="<?=base_url()?>assets/web-v2/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- clockpicker -->
<script type="text/javascript" src="<?=base_url()?>assets/web-v2/dist/clockpicker/bootstrap-clockpicker.min.js"></script>
<!-- DataTables -->
<script src="<?=base_url()?>assets/web-v2/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>assets/web-v2/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- ADDITIONAL JS -->
<!-- bootstrap datepicker -->
<script src="<?=base_url()?>assets/web-v2/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>

<script type="text/javascript">
 var table = $('#list-week-timesheet').DataTable({
       ajax:  {
          url: $("#base-url").val() + "timesheet/get_timesheet_detail_list_approver/"+$('#id_week_timesheet').val()+"/"+$('#id_pegawai').val()+"/"+$('#id_proyek').val(),
          dataSrc: 'list_detail_timesheet_approver'     
       }, 
       columns: [
        { data: "date" },
        { data: "category" },
        { data: "id_proyek" },
        { data: "nama_proyek" },
        { data: "stage" },
        { data: "output" },
        { data: "task" },
        { data: "total_hours" },
        { data: "action" },
       ] 
  })

 function approveDayTimesheet(idTimesheetDetail, idTimesheetWeek) {
   $.ajax({
        url: $("#base-url").val() + "timesheet/approve_day_timesheet",
        traditional: true,
        type: "post",
        dataType: "text", 
        data: {sendData : JSON.stringify({
                              idTimesheetDetail:idTimesheetDetail, 
                              idTimesheetWeek:idTimesheetWeek
                            })
              },
        success: function (hasil) {
          var result = JSON.parse(hasil);
          if(result.status=="success"){
              table.ajax.reload();
          }
          else{
              alert("Oops there is something wrong!");
          }
        }
      })
 }

 $('#submit-reason').click(function () {
      var idTimesheetDetail = $("#reason-idTimesheetDetail").val();
      var idTimesheetWeek = $("#reason-idTimesheetWeek").val();
      var reason = $("#input-reason").val();
      $('#modal-reason').modal("toggle");
      $.ajax({
        url: $("#base-url").val() + "timesheet/reject_day_timesheet",
        traditional: true,
        type: "post",
        dataType: "text", 
        data: {sendData : JSON.stringify({
                              idTimesheetDetail:idTimesheetDetail, 
                              idTimesheetWeek:idTimesheetWeek,
                              reason:reason
                            })
              },
        success: function (hasil) {
          var result = JSON.parse(hasil);
          if(result.status=="success"){
              table.ajax.reload();
          }
          else{
              alert("Oops there is something wrong!");
          }
        }
      })
 })

 function rejectDayTimesheet(idTimesheetDetail, idTimesheetWeek) {
    $("#reason-idTimesheetDetail").val(idTimesheetDetail);
    $("#reason-idTimesheetWeek").val(idTimesheetWeek);
    $('#modal-reason').modal("toggle");
 }
  

</script>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. -->
</body>
</html>