<!DOCTYPE html>

<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Application Anabatic</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/dist/css/AdminLTE.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/bower_components/select2/dist/css/select2.min.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect. -->
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/dist/css/skins/skin-blue.min.css">

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>


<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <!-- Main Header -->
  <header class="main-header">
    <!-- Logo -->
    <a href="<?=base_url()?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><img class="logoAnabatic" src="<?=base_url()?>assets/web-v2/dist/img/logo-anabatic-lg.png"></img></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Anabatic</b> APP</span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <li><a href="#"><span class="h4">Access Level : <?php echo $role_string; ?></span></a></li>
        </ul>
      </div>

    </nav>
  
  </header>

  <input type="hidden"  id="base-url" value="<?=base_url()?>"/>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar Menu -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MENU</li>
        <li><a href="<?=base_url("dashboard/pegawai")?>"><i class="glyphicon glyphicon-list-alt"></i> <span>Timesheet</span></a></li>
        <li class="active"><a href="#"><i class="glyphicon glyphicon-plane"></i> <span>Cuti</span></a></li>
        <li class="treeview">
          <a href="#">
            <i class="glyphicon glyphicon-pencil"></i>
            <span>Ubah Data</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url("dashboard/edit_detailPegawai")?>"><i class="glyphicon glyphicon-list-alt"></i> Data Detail</a></li>
            <li><a href="<?=base_url("dashboard/edit_passwordPegawai")?>"><i class="glyphicon glyphicon-cog"></i> Password</a></li>
          </ul>
        </li>
        <li><a href="<?=base_url("authentication/logout")?>"><i class="glyphicon glyphicon-log-out"></i> <span>Logout</span></a></li>
       
      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>

  <!--------------------------
    | yhouga content |
  -------------------------->

 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><small></small></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">
       <!--------------------------
          | Alert |
        -------------------------->
        <div class="row">
          <div class="col-md-12">
            <div class="alert alert-success" id="alert-success" style="display: none;">
              <a href="#" class="close" data-dismiss="alert">&times;</a>
              <strong id="success-response"></strong>
            </div>
            <div class="alert alert-warning" id="alert-warning" style="display: none;">
              <a href="#" class="close" data-dismiss="alert">&times;</a>
              <strong id="warning-response"></strong>
            </div>
          </div>
        </div>
      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-body">
              <h2 style="text-align: center;">
                Daftar Cuti
              </h2>
            </div>
            <div class="box-body">
              <div class="col-md-12">
                <div class="box-body">
                  <button type="button" class="btn btn-primary" id="add-cuti" data-toggle="modal" data-target="#modal-addLeave">Tambah </button>
                  <table style="text-align: center;" id="cuti-table" class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th style="text-align: center;">Leave ID</th>
                        <th style="text-align: center;">Employe ID</th>
                        <th style="text-align: center;">Name</th>
                        <th style="text-align: center;">Project</th>
                        <th style="text-align: center;">Leave Start</th>
                        <th style="text-align: center;">Leave End</th>
                        <th style="text-align: center;">Total Day</th>
                        <th style="text-align: center;">Back To Work At</th>
                        <th style="text-align: center;">Status</th>
                        <th style="text-align: center;">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table> 
                </div>
              </div>

              <!-- MODAL PROJEK -->
              <div class="modal fade" id="modal-addLeave">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title">Add Leave Form</h4>
                    </div>
                    <div class="modal-body">
                      <div class="row">
                         <div class="col-md-3"></div>
                          <div class="col-md-6">
                            <div class="small-box bg-aqua">
                              <div class="inner">
                                <h3 id="count_total_leave">4</h3>

                                <p>Annual Leave</p>
                              </div>
                              <div class="icon">
                                <i class="fa fa-plane"></i>
                              </div>
                            </div>
                          </div>
                        <div class="col-md-3"></div>
                      </div>
                        <form role="form" id="addLeave">
                          <div class="box-body">
                            <div class="form-group col-md-12">
                              <label for="label-pid">PID<span class="text-red" style="visibility: hidden" id="error-pid"> *Nama PID kosong</span></label>
                              <select id="input-pid" class="form-control projek-select" style="width: 100%;">
                              </select>
                            </div>
                            <div class="form-group col-md-4">
                              <label for="label-tanggal">Start Leave <span class="text-red" style="visibility: hidden" id="error-start-leave"> *Start Leave kosong</span></label>
                              <div class="input-group">
                                <div class="input-group-addon">
                                  <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" id="input-start-leave" class="form-control pull-right datepicker">
                              </div>
                            </div>
                            <div class="form-group col-md-4">
                              <label for="label-tanggal">Back To Work<span class="text-red" style="visibility: hidden" id="error-end-leave"> *Back To Work kosong</span></label>
                              <div class="input-group">
                                <div class="input-group-addon">
                                  <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" id="input-end-leave" class="form-control pull-right datepicker">
                              </div>
                            </div>
                            <div class="form-group col-md-4">
                              <label for="label-total-days">For () Days</label>
                              <input type="text" disabled="true" class="form-control" id="input-total" value="">
                            </div>
                            <div class="form-group col-md-12">
                              <label for="label-reason">Reason<span class="text-red" style="visibility: hidden" id="error-reason"> *Reason kosong</span></label>
                              <textarea id="input-reason" class="form-control" rows="3" placeholder="Reason"></textarea>
                            </div>
                            <div class="form-group col-md-12">
                              <label for="label-contact">Contact<span class="text-red" style="visibility: hidden" id="error-contact"> *Contact kosong</span></label>
                              <textarea id="input-contact" class="form-control" rows="3" placeholder="Contact"></textarea>
                            </div>
                          </div>
                          <!-- /.box-body -->
                        </form>
                    </div>
                    <div class="modal-footer">
                      <div class="form-group col-md-4 ">
                        <button type="button" id="submit-cuti" class="btn btn-success pull-left"><i id="loading-update-submit" style="display:none;" class="fa fa-spin fa-refresh pull-left"></i>Submit</button>
                        <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Batal</button>
                      </div>       
                    </div>
                  </div>
                  <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
              </div>
              <!-- /.END MODAL PROJEK -->

            </div>
          </div>  
        </div>
      </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
      Anything you want
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2019 <a href="#">Company</a>.</strong> All rights reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane active" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:;">
              <i class="menu-icon fa fa-birthday-cake bg-red"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                <p>Will be 23 on April 24th</p>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:;">
              <h4 class="control-sidebar-subheading">
                Custom Template Design
                <span class="pull-right-container">
                    <span class="label label-danger pull-right">70%</span>
                  </span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">General Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Report panel usage
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Some information about this general settings option
            </p>
          </div>
          <!-- /.form-group -->
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
  immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 3 -->
<script src="<?=base_url()?>assets/web-v2/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?=base_url()?>assets/web-v2/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="<?=base_url()?>assets/web-v2/dist/js/adminlte.min.js"></script>

<!-- ADDITIONAL JS -->
<!-- DataTables -->
<script src="<?=base_url()?>assets/web-v2/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>assets/web-v2/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- Select2 -->
<script src="<?=base_url()?>assets/web-v2/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- bootstrap datepicker -->
<script src="<?=base_url()?>assets/web-v2/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>


<script type="text/javascript">
  var table = $('#cuti-table').DataTable({
       ajax:  {
          url: $("#base-url").val() + "cuti/get_cuti_list",
          dataSrc: 'list_cuti'     
       }, 
       columns: [
        { data: "id_cuti" },
        { data: "id_pegawai" },
        { data: "nama_pegawai" },
        { data: "nama_proyek" },
        { data: "start_leave" },
        { data: "end_leave" },
        { data: "total" },
        { data: "back_to_work" },
        { data: "status_conv" },
        { data: "button" },
       ] 
  })

  $(document).ready(function() {
    $('#add-cuti').click(function () {
      getProjectList();
      empty_modal();
      $('#input-start-leave').datepicker({
        autoclose: true,
        daysOfWeekDisabled:[0,6]
      });
      $('#input-end-leave').datepicker({
        autoclose: true,
        daysOfWeekDisabled:[0,6]
      });
      count_total_leave();

      document.getElementById("error-pid").style.visibility = "hidden";
      document.getElementById("error-start-leave").style.visibility = "hidden";
      document.getElementById("error-end-leave").style.visibility = "hidden";
      document.getElementById("error-reason").style.visibility = "hidden";
      document.getElementById("error-contact").style.visibility = "hidden";

    });

    $('#input-end-leave').on('change', function() {
      var startDate = $('#input-start-leave').val();
      if(startDate.length != 0){
        var splitStartDate = startDate.split("/");
        var compareStartDate = new Date(splitStartDate[2], splitStartDate[0]-1, splitStartDate[1]);
        var splitEndDate = $('#input-end-leave').val().split("/");
        var compareEndDate = new Date(splitEndDate[2], splitEndDate[0]-1, splitEndDate[1]);
        if (compareEndDate < compareStartDate) {
          alert("Tanggal tidak boleh kurang dari tanggal awal cuti");
          $('#input-end-leave').val("");
        } else if (compareStartDate == compareEndDate){
          console.log('start date'+compareStartDate+' and end date '+compareEndDate);
          alert("Tanggal sama");
          $('#input-end-leave').val("");
        } else {
          var diff  = new Date(compareEndDate - compareStartDate);
          var days  = diff/1000/60/60/24;
          for (var i = 1; i <= days; i++) {
            var day = Number(splitStartDate[1])+i;
            var cek = new Date(splitStartDate[2], splitStartDate[0]-1, day);
            if (cek.getDay() == 0 || cek.getDay() == 6) {
              days--;
            }
          }
          var getCount = $('#count_total_leave').text();
          if (days > getCount ) {
            alert('Jumlah cuti tidak mencukupi');
            $('#input-end-leave').val("");
          } else {
            $('#input-total').val(days);
            $('#hidden-input-total').val(days);
          }
        }
      } else {
        alert("Anda belum mengisi start leave");
        $('#input-end-leave').val("");
      }
    });

    $('#submit-cuti').click(function () {
      var cek = false;
      cek = validationInput();
      if (cek) {
        alert('field ada yang kosong');
      } else {
        var submit = confirm("Are you sure submit this leave? ");
        if (submit) {
          document.getElementById("loading-update-submit").style.display = "block";
          $.ajax({
            url: $("#base-url").val() + "cuti/submit_cuti",
            traditional: true,
            type: "post",
            dataType: "text", 
            data: {sendData : JSON.stringify({
                                  idProject:$('#input-pid').val(),
                                  startLeave:$('#input-start-leave').val(),
                                  endLeave:$('#input-end-leave').val(),
                                  totalLeave:$('#input-total').val(),
                                  reason:$('#input-reason').val()
                                })
                  },
            success: function (hasil) {
              var result = JSON.parse(hasil);
              if(result.status=="success"){
                  document.getElementById("loading-update-submit").style.display = "none";
                  empty_modal();
                  $('#modal-addLeave').modal("toggle");
                  table.ajax.reload();
              }
              else{
                  alert("Oops there is something wrong!");
              }
            }
          })
        }
      }
    });
    
  });

  function getProjectList() {
    $('.projek-select').empty();
    $('.projek-select').select2();   
    $.ajax({
        url: $("#base-url").val() + "proyek/ajax_proyek_list_byIdPegawai",
        traditional: true,
        type: "post",
        dataType: "text", 
        success: function (hasil) {
          var result = JSON.parse(hasil)['listProyek'];
          $('#input-pid').append("<option value='' disabled selected>Please select PID</option>");
          $.each( result, function( key, val ) {
                $('#input-pid').append("<option value='"+val.id+"'>"+val.id_proyek+" - "+val.nama_proyek+"</option>");
          })
        }
      })
 }

 function count_total_leave(){
  $.ajax({
        url: $("#base-url").val() + "cuti/count_total_leave",
        traditional: true,
        type: "post",
        dataType: "text", 
        success: function (hasil) {
          var result = JSON.parse(hasil);
          if (result.status == "success") {
            $('#count_total_leave').text(result.count);
          }
        }
      })
 }

 function empty_modal(){
    $('#input-start-leave').val("");
    $('#input-end-leave').val("");
    $('#input-total').val("");
    $('#input-reason').val("");
    $('#input-contact').val("");
  }

  function dataDeletion(idCuti) {
    var submit = confirm("Are you sure want to cancel this leave? ");
    $.ajax({
        url: $("#base-url").val() + "cuti/cancel_leave",
        traditional: true,
        type: "post",
        dataType: "text",
        data: {sendData : JSON.stringify({
                                idCuti:idCuti
                              })
                }, 
        success: function (hasil) {
          var result = JSON.parse(hasil);
          if (result.status == "success") {
            document.getElementById("alert-success").style.display = "block";
            $('#success-response').text(result.message);
            table.ajax.reload();
          }
          else{
              alert("Oops there is something wrong!");
          }

        }
      })
  }

  function validationInput() {
      var cek = false;
      if($('#input-pid').val() == null){
        document.getElementById("error-pid").style.visibility = "visible";
        cek = true;
      } else {
        document.getElementById("error-pid").style.visibility = "hidden";
      }
      if($('#input-start-leave').val().length == 0){
        document.getElementById("error-start-leave").style.visibility = "visible";
        cek = true;
      } else {
        document.getElementById("error-start-leave").style.visibility = "hidden";
      }
      if($('#input-end-leave').val().length == 0){
        document.getElementById("error-end-leave").style.visibility = "visible";
        cek = true;
      } else {
        document.getElementById("error-end-leave").style.visibility = "hidden";
      }
      if($('#input-reason').val().length == 0){
        document.getElementById("error-reason").style.visibility = "visible";
        cek = true;
      } else {
        document.getElementById("error-reason").style.visibility = "hidden";
      }
      if($('#input-contact').val().length == 0){
        document.getElementById("error-contact").style.visibility = "visible";
        cek = true;
      } else {
        document.getElementById("error-contact").style.visibility = "hidden";
      }
      
      
      
      
      
      return cek;
  }
</script>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. -->
</body>
</html>