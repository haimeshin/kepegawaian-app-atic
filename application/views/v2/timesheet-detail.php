<!DOCTYPE html>

<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Application Anabatic</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/dist/css/AdminLTE.min.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect. -->
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/dist/css/skins/skin-blue.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/bower_components/select2/dist/css/select2.min.css">
  <!-- clockpicker -->
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/dist/clockpicker/bootstrap-clockpicker.min.css">

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>


<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <!-- Main Header -->
  <header class="main-header">
    <!-- Logo -->
    <a href="<?=base_url()?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><img class="logoAnabatic" src="<?=base_url()?>assets/web-v2/dist/img/logo-anabatic-lg.png"></img></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Anabatic</b> APP</span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <li><a href="#"><span class="h4">Access Level : <?php echo $role_string; ?></span></a></li>
        </ul>
      </div>

    </nav>
  
  </header>

  <input type="hidden"  id="base-url" value="<?=base_url()?>"/>
  <input type="hidden"  id="id_week_timesheet" value="<?php echo $id_week_timesheet; ?>"/>
  <input type="hidden"  id="start_date" value="<?php echo $start_date; ?>"/>
  <input type="hidden"  id="end_date" value="<?php echo $end_date; ?>"/>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar Menu -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MENU</li>
          <li class="active"><a href="#"><i class="glyphicon glyphicon-list-alt"></i> <span>Timesheet</span></a></li>
          <li><a href="<?=base_url("dashboard/cuti")?>"><i class="glyphicon glyphicon-plane"></i> <span>Cuti</span></a></li>
        <li class="treeview">
          <a href="#">
            <i class="glyphicon glyphicon-pencil"></i>
            <span>Ubah Data</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url("dashboard/edit_detailPegawai")?>"><i class="glyphicon glyphicon-list-alt"></i> Data Detail</a></li>
            <li><a href="<?=base_url("dashboard/edit_passwordPegawai")?>"><i class="glyphicon glyphicon-cog"></i> Password</a></li>
          </ul>
        </li>
        <li><a href="<?=base_url("authentication/logout")?>"><i class="glyphicon glyphicon-log-out"></i> <span>Logout</span></a></li>
       
      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>

  <!--------------------------
    | yhouga content |
  -------------------------->

 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><small></small></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">
      <!--------------------------
        | Alert |
      -------------------------->
      <div class="row">
        <div class="col-md-12">
          <div class="alert alert-success" id="alert-success" style="display: none;">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong id="success-response"></strong>
          </div>
          <div class="alert alert-warning" id="alert-warning" style="display: none;">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong id="warning-response"></strong>
          </div>
        </div>
      </div>
      <!--------------------------
        | Your Page Content Here |
        -------------------------->
      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-body">
              <div class="col-md-12">
                <div class="box-body">
                  <!-- <h4> -->
                    <!-- Please submit the timesheet before 10 September 2019. otherwise you cannot SUBMIT your timesheet in this period! -->
                  <!-- </h4> -->
                  <div class="row">
                    <div class="col-md-4">
                        <button type="button" class="btn btn-primary" id="add-day-timesheet" data-toggle="modal" data-target="#modal-addTimesheet">Tambah </button>
                        <button type="button" class="btn btn-primary" id="import-leave"><i id="loading-import-leave" style="display:none;" class="fa fa-spin fa-refresh pull-left"></i>Import Leave </button>
                    </div>
                    <div class="col-md-12">
                      <table style="text-align: center;" id="list-week-timesheet" class="table table-bordered table-hover">
                        <thead>
                          <tr>
                            <th>Date</th>
                            <th>Category</th>
                            <th>PID</th>
                            <th>Project</th>
                            <th>Stage</th>
                            <th style="width: 15%">Task</th>
                            <th style="width: 15%">Output</th>
                            <th>Duration</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                      </table> 
                    </div>
                    <div class="col-md-4">
                        <button type="button" class="btn btn-success" id="submit-week-timesheet"><i id="loading-update-submit" style="display:none;" class="fa fa-spin fa-refresh pull-left"></i>Submit</button>
                        <a href="<?php echo base_url('dashboard/pegawai'); ?>"><button type="button" id="save-draft" class="btn btn-primary">Save Draft</button></a>
                    </div>
                  </div>
                </div>
              </div>

              <div class="modal fade" id="modal-viewReason">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title">Timesheet Reject Reason</h4>
                    </div>
                    <div class="modal-body">
                      <p id="show-reason"></p>
                    </div>
                    <div class="modal-footer">
                      <div class="form-group col-md-4 ">
                        <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
                      </div>       
                    </div>
                  </div>
                  <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
              </div>
              <!-- /.END MODAL PROJEK -->

              <!-- MODAL PROJEK -->
              <div class="modal fade" id="modal-addTimesheet">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title">Form Tambah Timesheet</h4>
                    </div>
                    <div class="modal-body">
                        <form role="form" id="addTimesheet">
                          <input type="hidden" id="input-id-timesheet-day" value="" class="form-control pull-right">
                          <div class="box-body">
                            <div class="form-group col-md-6">
                              <label for="label-tanggal">Tanggal<span class="text-red" style="visibility: hidden" id="error-tanggal"> *Tanggal kosong</span></label>
                              <div class="input-group">
                                <div class="input-group-addon">
                                  <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" id="input-tanggal" class="form-control pull-right">
                              </div>
                            </div>
                            <div class="form-group col-md-6">
                              <label for="label-category">Category<span class="text-red" style="visibility: hidden" id="error-category"> *Category kosong</span></label>
                              <select id="input-category" class="form-control">
                                <option value="" disabled selected>Please select Category</option>
                                <option value="1">Billable</option>
                                <option value="2">Non Billable</option>
                                <option value="3">Sick</option>
                                <option value="4">Permission</option>
                              </select>
                            </div>
                            <div class="form-group col-md-12">
                              <label for="label-pid">PID<span class="text-red" style="visibility: hidden" id="error-pid"> *PID kosong</span></label>
                              <select id="input-pid" class="form-control projek-select" style="width: 100%;"></select>
                            </div>
                            <div class="form-group col-md-6">
                              <label for="label-stage">Stage<span class="text-red" style="visibility: hidden" id="error-stage"> *Stage kosong</span></label>
                              <select id="input-stage" class="form-control">
                                <option value="" disabled selected>Please select Stage</option>
                                <option value="1">Development</option>
                                <option value="2">Document</option>
                                <option value="3">Maintenance</option>
                                <option value="4">Meeting</option>
                              </select>
                            </div>
                            <div class="form-group col-md-12">
                              <label for="label-task">Task<span class="text-red" style="visibility: hidden" id="error-task"> *Task kosong</span></label>
                              <textarea id="input-task" class="form-control" rows="3" placeholder="Task"></textarea>
                            </div>
                            <div class="form-group col-md-12">
                              <label for="label-output">Output<span class="text-red" style="visibility: hidden" id="error-output"> *Output kosong</span></label>
                              <textarea id="input-output" class="form-control" rows="3" placeholder="Output"></textarea>
                            </div>
                            <div class="form-group col-md-6 clockpicker">
                              <label for="label-start-time">Start from : <span class="text-red" style="visibility: hidden" id="error-start-time"> *Start from kosong</span></label>
                              <input id="input-start-time" type="text" class="form-control" placeholder="Start">
                            </div>
                            <div class="form-group col-md-6 clockpicker">
                              <label for="label-end-time">End : <span class="text-red" style="visibility: hidden" id="error-end-time"> *End kosong</span></label>
                              <input id="input-end-time" type="text" class="form-control" placeholder="End">
                            </div>
                          </div>
                          <!-- /.box-body -->
                        </form>
                    </div>
                    <div class="modal-footer">
                      <div class="form-group col-md-4 ">
                        <button type="button" class="btn btn-success pull-left" id="submit-day-timesheet"><i id="loading" style="display:none;" class="fa fa-spin fa-refresh pull-left"></i>Submit</button>
                        <button type="button" class="btn btn-warning pull-left" id="ubah-day-timesheet" style="display:none;"><i id="loading-update" style="display:none;" class="fa fa-spin fa-refresh pull-left"></i>Ubah</button>
                        <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Batal</button>
                      </div>       
                    </div>
                  </div>
                  <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
              </div>
              <!-- /.END MODAL PROJEK -->
            </div>
          </div>  
        </div>
      </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
      Anything you want
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2019 <a href="#">Company</a>.</strong> All rights reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane active" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:;">
              <i class="menu-icon fa fa-birthday-cake bg-red"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                <p>Will be 23 on April 24th</p>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:;">
              <h4 class="control-sidebar-subheading">
                Custom Template Design
                <span class="pull-right-container">
                    <span class="label label-danger pull-right">70%</span>
                  </span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">General Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Report panel usage
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Some information about this general settings option
            </p>
          </div>
          <!-- /.form-group -->
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
  immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 3 -->
<script src="<?=base_url()?>assets/web-v2/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?=base_url()?>assets/web-v2/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="<?=base_url()?>assets/web-v2/dist/js/adminlte.min.js"></script>
<!-- Select2 -->
<script src="<?=base_url()?>assets/web-v2/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- clockpicker -->
<script type="text/javascript" src="<?=base_url()?>assets/web-v2/dist/clockpicker/bootstrap-clockpicker.min.js"></script>
<!-- DataTables -->
<script src="<?=base_url()?>assets/web-v2/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>assets/web-v2/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- ADDITIONAL JS -->
<!-- bootstrap datepicker -->
<script src="<?=base_url()?>assets/web-v2/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>

<script type="text/javascript">
 var table = $('#list-week-timesheet').DataTable({
       ajax:  {
          url: $("#base-url").val() + "timesheet/get_timesheet_detail_list/"+$('#id_week_timesheet').val(),
          dataSrc: 'list_week_timesheet'     
       }, 
       columns: [
        { data: "date" },
        { data: "category" },
        { data: "id_proyek" },
        { data: "nama_proyek" },
        { data: "stage" },
        { data: "output" },
        { data: "task" },
        { data: "total_hours" },
        { data: "action" },
       ] 
  })

 $(document).ready(function() {
    // set datepicker
    var rawStartDate = $('#start_date').val();
    var splitStartDate = rawStartDate.split("-");
    var startDate = new Date(splitStartDate[2], splitStartDate[1]-1, splitStartDate[0]);
    var rawEndDate = $('#end_date').val();
    var splitEndDate = rawEndDate.split("-");
    var endDate = new Date(splitEndDate[2], splitEndDate[1]-1, splitEndDate[0]);
    //Date picker
    checkWeekTimesheet($('#id_week_timesheet').val());
    $('#input-tanggal').datepicker({
      autoclose: true,
      startDate: startDate,
      endDate: endDate,
      format: 'yyyy-mm-dd'
    });

    $('#add-day-timesheet').click(function () {
      document.getElementById("ubah-day-timesheet").style.display = "none";
      document.getElementById("submit-day-timesheet").style.display = "block";

      document.getElementById("error-tanggal").style.visibility = "hidden";
      document.getElementById("error-category").style.visibility = "hidden";
      document.getElementById("error-pid").style.visibility = "hidden";
      document.getElementById("error-task").style.visibility = "hidden";
      document.getElementById("error-output").style.visibility = "hidden";
      document.getElementById("error-start-time").style.visibility = "hidden";
      document.getElementById("error-end-time").style.visibility = "hidden";
      document.getElementById("error-stage").style.visibility = "hidden";

      getProjectList();
      empty_modal();
    });
    
    // set clock picker
    $('.clockpicker').clockpicker({
        placement: 'top',
        align: 'left',
        donetext: 'Done'
    });

    // submit timesheet day
    $('#submit-day-timesheet').click(function () {
      var cek = false;
      cek = validationInput();
      if (cek) {
        alert('field ada yang kosong');
      } else {
        document.getElementById("loading").style.display = "block";
        $.ajax({
          url: $("#base-url").val() + "timesheet/ajax_add_day_timesheet",
          traditional: true,
          type: "post",
          dataType: "text", 
          data: {sendData : JSON.stringify({
                                idTimesheetWeek:$('#id_week_timesheet').val(),
                                tanggal:$('#input-tanggal').val(),
                                category:$('#input-category').val(),
                                pid:$('#input-pid').val(),
                                stage:$('#input-stage').val(),
                                task:$('#input-task').val(),
                                output:$('#input-output').val(),
                                startTime:$('#input-start-time').val(),
                                endTime:$('#input-end-time').val() 
                })},
          success: function (hasil) {
            var result = JSON.parse(hasil);
            if(result.status=="success"){
                document.getElementById("loading").style.display = "none";
                empty_modal();
                $('#modal-addTimesheet').modal("toggle");
                table.ajax.reload();
            }
            else{
                alert("Oops there is something wrong!");
            }
          }
        })
      }
    })
    
    $('#ubah-day-timesheet').click(function () {
      var cek = false;
      cek = validationInput();
      if (cek) {
        alert('field ada yang kosong');
      } else {
        document.getElementById("loading-update").style.display = "block";
        $.ajax({
          url: $("#base-url").val() + "timesheet/ajax_update_day_timesheet",
          traditional: true,
          type: "post",
          dataType: "text", 
          data: {sendData : JSON.stringify({
                                idTimesheetDay:$('#input-id-timesheet-day').val(),
                                idTimesheetWeek:$('#id_week_timesheet').val(),
                                tanggal:$('#input-tanggal').val(),
                                category:$('#input-category').val(),
                                pid:$('#input-pid').val(),
                                stage:$('#input-stage').val(),
                                task:$('#input-task').val(),
                                output:$('#input-output').val(),
                                startTime:$('#input-start-time').val(),
                                endTime:$('#input-end-time').val()
                              })
                },
          success: function (hasil) {
            var result = JSON.parse(hasil);
            if(result.status=="success"){
                document.getElementById("loading-update").style.display = "none";
                document.getElementById("alert-success").style.display = "block";
                $('#success-response').text(result.message);
                $('#modal-addTimesheet').modal("toggle");
                table.ajax.reload();
            }
            else{
                alert("Oops there is something wrong!");
            }
          }
        })
      }
    })

    $('#submit-week-timesheet').click(function () {
      var submit = confirm("Are you sure to submit this timesheet? ");
      if (submit) {
        document.getElementById("loading-update-submit").style.display = "block";
        $.ajax({
          url: $("#base-url").val() + "timesheet/submit_week_timesheet",
          traditional: true,
          type: "post",
          dataType: "text", 
          data: {sendData : JSON.stringify({
                                idTimesheetWeek:$('#id_week_timesheet').val()
                              })
                },
          success: function (hasil) {
            var result = JSON.parse(hasil);
            if(result.status=="success"){
                document.getElementById("loading-update-submit").style.display = "none";
                // redirect ke halaman sebelumnya
                window.location.href = $("#base-url").val() + "dashboard/pegawai";
            }
            else{
                alert("Oops there is something wrong!");
            }
          }
        })
      } 
    })

    $('#import-leave').click(function () {
      var submit = confirm("Are you sure want to import leave? ");
      if (submit) {
        document.getElementById("loading-import-leave").style.display = "block";
        $.ajax({
          url: $("#base-url").val() + "timesheet/ajax_import_leave",
          traditional: true,
          type: "post",
          dataType: "text", 
          data: {sendData : JSON.stringify({
                              idTimesheetWeek:$('#id_week_timesheet').val(),
                              startDate:$('#start_date').val(),
                              endDate:$('#end_date').val() 
                })
          },
          success: function (hasil) {
            var result = JSON.parse(hasil);
            if(result.status=="success"){
                document.getElementById("loading-import-leave").style.display = "none";
                table.ajax.reload();
            }
            else{
                alert("Oops there is something wrong!");
            }
          }
        })
      } 
    })
    
   });
  
 function getProjectList() {
    $('.projek-select').empty();
    $('.projek-select').select2();   
    $.ajax({
        url: $("#base-url").val() + "proyek/ajax_proyek_list_byIdPegawai",
        traditional: true,
        type: "post",
        dataType: "text", 
        success: function (hasil) {
          var result = JSON.parse(hasil)['listProyek'];
          $('#input-pid').append("<option value='' disabled selected>Please select PID</option>");
          $.each( result, function( key, val ) {
                $('#input-pid').append("<option value='"+val.id+"'>"+val.id_proyek+" - "+val.nama_proyek+"</option>");
          })
        }
      })
 }

  function empty_modal(){
    $('#input-tanggal').datepicker('setDate', null);
    $('#input-category').val("");                             
    $('#input-stage').val("");
    $('#input-task').val("");
    $('#input-output').val("");
    $('#input-start-time').val("");
    $('#input-end-time').val("");
  }

  function fill_modal(id, tanggal, pid, category, stage, task, output, startTime, endTime){
    var rawDate = tanggal.split("-");
    var formatedDate = new Date(rawDate[0], rawDate[1]-1, rawDate[2]);
    $('#input-tanggal').datepicker('setDate', formatedDate);
    $('#input-id-timesheet-day').val(id);
    $('#input-category').val(category);                             
    $('#input-stage').val(stage);
    $('#input-task').val(task);
    $('#input-output').val(output);
    $('#input-start-time').val(startTime);
    $('#input-end-time').val(endTime);
    $('.projek-select').empty();
    $('.projek-select').select2();
    $.ajax({
        url: $("#base-url").val() + "proyek/ajax_proyek_list_byIdPegawai",
        traditional: true,
        type: "post",
        dataType: "text", 
        success: function (hasil) {
          var result = JSON.parse(hasil)['listProyek'];
          $('#input-pid').append("<option value='' disabled>Please select PID</option>");
          $.each( result, function( key, val ) {
            if (pid == val.id) {
              $('#input-pid').append("<option selected value='"+val.id+"'>"+val.id_proyek+" - "+val.nama_proyek+"</option>");
            } else {
              $('#input-pid').append("<option value='"+val.id+"'>"+val.id_proyek+" - "+val.nama_proyek+"</option>");
            }
          })
        }
      })
  }

  function getDataEdit(id) {
    $.ajax({
      url: $("#base-url").val() + "timesheet/get_timesheet_detail/"+id,
      traditional: true,
      type: "post",
      dataType: "text", 
      success: function (hasil) {
        var result = JSON.parse(hasil).list_week_timesheet;
        console.log(result);
        fill_modal( 
          id,
          result.date, 
          result.id_project, 
          result.category, 
          result.stage, 
          result.task, 
          result.output, 
          result.start_time, 
          result.end_time
        );
        document.getElementById("ubah-day-timesheet").style.display = "block";
        document.getElementById("submit-day-timesheet").style.display = "none";
        $('#modal-addTimesheet').modal("toggle");
      }
    })
  }

  function dataDeletion(id) {
    var submit = confirm("Are you sure to delete this record? "); // this goes for bool true or false
    if (submit) {
      $.ajax({
        url: $("#base-url").val() + "timesheet/ajax_delete_record_timesheetDay",
        traditional: true,
        type: "post",
        dataType: "text", 
        data: {sendData : JSON.stringify({
                              id:id
                            })
              },
        success: function (hasil) {
          var result = JSON.parse(hasil);
          if(result.status=="success"){
              document.getElementById("alert-success").style.display = "block";
              document.getElementById("success-response").innerHTML = result.message;
              table.ajax.reload();
          }
          else{
              alert("Oops there is something wrong!");
          }
        }
      })
    }
  }

  function validationInput() {
      var cek = false;
      if($('#input-tanggal').val().length == 0){
        document.getElementById("error-tanggal").style.visibility = "visible";
        cek = true;
      } else {
        document.getElementById("error-tanggal").style.visibility = "hidden";
      }
      if($('#input-category').val() == null){
        document.getElementById("error-category").style.visibility = "visible";
        cek = true;
      } else {
        document.getElementById("error-category").style.visibility = "hidden";
      }
      if($('#input-pid').val() == null){
        document.getElementById("error-pid").style.visibility = "visible";
        cek = true;
      } else {
        document.getElementById("error-pid").style.visibility = "hidden";
      }
      if($('#input-task').val() == 0){
        document.getElementById("error-task").style.visibility = "visible";
        cek = true;
      } else {
        document.getElementById("error-task").style.visibility = "hidden";
      }
      if($('#input-output').val().length == 0){
        document.getElementById("error-output").style.visibility = "visible";
        cek = true;
      } else {
        document.getElementById("error-output").style.visibility = "hidden";
      }
      if($('#input-start-time').val().length == 0){
        document.getElementById("error-start-time").style.visibility = "visible";
        cek = true;
      } else {
        document.getElementById("error-start-time").style.visibility = "hidden";
      }
      if($('#input-end-time').val().length == 0){
        document.getElementById("error-end-time").style.visibility = "visible";
        cek = true;
      } else {
        document.getElementById("error-end-time").style.visibility = "hidden";
      }
      if($('#input-stage').val() == null){
        document.getElementById("error-stage").style.visibility = "visible";
        cek = true;
      } else {
        document.getElementById("error-stage").style.visibility = "hidden";
      }

      return cek;
  }

  function getReason(key){
    $.ajax({
        url: $("#base-url").val() + "timesheet/get_reason_by_timesheet_day/"+key,
        traditional: true,
        type: "post",
        dataType: "text", 
        success: function (hasil) {
          var result = JSON.parse(hasil);
          $('#show-reason').html(result.reason);
          $('#modal-viewReason').modal("toggle");
        }
      })
  }

  function checkWeekTimesheet (idTimesheetWeek){
    $.ajax({
      url: $("#base-url").val() + "timesheet/get_status_weekly_timesheet",
      traditional: true,
      type: "post",
      dataType: "text",
      data: {sendData : JSON.stringify({
                              idTimesheetWeek:idTimesheetWeek
                            })
              }, 
      success: function (hasil) {
        var result = JSON.parse(hasil);
        if(result.status == 'approved' || result.status == 'submited'){
          document.getElementById("add-day-timesheet").style.display = "none";
          document.getElementById("import-leave").style.display = "none";
          document.getElementById("submit-week-timesheet").style.display = "none";
          $('#save-draft').html('Back');
        } else if (result.status == 'reject') {
          document.getElementById("add-day-timesheet").style.display = "none";
          document.getElementById("import-leave").style.display = "none";
          // document.getElementById("submit-week-timesheet").style.display = "none";
          $('#save-draft').html('Back');
        }
      }
    })
 }

</script>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. -->
</body>
</html>