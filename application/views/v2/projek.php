<!DOCTYPE html>

<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Application Anabatic</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/dist/css/AdminLTE.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/bower_components/select2/dist/css/select2.min.css">
  <!-- multi select -->
  <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/web-v2/dist/css/multiselect/multi-select.css">
  <!-- Pace style -->
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/plugins/pace/pace.min.css">
  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect. -->
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/dist/css/skins/skin-blue.min.css">

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>


<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <!-- Main Header -->
  <header class="main-header">
    <!-- Logo -->
    <a href="<?=base_url()?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><img class="logoAnabatic" src="<?=base_url()?>assets/web-v2/dist/img/logo-anabatic-lg.png"></img></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Anabatic</b> APP</span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <li><a href="#"><span class="h4">Access Level : <?php echo $role_string; ?></span></a></li>
        </ul>
      </div>

    </nav>
  
  </header>

  <input type="hidden"  id="base-url" value="<?=base_url()?>"/>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar Menu -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MENU</li>
        <!-- Optionally, you can add icons to the links -->
        <li><a href="<?=base_url("dashboard/admin")?>"><i class="glyphicon glyphicon-user"></i> <span>Kelola Pegawai</span></a></li>
        <li class="active"><a href="#"><i class="glyphicon glyphicon-briefcase"></i> <span>Kelola Projek</span></a></li>
        <!-- <li><a href="timesheet.html"><i class="glyphicon glyphicon-list-alt"></i> <span>Timesheet</span></a></li>
        <li><a href="cuti.html"><i class="glyphicon glyphicon-plane"></i> <span>Cuti</span></a></li> -->
        <li><a href="<?=base_url("authentication/logout")?>"><i class="glyphicon glyphicon-log-out"></i> <span>Logout</span></a></li>
       
      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>

  <!--------------------------
    | yhouga content |
  -------------------------->

 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><small></small></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">
      <!--------------------------
        | Alert |
      -------------------------->
      <div class="row">
        <div class="col-md-12">
          <div class="alert alert-success" id="alert-success" style="display: none;">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong id="success-response"></strong>
          </div>
          <div class="alert alert-warning" id="alert-warning" style="display: none;">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong id="warning-response"></strong>
          </div>
        </div>
      </div>
      <!--------------------------
        | Your Page Content Here |
        -------------------------->
      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-body">
              <div class="col-md-12">
                <h2>
                    Daftar Projek
                  </h2>
                  <button type="button" id="button-addProyek" class="btn btn-primary" data-toggle="modal" data-target="#modal-addProjek">Tambah </button>
                <div class="box-body">
                  <table id="table-projek" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                      <th>Id Projek</th>
                      <th>Nama Projek</th>
                      <th>Customer</th>
                      <th>Active From - To</th>
                      <th>Team</th>
                      <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table> 
              </div>
              </div>

              <!-- MODAL PROJEK -->
              <div class="modal fade" id="modal-addProjek">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                      <h4 id="modal-form-projek" class="modal-title"></h4>
                    </div>
                    <div class="modal-body">
                        <form role="form" id="addProjek">
                        <input type="hidden" class="form-control" id="input-idProyek" value="">
                        <input type="hidden" class="form-control" id="input-keyProyek" value="">
                          <div class="box-body">
                            <div class="form-group col-md-6">
                              <label for="label-nama-projek">Nama Projek<span class="text-red" style="visibility: hidden" id="error-name-projek"> *Nama Projek kosong</span></label>
                              <input type="text" class="form-control" id="input-name-projek" placeholder="Nama Projek">
                            </div>
                            <div class="form-group col-md-6">
                              <label for="label-customer">Customer<span class="text-red" style="visibility: hidden" id="error-customer"> *Customer kosong</span></label>
                              <input type="text" class="form-control" id="input-customer" placeholder="Customer">
                            </div>
                            <div class="form-group col-md-12">
                              <label for="label-valid">Valid (dari-sampai)<span class="text-red" style="visibility: hidden" id="error-projectValid"> *Tanggal Valid kosong</span></label>
                              <div class="input-group">
                                <div class="input-group-addon">
                                  <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control pull-right" id="input-projectValid">
                              </div>
                            </div>
                            <div class="form-group col-md-12">
                              <label for="label-approver">Approver<span class="text-red" style="visibility: hidden" id="error-apporver"> *Approver kosong</span></label>
                              <select id="input-apporver" class="form-control select2" style="width: 100%;">
                              </select>
                            </div>
                            <div class="form-group col-md-12">
                              <label>Select Team<span class="text-red" style="visibility: hidden" id="error-team"> *Team kosong</span></label>
                              <select id='input-team' class="form-control select2" multiple="multiple" data-placeholder="SPilih Anggota Tim"
                                    style="width: 100%; height: 180px;">                                  
                            </select>
                            </div>
                          </div>
                          <!-- /.box-body -->
                        </form>
                    </div>
                    <div class="modal-footer">
                      <div class="form-group col-md-4 ">
                        <button type="button" class="btn btn-success pull-left" id="submit-proyek-new"><i id="loading" style="display:none;" class="fa fa-spin fa-refresh pull-left"></i>Submit</button>
                        <button type="button" class="btn btn-warning pull-left" id="submit-proyek-update" style="display:none;"><i id="loading-update" style="display:none;" class="fa fa-spin fa-refresh pull-left"></i>Ubah</button>
                        <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Batal</button>
                      </div>       
                    </div>
                  </div>
                  <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
              </div>
              <!-- /.END MODAL PROJEK -->

              <div class="modal fade" id="modal-viewTeam">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title">Anggota Team</h4>
                    </div>
                    <div class="modal-body">
                      <dl class="dl-horizontal" id="listTeamProyek"></dl>
                    </div>
                    <div class="modal-footer">
                      <div class="form-group col-md-4 ">
                        <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
                      </div>       
                    </div>
                  </div>
                  <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
              </div>
              <!-- /.END MODAL PROJEK -->
 
            </div>
          </div>  
        </div>
      </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
      Anything you want
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2019 <a href="#">Company</a>.</strong> All rights reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane active" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:;">
              <i class="menu-icon fa fa-birthday-cake bg-red"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                <p>Will be 23 on April 24th</p>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:;">
              <h4 class="control-sidebar-subheading">
                Custom Template Design
                <span class="pull-right-container">
                    <span class="label label-danger pull-right">70%</span>
                  </span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">General Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Report panel usage
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Some information about this general settings option
            </p>
          </div>
          <!-- /.form-group -->
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
  immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 3 -->
<script src="<?=base_url()?>assets/web-v2/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?=base_url()?>assets/web-v2/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="<?=base_url()?>assets/web-v2/dist/js/adminlte.min.js"></script>

<!-- ADDITIONAL JS -->
<!-- DataTables -->
<script src="<?=base_url()?>assets/web-v2/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>assets/web-v2/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- date-range-picker -->
<script src="<?=base_url()?>assets/web-v2/bower_components/moment/min/moment.min.js"></script>
<script src="<?=base_url()?>assets/web-v2/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- Select2 -->
<script src="<?=base_url()?>assets/web-v2/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- multiselect -->
<script src="<?=base_url()?>assets/web-v2/plugins/multiselect/jquery.multi-select.js"></script>

<script src="<?=base_url()?>assets/web-v2/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?=base_url()?>assets/web-v2/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?=base_url()?>assets/web-v2/plugins/input-mask/jquery.inputmask.extensions.js"></script>

<!-- PACE -->
<script src="<?=base_url()?>assets/web-v2/bower_components/PACE/pace.min.js"></script>
<script type="text/javascript">
  $(document).ajaxStart(function () {
    Pace.restart()
  })

  var table = $('#table-projek').DataTable({
       ajax:  {
          url: $("#base-url").val() + "proyek/ajax_proyek_list",
          dataSrc: 'listProyek'
       }, 
       columns: [
        { data: "id_proyek" },
        { data: "nama_proyek" },
        { data: "customer" },
        { data: "projectValid" },
        { data: "listTeam" },
        { data: "action" },
       ] 
  })

  $(function () {
    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    $('[data-mask]').inputmask()
    //Date range picker
    $('#input-projectValid').daterangepicker({
      parentEl: "#modal-addProjek .modal-body",
      locale: {
            format: 'DD/MM/YYYY'
        }
    })

    $('#button-addProyek').click(function () {
      $('#modal-form-projek').text("Form Tambah Projek");
      document.getElementById("submit-proyek-update").style.display = "none";
      document.getElementById("submit-proyek-new").style.display = "block";
      document.getElementById("error-name-projek").style.visibility = "hidden";
      document.getElementById("error-customer").style.visibility = "hidden";
      document.getElementById("error-projectValid").style.visibility = "hidden";
      document.getElementById("error-apporver").style.visibility = "hidden";
      document.getElementById("error-team").style.visibility = "hidden";
      getListElement();
    })

    $('#submit-proyek-new').click(function () {
      var cek = false;
      cek = validationInput();
      if (cek) {
        alert('field ada yang kosong');
      } else {
        document.getElementById("loading").style.display = "block";
        $.ajax({
          url: $("#base-url").val() + "proyek/ajax_proyek_new",
          traditional: true,
          type: "post",
          dataType: "text", 
          data: {sendData : JSON.stringify({
                                nama_proyek:$('#input-name-projek').val(),
                                customer:$('#input-customer').val(),
                                valid_date:$('#input-projectValid').val(),
                                approver:$('#input-apporver').val(), 
                                team:$('#input-team').val()
                              })
                },
          success: function (hasil) {
            var result = JSON.parse(hasil);
            if(result.status=="success"){
              document.getElementById("loading").style.display = "none";
              empty_modal();
              document.getElementById("alert-success").style.display = "block";
              $('#success-response').text(result.message);
              $('#modal-addProjek').modal("toggle");
              table.ajax.reload();
            } else {
              alert("Oops there is something wrong!");
            }
          }
        })
      }

    })

    $('#submit-proyek-update').click(function () {
      var cek = false;
      cek = validationInput();
      if (cek) {
        alert('field ada yang kosong');
      } else {
        document.getElementById("loading").style.display = "block";
        $.ajax({
          url: $("#base-url").val() + "proyek/ajax_proyek_update",
          traditional: true,
          type: "post",
          dataType: "text", 
          data: {sendData : JSON.stringify({
                                id:$('#input-keyProyek').val(),
                                id_proyek:$('#input-idProyek').val(),
                                nama_proyek:$('#input-name-projek').val(),
                                customer:$('#input-customer').val(),
                                valid_date:$('#input-projectValid').val(),
                                approver:$('#input-apporver').val(), 
                                team:$('#input-team').val()
                              })
                },
          success: function (hasil) {
            var result = JSON.parse(hasil);
            if(result.status=="success"){
              document.getElementById("loading").style.display = "none";
              empty_modal();
              document.getElementById("alert-success").style.display = "block";
              $('#success-response').text(result.message);
              $('#modal-addProjek').modal("toggle");
              table.ajax.reload();
            } else {
              alert("Oops there is something wrong!");
            }
          }
        })
      }

    })

  })

  function getDataEdit(id_proyek) {
    $.ajax({
      url: $("#base-url").val() + "proyek/ajax_get_proyek_data",
      traditional: true,
      type: "post",
      dataType: "text", 
      data: {sendData : JSON.stringify({
                            id_proyek:id_proyek
                          })
            },
      success: function (hasil) {
        var result = JSON.parse(hasil);
        getListElementEdit(result.report_user,2);
        fill_modal(
            result.id,
            result.id_proyek, 
            result.nama_proyek, 
            result.customer, 
            result.start_proyek, 
            result.end_proyek, 
            result.approver,
            result.team
        );
        document.getElementById("submit-proyek-update").style.display = "block";
        document.getElementById("submit-proyek-new").style.display = "none";
        $('#modal-form-projek').text("Form Ubah Projek");
        $('#modal-addProjek').modal("toggle");
      }
    })
  }
  
  function dataDeletion(id, nama_proyek) {
    var submit = confirm("Are you sure to delete this proyek ("+nama_proyek+") ?"); // this goes for bool true or false
    if (submit) {
      $.ajax({
        url: $("#base-url").val() + "proyek/ajax_deactivate_proyek",
        traditional: true,
        type: "post",
        dataType: "text", 
        data: {sendData : JSON.stringify({
                              id:id,
                              nama_proyek:nama_proyek
                            })
              },
        success: function (hasil) {
          var result = JSON.parse(hasil);
          if(result.status=="success"){
              document.getElementById("alert-success").style.display = "block";
              document.getElementById("success-response").innerHTML = result.message;
              table.ajax.reload();
          }
          else{
              alert("Oops there is something wrong!");
          }
        }
      })
    }
  }

  function getListElement() {
    empty_modal();
    $('.select2').empty();
    $('.select2').select2();
    var data = null;

    $.ajax({
        url: $("#base-url").val() + "pegawai/ajax_list_approver",
        traditional: true,
        type: "post",
        dataType: "text", 
        success: function (hasil) {
          var result = JSON.parse(hasil);
          $('#input-apporver').append("<option value='' disabled selected>Please select approver</option>");
          $.each( result, function( key, val ) {
                $('#input-apporver').append("<option value='"+val.id_pegawai+"'>"+val.nama_pegawai+"</option>");
          })
        }
      })
    
    $.ajax({
        url: $("#base-url").val() + "pegawai/ajax_list_non_approver",
        traditional: true,
        type: "post",
        dataType: "text", 
        success: function (hasil) {
          var result = JSON.parse(hasil);
          data = result;
          $('#input-team').select2({
            data: data
          });
        }
      })

  }

  function getListElementEdit(idApprover, arrNonApprover) {
    $.ajax({
        url: $("#base-url").val() + "pegawai/ajax_list_approver",
        traditional: true,
        type: "post",
        dataType: "text", 
        success: function (hasil) {
          var result = JSON.parse(hasil);
          $('#input-apporver').append("<option value='' disabled>Please select approver</option>");
          $.each( result, function( key, val ) {
            if (idApprover == val.id_pegawai) {
              $('#input-apporver').append("<option selected value='"+val.id_pegawai+"'>"+val.nama_pegawai+"</option>");
            } else {
              $('#input-apporver').append("<option value='"+val.id_pegawai+"'>"+val.nama_pegawai+"</option>");
            }
          })
        }
      })
    
  }

  function getListTeamByProyek(key){
    $.ajax({
        url: $("#base-url").val() + "pegawai/ajax_list_TeamByProyek",
        traditional: true,
        type: "post",
        dataType: "text", 
        data: {sendData : JSON.stringify({
                              key:key
                            })
              },
        success: function (hasil) {
          var result = JSON.parse(hasil);
          $('#listTeamProyek').html(result.string);
        }
      })
  }

  function empty_modal(){
      $('#input-name-projek').val("");
      $('#input-customer').val("");
      $('#input-projectValid').val("");
      $('#input-apporver').val(""); 
      // tidak bisa masihan
      $('#input-team').val(null).trigger('change');
    }

  function fill_modal(
      keyProyek, idProyek, namaProyek, customer, startDate, endDate, apporver, team
  ){
      var arrStartDate = startDate.split("-");
      var strStartDate = arrStartDate[2]+"/"+arrStartDate[1]+"/"+arrStartDate[0];
      var arrEndDate = endDate.split("-");
      var strEndDate = arrEndDate[2]+"/"+arrEndDate[1]+"/"+arrEndDate[0];
      $('#input-keyProyek').val(keyProyek);
      $('#input-idProyek').val(idProyek);
      $('#input-name-projek').val(namaProyek);
      $('#input-customer').val(customer);
      // tidak bisa masihan
      $('#input-projectValid').daterangepicker({
        parentEl: "#modal-addProjek .modal-body",
        locale: {
              format: 'DD/MM/YYYY'
          },
        startDate: strStartDate,
        endDate: strEndDate
      })
      $('#input-apporver').val(apporver); 
      $('.select2').empty();
      $('.select2').select2();
      $('#input-team').select2({
        data: team
      });
  }

  function validationInput() {
      var cek = false;
      if($('#input-name-projek').val().length == 0){
        document.getElementById("error-name-projek").style.visibility = "visible";
        cek = true;
      } else {
        document.getElementById("error-name-projek").style.visibility = "hidden";
      }
      if($('#input-customer').val().length == 0){
        document.getElementById("error-customer").style.visibility = "visible";
        cek = true;
      } else {
        document.getElementById("error-customer").style.visibility = "hidden";
      }
      if($('#input-projectValid').val().length == 0){
        document.getElementById("error-projectValid").style.visibility = "visible";
        cek = true;
      } else {
        document.getElementById("error-projectValid").style.visibility = "hidden";
      }
      if($('#input-apporver').val() == null){
        document.getElementById("error-apporver").style.visibility = "visible";
        cek = true;
      } else {
        document.getElementById("error-apporver").style.visibility = "hidden";
      }
      if($('#input-team').val().length == 0){
        document.getElementById("error-team").style.visibility = "visible";
        cek = true;
      } else {
        document.getElementById("error-team").style.visibility = "hidden";
      }
      
      return cek;
  }
</script>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. -->
</body>
</html>