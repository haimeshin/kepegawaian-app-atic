<!DOCTYPE html>

<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Application Anabatic</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/dist/css/AdminLTE.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
    <!-- Pace style -->
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/plugins/pace/pace.min.css">
  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect. -->
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/dist/css/skins/skin-blue.min.css">

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>


<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <!-- Main Header -->
  <header class="main-header">
    <!-- Logo -->
    <a href="<?=base_url()?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><img class="logoAnabatic" src="<?=base_url()?>assets/web-v2/dist/img/logo-anabatic-lg.png"></img></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Anabatic</b> APP</span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <li><a href="#"><span class="h4">Access Level : <?php echo $role_string; ?></span></a></li>
        </ul>
      </div>

    </nav>
  
  </header>

  <input type="hidden"  id="base-url" value="<?=base_url()?>"/>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar Menu -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MENU</li>
        <!-- Optionally, you can add icons to the links -->
        <li class="active"><a href="#"><i class="glyphicon glyphicon-user"></i> <span>Kelola Pegawai</span></a></li>
        <li><a href="<?=base_url("dashboard/projek")?>"><i class="glyphicon glyphicon-briefcase"></i> <span>Kelola Projek</span></a></li>
        <!-- <li><a href="timesheet.html"><i class="glyphicon glyphicon-list-alt"></i> <span>Timesheet</span></a></li>
        <li><a href="cuti.html"><i class="glyphicon glyphicon-plane"></i> <span>Cuti</span></a></li> -->
        <li><a href="<?=base_url("authentication/logout")?>"><i class="glyphicon glyphicon-log-out"></i> <span>Logout</span></a></li>
       
      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>

  <!--------------------------
    | Content |
  -------------------------->
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><small></small></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">
      <!--------------------------
        | Alert |
      -------------------------->
      <div class="row">
        <div class="col-md-12">
          <div class="alert alert-success" id="alert-success" style="display: none;">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong id="success-response"></strong>
          </div>
          <div class="alert alert-warning" id="alert-warning" style="display: none;">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong id="warning-response"></strong>
          </div>
        </div>
      </div>
      <!--------------------------
        | Your Page Content Here |
        -------------------------->
      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-body">
              <div class="col-md-12">
                <h2>
                    Daftar Pegawai
                  </h2>
                  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-addPegawai" onclick="empty_modal()" id="button-addPegawai">Tambah</button>
                <div class="box-body">
                  <table id="table-pegawai" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                      <th>Nik Pegawai</th>
                      <th>Nama</th>
                      <th>Role</th>
                      <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table> 
              </div>
              </div>

              <!-- MODAL PEGAWAI -->
              <div class="modal fade" id="modal-addPegawai">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title"></h4>
                    </div>
                    <input type="hidden" class="form-control" id="input-id-pegawai" value="">
                    <input type="hidden" class="form-control" id="input-nik" value="">
                    <div class="modal-body">
                        <form role="form" id="addPegawai">
                          <div class="box-body">
                            <div class="form-group col-md-12">
                              <label for="label-username">Username<span class="text-red" style="visibility: hidden" id="error-username"> *Username kosong</span></label>
                              <input type="text" class="form-control" id="input-username" placeholder="Username">
                            </div>
                            <div class="form-group col-md-12">
                              <label for="label-nama">Nama Lengkap<span class="text-red" style="visibility: hidden" id="error-nama"> *Nama Lengkap kosong</span></label>
                              <input type="text" class="form-control" id="input-nama" placeholder="Nama">
                            </div>
                            <div class="form-group col-md-12">
                              <label for="label-alamat">Alamat<span class="text-red" style="visibility: hidden" id="error-alamat"> *Alamat kosong</span></label>
                              <textarea id="input-alamat" class="form-control" rows="2" placeholder="Alamat"></textarea>
                            </div>
                            <div class="form-group col-md-6">
                              <label for="label-tmpt-lahir">Tempat Lahir<span class="text-red" style="visibility: hidden" id="error-tmpt-lahir"> *Tempat Lahir kosong</span></label>
                              <input type="text" class="form-control" id="input-tmpt-lahir" placeholder="Tempat Lahir">
                            </div>
                            <div class="form-group col-md-6">
                              <label for="label-tgl-lahir">Tanggal Lahir<span class="text-red" style="visibility: hidden" id="error-tgl-lahir"> *Tanggal Lahir kosong</span></label>
                              <div class="input-group">
                                <div class="input-group-addon">
                                  <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control" id="input-tgl-lahir" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                              </div>
                            </div>
                            <div class="form-group col-md-6">
                              <label for="label-email">Email<span class="text-red" style="visibility: hidden" id="error-email"> *Email kosong</span></label>
                              <input type="email" class="form-control" id="input-email" placeholder="Email">
                            </div>
                            <div class="form-group col-md-6">
                              <label for="label-agama">Agama<span class="text-red" style="visibility: hidden" id="error-agama"> *Agama kosong</span></label>
                              <select class="form-control" id="input-agama">
                                <option value="" disabled selected>Agama</option>
                                <option value="Islam">Islam</option>
                                <option value="Kristen">Kristen</option>
                                <option value="Katholik">Katholik</option>
                                <option value="Hindu">Hindu</option>
                                <option value="Budha">Budha</option>
                              </select>
                            </div>
                            <div class="form-group col-md-6">
                              <label for="label-jk">Jenis Kelamin<span class="text-red" style="visibility: hidden" id="error-jk"> *Jenis Kelamin kosong</span></label>
                              <select class="form-control" id="input-jk">
                                <option value="" disabled selected>Jenis Kelamin</option>
                                <option value="L">Laki-laki</option>
                                <option value="P">Perempuan</option>
                              </select>
                            </div>
                            <div class="form-group col-md-6">
                              <label for="label-status">Status<span class="text-red" style="visibility: hidden" id="error-status"> *Status kosong</span></label>
                              <select class="form-control" id="input-status">
                                <option value="" disabled selected>Status</option>
                                <option value="Belum Menikah">Belum Menikah</option>
                                <option value="Menikah">Menikah</option>
                                <option value="Janda">Janda</option>
                                <option value="Duda">Duda</option>
                              </select>
                            </div>
                            <div class="form-group col-md-6">
                              <label for="label-no-telp">Nomor Telpon<span class="text-red" style="visibility: hidden" id="error-no-telp"> *Nomor Telpon kosong</span></label>
                              <input type="text" class="form-control" id="input-no-telp" placeholder="Nomor Telpon">
                            </div>
                            <div class="form-group col-md-6">
                              <label for="label-jabatan">Jabatan<span class="text-red" style="visibility: hidden" id="error-jabatan"> *Jabatan kosong</span></label>
                              <select id="input-jabatan" class="form-control" style="width: 100%;">
                                <option value="" disabled selected>Please select position</option>
                                <option value="Back End Programmer">Back End Programmer</option>
                                <option value="Front End Programmer">Front End Programmer</option>
                                <option value="UI/UX">UI/UX</option>
                                <option value="Business Analyst">Business Analyst</option>
                                <option value="Tester">Tester</option>
                                <option value="Project Manager">Project Manager</option>
                              </select>
                            </div>
                            <div class="form-group col-md-12">
                              <label for="label-role">Role<span class="text-red" style="visibility: hidden" id="error-role"> *Role kosong</span></label>
                              <select class="form-control" id="input-role">
                                <option value="" disabled selected>Role</option>
                                <option value="1">Admin</option>
                                <option value="2">Pegawai</option>
                              </select>
                            </div>
                          </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                      <div class="form-group col-md-4 ">
                        <button type="button" class="btn btn-success pull-left" id="submit-pegawai-new"><i id="loading" style="display:none;" class="fa fa-spin fa-refresh pull-left"></i>Submit</button>
                        <button type="button" class="btn btn-warning pull-left" id="submit-pegawai-update" style="display:none;"><i id="loading-update" style="display:none;" class="fa fa-spin fa-refresh pull-left"></i>Ubah</button>
                        <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Batal</button>
                      </div>       
                    </div>
                  </div>
                  <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
              </div>
              <!-- /.END MODAL PEGAWAI -->
              
            </div>  
          </div>
        </div>  
      </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
      Anything you want
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2019 <a href="#">Company</a>.</strong> All rights reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane active" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:;">
              <i class="menu-icon fa fa-birthday-cake bg-red"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                <p>Will be 23 on April 24th</p>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:;">
              <h4 class="control-sidebar-subheading">
                Custom Template Design
                <span class="pull-right-container">
                    <span class="label label-danger pull-right">70%</span>
                  </span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">General Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Report panel usage
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Some information about this general settings option
            </p>
          </div>
          <!-- /.form-group -->
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
  immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 3 -->
<script src="<?=base_url()?>assets/web-v2/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?=base_url()?>assets/web-v2/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="<?=base_url()?>assets/web-v2/dist/js/adminlte.min.js"></script>
<!-- DataTables -->
<script src="<?=base_url()?>assets/web-v2/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>assets/web-v2/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<script src="<?=base_url()?>assets/web-v2/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?=base_url()?>assets/web-v2/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?=base_url()?>assets/web-v2/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- PACE -->
<script src="<?=base_url()?>assets/web-v2/bower_components/PACE/pace.min.js"></script>
<script type="text/javascript">
  // To make Pace works on Ajax calls
  $(document).ajaxStart(function () {
    Pace.restart()
  })

  var table = $('#table-pegawai').DataTable({
       ajax:  {
          url: $("#base-url").val() + "pegawai/ajax_pegawai_list",
          dataSrc: 'listPegawai'
       }, 
       columns: [
        { data: "nik" },
        { data: "nama_pegawai" },
        { data: "roleid" },
        { data: "button" },
       ] 
  })

  $(function () {
    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    $('[data-mask]').inputmask()
    
    $('#button-addPegawai').click(function () {
      $('.modal-title').text("Form Tambah Pegawai");
      document.getElementById("submit-pegawai-update").style.display = "none";
      document.getElementById("submit-pegawai-new").style.display = "block";
      document.getElementById("error-nama").style.visibility = "hidden";
      document.getElementById("error-username").style.visibility = "hidden";
      document.getElementById("error-email").style.visibility = "hidden";
      document.getElementById("error-tgl-lahir").style.visibility = "hidden";
      document.getElementById("error-tmpt-lahir").style.visibility = "hidden";
      document.getElementById("error-jabatan").style.visibility = "hidden";
      document.getElementById("error-agama").style.visibility = "hidden";
      document.getElementById("error-alamat").style.visibility = "hidden";
      document.getElementById("error-jk").style.visibility = "hidden";
      document.getElementById("error-status").style.visibility = "hidden";
      document.getElementById("error-no-telp").style.visibility = "hidden";
      document.getElementById("error-role").style.visibility = "hidden";
    })

    $('#submit-pegawai-new').click(function () {
      var cek = false;
      cek = validationInput();
      if(cek){
        alert('field ada yang kosong');
      } else {
        document.getElementById("loading").style.display = "block";
        $.ajax({
          url: $("#base-url").val() + "pegawai/ajax_add_pegawai",
          traditional: true,
          type: "post",
          dataType: "text", 
          data: {sendData : JSON.stringify({
                                nama_pegawai:$('#input-nama').val(), 
                                username:$('#input-username').val(),
                                email:$('#input-email').val(), 
                                tempatLahir:$('#input-tmpt-lahir').val(), 
                                tgllahir:$('#input-tgl-lahir').val(), 
                                jabatan:$('#input-jabatan').val(),
                                agama:$('#input-agama').val(),
                                alamat:$('#input-alamat').val(), 
                                jenisKelamin:$('#input-jk').val(), 
                                status:$('#input-status').val(), 
                                noTelp:$('#input-no-telp').val(),
                                roleid:$('#input-role').val()
                              })
                },
          success: function (hasil) {
            var result = JSON.parse(hasil);
            if(result.status=="success"){
                document.getElementById("loading").style.display = "none";
                empty_modal();
                document.getElementById("alert-success").style.display = "block";
                $('#success-response').text(result.message);
                $('#modal-addPegawai').modal("toggle");
                table.ajax.reload();
            }
            else{
                alert("Oops there is something wrong!");
            }
          }
        })
      }
    })

    $('#submit-pegawai-update').click(function () {
      var cek = false;
      cek = validationInput();
      if(cek){
        alert('field ada yang kosong');
      } else {
        document.getElementById("loading-update").style.display = "block";
        $.ajax({
          url: $("#base-url").val() + "pegawai/ajax_update_pegawai",
          traditional: true,
          type: "post",
          dataType: "text", 
          data: {sendData : JSON.stringify({
                                id_pegawai:$('#input-id-pegawai').val(),
                                nik:$('#input-nik').val(),
                                nama_pegawai:$('#input-nama').val(), 
                                username:$('#input-username').val(),
                                email:$('#input-email').val(), 
                                tempatLahir:$('#input-tmpt-lahir').val(), 
                                tgllahir:$('#input-tgl-lahir').val(), 
                                jabatan:$('#input-jabatan').val(),
                                agama:$('#input-agama').val(),
                                alamat:$('#input-alamat').val(), 
                                jenisKelamin:$('#input-jk').val(), 
                                status:$('#input-status').val(), 
                                noTelp:$('#input-no-telp').val(),
                                roleid:$('#input-role').val()
                              })
                },
          success: function (hasil) {
            var result = JSON.parse(hasil);
            if(result.status=="success"){
                document.getElementById("loading-update").style.display = "none";
                empty_modal();
                document.getElementById("alert-success").style.display = "block";
                $('#success-response').text(result.message);
                $('#modal-addPegawai').modal("toggle");
                table.ajax.reload();
            }
            else{
                alert("Oops there is something wrong!");
            }
          }
        })
      }
      
    })

  })

  function empty_modal(){
      $('#input-nama').val("");
      $('#input-username').val("");
      $('#input-email').val("");
      $('#input-tmpt-lahir').val(""); 
      $('#input-tgl-lahir').val(""); 
      $('#input-jabatan').val("");
      $('#input-agama').val("");
      $('#input-alamat').val(""); 
      $('#input-jk').val(""); 
      $('#input-status').val(""); 
      $('#input-no-telp').val("");
      $('#input-role').val("");
      $('#input-id-pegawai').val("");
    }

  function fill_modal(
      idPegawai, nama, nik, username, email, tmptLahir, tglLahir, jabatan, agama, alamat, jk, status, noTelp, role
  ){
      $('#input-id-pegawai').val(idPegawai);
      $('#input-nama').val(nama);
      $('#input-nik').val(nik);
      $('#input-username').val(username);
      $('#input-email').val(email);
      $('#input-tmpt-lahir').val(tmptLahir); 
      $('#input-tgl-lahir').val(tglLahir); 
      $('#input-jabatan').val(jabatan);
      $('#input-agama').val(agama);
      $('#input-alamat').val(alamat); 
      $('#input-jk').val(jk); 
      $('#input-status').val(status); 
      $('#input-no-telp').val(noTelp);
      $('#input-role').val(role);
    }

  function dataDeletion(id, username, nik) {
    var submit = confirm("Are you sure to delete "+nik+" ?"); // this goes for bool true or false
    if (submit) {
      $.ajax({
        url: $("#base-url").val() + "pegawai/ajax_deactivate_pegawai",
        traditional: true,
        type: "post",
        dataType: "text", 
        data: {sendData : JSON.stringify({
                              id:id, 
                              username:username,
                              nik:nik
                            })
              },
        success: function (hasil) {
          var result = JSON.parse(hasil);
          if(result.status=="success"){
              console.log(result.message);
              document.getElementById("alert-success").style.display = "block";
              document.getElementById("success-response").innerHTML = result.message;
              table.ajax.reload();
          }
          else{
              alert("Oops there is something wrong!");
          }
        }
      })
    }
  }

  function getDataEdit(id) {
    $('.modal-title').text("Form Edit Pegawai");
    $.ajax({
      url: $("#base-url").val() + "pegawai/ajax_get_pegawai_data",
      traditional: true,
      type: "post",
      dataType: "text", 
      data: {sendData : JSON.stringify({
                            idPegawai:id
                          })
            },
      success: function (hasil) {
        var result = JSON.parse(hasil);
        fill_modal(
            result.id_pegawai,
            result.nama_pegawai, 
            result.nik,
            result.username, 
            result.email, 
            result.tempat_lahir, 
            result.tanggal_lahir, 
            result.jabatan, 
            result.agama, 
            result.alamat, 
            result.jenis_kelamin, 
            result.status, 
            result.no_telepon, 
            result.roleid
        );
        document.getElementById("submit-pegawai-update").style.display = "block";
        document.getElementById("submit-pegawai-new").style.display = "none";
        $('#modal-addPegawai').modal("toggle");
      }
    })
  }

  function resetPassword(idPegawai, nik) {
    var submit = confirm("Are you sure to reset password for this "+nik+" ?"); // this goes for bool true or false
    if (submit) {
      $.ajax({
        url: $("#base-url").val() + "pegawai/ajax_reset_pegawai_password",
        traditional: true,
        type: "post",
        dataType: "text", 
        data: {sendData : JSON.stringify({
                              idPegawai:idPegawai,
                              nik:nik
                            })
              },
        success: function (hasil) {
          var result = JSON.parse(hasil);
          if(result.status=="success"){
              console.log(result.message);
              document.getElementById("alert-success").style.display = "block";
              document.getElementById("success-response").innerHTML = result.message;
              table.ajax.reload();
          }
          else{
              alert("Oops there is something wrong!");
          }
        }
      })
    }
  }

  function validationInput() {
      var cek = false;
      if($('#input-nama').val().length == 0){
        document.getElementById("error-nama").style.visibility = "visible";
        cek = true;
      } else {
        document.getElementById("error-nama").style.visibility = "hidden";
      }
      if($('#input-username').val().length == 0){
        document.getElementById("error-username").style.visibility = "visible";
        cek = true;
      } else {
        document.getElementById("error-username").style.visibility = "hidden";
      }
      if($('#input-email').val().length == 0){
        document.getElementById("error-email").style.visibility = "visible";
        cek = true;
      } else {
        document.getElementById("error-email").style.visibility = "hidden";
      }
      if($('#input-tgl-lahir').val().length == 0){
        document.getElementById("error-tgl-lahir").style.visibility = "visible";
        cek = true;
      } else {
        document.getElementById("error-tgl-lahir").style.visibility = "hidden";
      }
      if($('#input-tmpt-lahir').val().length == 0){
        document.getElementById("error-tmpt-lahir").style.visibility = "visible";
        cek = true;
      } else {
        document.getElementById("error-tmpt-lahir").style.visibility = "hidden";
      }
      if($('#input-jabatan').val() == null){
        document.getElementById("error-jabatan").style.visibility = "visible";
        cek = true;
      } else {
        document.getElementById("error-jabatan").style.visibility = "hidden";
      }
      if($('#input-agama').val() == null){
        document.getElementById("error-agama").style.visibility = "visible";
        cek = true;
      } else {
        document.getElementById("error-agama").style.visibility = "hidden";
      }
      if($('#input-alamat').val().length == 0){
        document.getElementById("error-alamat").style.visibility = "visible";
        cek = true;
      } else {
        document.getElementById("error-alamat").style.visibility = "hidden";
      }
      if($('#input-jk').val() == null){
        document.getElementById("error-jk").style.visibility = "visible";
        cek = true;
      } else {
        document.getElementById("error-jk").style.visibility = "hidden";
      }
      if($('#input-status').val() == null){
        document.getElementById("error-status").style.visibility = "visible";
        cek = true;
      } else {
        document.getElementById("error-status").style.visibility = "hidden";
      }
      if($('#input-no-telp').val().length == 0){
        document.getElementById("error-no-telp").style.visibility = "visible";
        cek = true;
      } else {
        document.getElementById("error-no-telp").style.visibility = "hidden";
      }
      if($('#input-role').val() == null){
        document.getElementById("error-role").style.visibility = "visible";
        cek = true;
      } else {
        document.getElementById("error-role").style.visibility = "hidden";
      }
      return cek;
  }
  
</script>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. -->
</body>
</html>