<!DOCTYPE html>

<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Application Anabatic</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/dist/css/AdminLTE.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/bower_components/select2/dist/css/select2.min.css">
  <!-- multi select -->
  <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/web-v2/dist/css/multiselect/multi-select.css">
  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect. -->
  <link rel="stylesheet" href="<?=base_url()?>assets/web-v2/dist/css/skins/skin-blue.min.css">

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>


<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <!-- Main Header -->
  <header class="main-header">
    <!-- Logo -->
    <a href="<?=base_url()?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><img class="logoAnabatic" src="<?=base_url()?>assets/web-v2/dist/img/logo-anabatic-lg.png"></img></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Anabatic</b> APP</span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <li><a href="#"><span class="h4">Access Level : <?php echo $role_string; ?></span></a></li>
        </ul>
      </div>

    </nav>
  
  </header>

  <input type="hidden"  id="base-url" value="<?=base_url()?>"/>
  <input type="hidden"  id="id_pegawai" value="<?=$id_pegawai?>"/>
  <input type="hidden"  id="id_proyek" value="<?=$id_proyek?>"/>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar Menu -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MENU</li>
        <li><a href="<?=base_url("dashboard/timesheet_approval")?>"><i class="glyphicon glyphicon-list-alt"></i> <span>Approval Timesheet</span></a></li>
        <li class="active"><a href="#"><i class="glyphicon glyphicon-plane"></i> <span>Approval Cuti</span></a></li>
        <li class="treeview">
          <a href="#">
            <i class="glyphicon glyphicon-pencil"></i>
            <span>Ubah Data</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url("dashboard/edit_detailPegawai")?>"><i class="glyphicon glyphicon-list-alt"></i> Data Detail</a></li>
            <li><a href="<?=base_url("dashboard/edit_passwordPegawai")?>"><i class="glyphicon glyphicon-cog"></i> Password</a></li>
          </ul>
        </li>
        <li><a href="<?=base_url("authentication/logout")?>"><i class="glyphicon glyphicon-log-out"></i> <span>Logout</span></a></li>
       
      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>

  <!--------------------------
    | yhouga content |
  -------------------------->

 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><small></small></h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">
      <!--------------------------
        | Alert |
      -------------------------->
      <div class="row">
        <div class="col-md-12">
          <!-- <div class="alert alert-success" id="alert-success" style="display: none;">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong id="success-response"></strong>
          </div> -->
          <div class="alert alert-danger" id="alert-danger" style="display: none;">
            <a href="#" class="close" data-dismiss="alert"></a>
            <div id="danger-response"></div>
          </div>
        </div>
      </div>
      
      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-body">
              <div class="col-md-12">
                <div class="box-body">
                  <h2>
                    Approval Cuti
                  </h2>
                  <table style="text-align: center;" id="cuti-table" class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th style="text-align: center;">Leave ID</th>
                        <th style="text-align: center;">Employe ID</th>
                        <th style="text-align: center;">Name</th>
                        <th style="text-align: center;">Leave Start</th>
                        <th style="text-align: center;">Leave End</th>
                        <th style="text-align: center;">Total Day</th>
                        <th style="text-align: center;">Back To Work At</th>
                        <th style="text-align: center;">Action</th>
                      </tr>
                    </thead>
                  </table> 
              </div>
                <a href="<?php echo base_url('dashboard/cuti_approval'); ?>"><button type="button" class="btn btn-primary">Back</button></a>
              </div>

            </div>
          </div>  
        </div>
      </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
      Anything you want
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2019 <a href="#">Company</a>.</strong> All rights reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane active" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:;">
              <i class="menu-icon fa fa-birthday-cake bg-red"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                <p>Will be 23 on April 24th</p>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:;">
              <h4 class="control-sidebar-subheading">
                Custom Template Design
                <span class="pull-right-container">
                    <span class="label label-danger pull-right">70%</span>
                  </span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">General Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Report panel usage
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Some information about this general settings option
            </p>
          </div>
          <!-- /.form-group -->
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
  immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 3 -->
<script src="<?=base_url()?>assets/web-v2/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?=base_url()?>assets/web-v2/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="<?=base_url()?>assets/web-v2/dist/js/adminlte.min.js"></script>

<!-- ADDITIONAL JS -->
<!-- DataTables -->
<script src="<?=base_url()?>assets/web-v2/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>assets/web-v2/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>


<script type="text/javascript">
  var table = $('#cuti-table').DataTable({
       ajax:  {
          url: $("#base-url").val() + "cuti/get_cuti_list_approval/"+$('#id_pegawai').val()+"/"+$('#id_proyek').val(),
          dataSrc: 'list_cuti'     
       }, 
       columns: [
        { data: "id_cuti" },
        { data: "id_pegawai" },
        { data: "nama_pegawai" },
        { data: "start_leave" },
        { data: "end_leave" },
        { data: "total" },
        { data: "back_to_work" },
        { data: "button" },
       ] 
  })

  function cuti_approve(idCuti) {
    $.ajax({
        url: $("#base-url").val() + "cuti/cuti_approval/2",
        traditional: true,
        type: "post",
        dataType: "text", 
        data: {sendData : JSON.stringify({
                              idCuti:idCuti
                            })
              },
        success: function (hasil) {
          var result = JSON.parse(hasil);
          if(result.status=="success"){
              table.ajax.reload();
          }
          else{
              alert("Oops there is something wrong!");
          }
        }
      })
  }

  function cuti_reject(idCuti) {
    $.ajax({
        url: $("#base-url").val() + "cuti/cuti_approval/3",
        traditional: true,
        type: "post",
        dataType: "text", 
        data: {sendData : JSON.stringify({
                              idCuti:idCuti
                            })
              },
        success: function (hasil) {
          var result = JSON.parse(hasil);
          if(result.status=="success"){
              table.ajax.reload();
          }
          else{
              alert("Oops there is something wrong!");
          }
        }
      })
  }
  
</script>
</body>
</html>