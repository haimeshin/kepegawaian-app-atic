

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header"><?=$title?></h3>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

            <?php if($this->session->flashdata('success')):?>
                <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong><?php echo $this->session->flashdata('success'); ?></strong>
                </div>
            <?php elseif($this->session->flashdata('error')):?>
                <div class="alert alert-warning">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong><?php echo $this->session->flashdata('error'); ?></strong>
                </div>
            <?php endif;?>
            <div class="row">
                <div class="col-lg-12">  
                     <tr>
                     <td>
                         <a class="btn btn-primary" src="<?=base_url()?>assets/images/add.png" id="proyek-add" data-toggle="modal" data-target="#addProyek"> ADD </a>
                      </td>
                     </tr>
                    <table class="table table-striped table-bordered table-hover" id="dataTables-proyek-list">
                        <thead>
                            <tr>
                                <th>Proyek ID</th>
                                <th>Nama Proyek</th>
                                <th>Customer</th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($proyeks as $row): ?>
                            <tr>
                                <td><?php echo $row->id_proyek; ?></td> 
                                <td><?php echo $row->nama_proyek; ?></td>
                                <td><?php echo $row->customer; ?></td>
                                <td>
                                    <a class="btn btn-primary" id="user-edit"  onclick="edit_proyek_popup('<?=$row->username?>','<?=$row->email?>','<?=$row->id?>','<?=$row->fullname?>','<?=$row->roleid?>');" data-toggle="modal" data-target="#editUser"> EDIT </a>
                                    <a class="btn btn-warning" id="user-riset" onclick="reset_confirmation('<?=$row->username?>','<?=$row->email?>','<?=$row->id?>')" data-toggle="modal" data-target="#resetConfirm"> RESET </a>
                                    <a class="btn btn-danger" id="user-delete" onclick="deactivate_confirmation('<?=$row->username?>','<?=$row->id?>');" data-toggle="modal" data-target="#deactivateConfirm"> DELETE </a>
                                    
                                </td>

                            </tr>
                            <?php endforeach; ?>
                            
                        </tbody>
                    </table>

                    <!-- <div class="col-lg-12" style="position:fixed;bottom: 5%;left: 88%; width: 150px;text-align: center;border-radius: 100%;">
                        <img class="add_user"  data-toggle="modal" data-target="#addUser" />
                    </div> -->

                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>



        <!-- Modal -->
        <div class="modal fade" id="deactivateConfirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header modal-red">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">DELETE CONFIRMATION</h4>
                    </div>
                    <div class="modal-body">
                        <label>You are going to delete user <label id="user-username" style="color:blue;"></label>.</label><br/>
                        <label>Click <b>Yes</b> to continue.</label>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <a id="deactivateYesButton" class="btn btn-danger" >Yes</a>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

        <!-- Modal -->
        <div class="modal fade" id="resetConfirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header modal-red">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">RESET CONFIRMATION</h4>
                    </div>
                    <div class="modal-body">
                        <label>You are going to reset user <label id="reset-user-username" style="color:blue;"></label>'s password.</label><br/>
                        <label>Tempolary password will be sent to this email.</label><br/>
                        <label>Click <b>Yes</b> to continue.</label>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <a id="resetYesButton" class="btn btn-warning" >Yes</a>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->




        <div class="modal fade" id="addProyek" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header modal-blue">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">CREATE NEW PROYEK</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>ID Proyek</label> &nbsp;&nbsp;
                                    <label class="error" id="error_idproyek"> field is required.</label>
                                    <input class="form-control" id="id_proyek" placeholder="ID Proyek" name="id_proyek" type="text" autofocus>
                                </div> 
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Nama Proyek</label> &nbsp;&nbsp;
                                    <label class="error" id="error_namaproyek"> field is required.</label>
                                    <input class="form-control" id="nama_proyek" placeholder="Nama Proyek" name="nama_proyek" type="text" autofocus>
                                </div> 
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Customer</label> &nbsp;&nbsp;
                                    <label class="error" id="error_customer"> field is required.</label>
                                    <input class="form-control" id="customer" placeholder="Customer" name="customer" type="text" autofocus>
                                </div> 
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Report User</label> &nbsp;&nbsp;
                                    <label class="error" id="error_reportUser"> field is required.</label>
                                    <input class="form-control" id="report_user" placeholder="Report User" name="report_user" type="text" autofocus>
                                </div> 
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>Status</label>&nbsp;&nbsp;
                                    <label class="error" id="error_flagStatus"> field is required.</label>
                                    <select name="flagStatus" id="flagStatus" class="form-control" >
                                        <option value="0" selected="selected">-- SELECT STATUS --</option>
                                        <option value="1">Aktif</option>
                                        <option value="2">Non-Aktif</option>
                                    </select> 
                                </div>
                            </div>
                            </div>
                      </div>
                        
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">CANCEL</button>
                        <button id="newProyekSubmit" type="button" class="btn btn-primary">CREATE</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->


        <div class="modal fade" id="editUser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header modal-blue">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">UPDATE USER DETAILS</h4>
                    </div>
                    <div class="modal-body">
                        <input type="hidden"  id="edit-user-id" value=""/>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Username</label> &nbsp;&nbsp;
                                    <label class="error" id="edit-error_username"> field is required.</label>
                                    <label class="error" id="edit-error_username2"> name must be alphanumeric.</label>
                                    <input class="form-control" id="edit-username" placeholder="Username" name="edit-username" type="text" autofocus>
                                </div> 
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Fullname</label> &nbsp;&nbsp;
                                    <label class="error" id="edit-error_fullname"> field is required.</label>
                                    <label class="error" id="edit-error_fullname2"> name must be alphanumeric.</label>
                                    <input class="form-control" id="edit-fullname" placeholder="Fullname" name="edit-fullname" type="text" autofocus>
                                </div> 
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Email</label> &nbsp;&nbsp;
                                    <label class="error" id="edit-error_email"> field is required.</label>
                                    <label class="error" id="edit-error_email2"> email has already exist.</label>
                                    <label class="error" id="edit-error_email3"> invalid email adress.</label>
                                    <input class="form-control" id="edit-email" placeholder="E-mail" name="edit-email" type="email" autofocus>
                                </div> 
                            </div>
                      </div>
                      <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>Role</label>&nbsp;&nbsp;
                                    <label class="error" id="edit-error_roleid"> field is required.</label>
                                    <select name="roleid" id="edit-roleid" class="form-control" >
                                    </select> 
                                </div>
                            </div>
                      </div>
                        
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">CANCEL</button>
                        <button id="editUserSubmit" type="button" class="btn btn-primary">UPDATE</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
       
        <!-- /#page-wrapper -->
        <?php $this->load->view('frame/footer_view')?>
        <script src="<?=base_url()?>assets/js/view/proyek_list.js"></script>