

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header"><?=$title?></h3>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->

            <?php if($this->session->flashdata('success')):?>
                <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong><?php echo $this->session->flashdata('success'); ?></strong>
                </div>
            <?php elseif($this->session->flashdata('error')):?>
                <div class="alert alert-warning">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong><?php echo $this->session->flashdata('error'); ?></strong>
                </div>
            <?php endif;?>
            <div class="row">
                <div class="col-lg-12">  
                     <tr>
                     <td>
                         <a class="btn btn-primary" src="<?=base_url()?>assets/images/add.png" id="user-add" data-toggle="modal" data-target="#addPegawai"> ADD </a>
                      </td>
                     </tr>
                    <table class="table table-striped table-bordered table-hover" id="dataTables-pegawai-list">
                        <thead>
                            <tr>
                                <th>NIK</th>
                                <th>Username</th>
                                <th>Full Name</th>
                                <th>Email</th>
                                <th>Role</th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($pegawais as $row): ?>
                            <tr>
                                <td><?php echo $row->nik; ?></td> 
                                <td><?php echo $row->username; ?></td> 
                                <td><?php echo $row->nama_pegawai; ?></td>
                                <td><?php echo $row->email; ?></td>
                                <?php if($this->session->userdata('roleid') == '1'): ?>
                                <td>Admin</td> 
                                <?php endif; ?>
                                <?php if($this->session->userdata('roleid') == '2'): ?>
                                <td>Pegawai</td> 
                                <?php endif; ?>
                                <?php if($this->session->userdata('roleid') == '3'): ?>
                                <td>Approval</td> 
                                <?php endif; ?>
                                <td>
                                    <a class="btn btn-primary" id="user-edit"  onclick="edit_pegawai_popup('<?=$row->nik?>','<?=$row->nama_pegawai?>','<?=$row->username?>','<?=$row->email?>','<?=$row->tempat_lahir?>','<?=$row->tanggal_lahir?>','<?=$row->alamat?>','<?=$row->jabatan?>','<?=$row->agama?>','<?=$row->jenis_kelamin?>','<?=$row->status?>','<?=$row->no_telepon?>','<?=$row->roleid?>');" data-toggle="modal" data-target="#editUser"> EDIT </a>
                                    <a class="btn btn-warning" id="user-riset" onclick="reset_confirmation('<?=$row->username?>','<?=$row->email?>','<?=$row->nik?>')" data-toggle="modal" data-target="#resetConfirm"> RESET </a>
                                    <a class="btn btn-danger" id="user-delete" onclick="deactivate_confirmation('<?=$row->username?>','<?=$row->nik?>');" data-toggle="modal" data-target="#deactivateConfirm"> DELETE </a>
                                    
                                </td>

                            </tr>
                            <?php endforeach; ?>
                            
                        </tbody>
                    </table>

                    <!-- <div class="col-lg-12" style="position:fixed;bottom: 5%;left: 88%; width: 150px;text-align: center;border-radius: 100%;">
                        <img class="add_user"  data-toggle="modal" data-target="#addUser" />
                    </div> -->

                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>



        <!-- Modal -->
        <div class="modal fade" id="deactivateConfirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header modal-red">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">DELETE CONFIRMATION</h4>
                    </div>
                    <div class="modal-body">
                        <label>You are going to delete user <label id="user-username" style="color:blue;"></label>.</label><br/>
                        <label>Click <b>Yes</b> to continue.</label>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <a id="deactivateYesButton" class="btn btn-danger" >Yes</a>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

        <!-- Modal -->
        <div class="modal fade" id="resetConfirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header modal-red">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">RESET CONFIRMATION</h4>
                    </div>
                    <div class="modal-body">
                        <label>You are going to reset user <label id="reset-user-username" style="color:blue;"></label>'s password.</label><br/>
                        <label>Tempolary password will be sent to this email.</label><br/>
                        <label>Click <b>Yes</b> to continue.</label>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <a id="resetYesButton" class="btn btn-warning" >Yes</a>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->




        <div class="modal fade" id="addPegawai" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header modal-blue">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">CREATE NEW PEGAWAI</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                             <div class="col-lg-6">
                                <div class="form-group">
                                    <label>NIK</label> &nbsp;&nbsp;
                                    <label class="error" id="error_nik"> field is required.</label>
                                    <label class="error" id="error_nik2"> name must be numeric.</label>
                                    <input class="form-control" id="nik" placeholder="NIK" name="nik" type="text" autofocus>
                                </div> 
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Nama Pegawai</label> &nbsp;&nbsp;
                                    <label class="error" id="error_nama"> field is required.</label>
                                    <label class="error" id="error_nama2"> name must be alphanumeric.</label>
                                    <input class="form-control" id="nama_pegawai" placeholder="Nama Pegawai" name="nama_pegawai" type="text" autofocus>
                                </div> 
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Username</label> &nbsp;&nbsp;
                                    <label class="error" id="error_username"> field is required.</label>
                                    <label class="error" id="error_username2"> fullname must be alphanumeric.</label>
                                    <input class="form-control" id="username" placeholder="Username" name="username" type="text" autofocus>
                                </div> 
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Email</label> &nbsp;&nbsp;
                                    <label class="error" id="error_email"> field is required.</label>
                                    <label class="error" id="error_email2"> email has already exist.</label>
                                    <label class="error" id="error_email3"> invalid email adress.</label>
                                    <input class="form-control" id="email" placeholder="E-mail" name="email" type="email" autofocus>
                                </div> 
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Tempat Lahir</label> &nbsp;&nbsp;
                                    <label class="error" id="error_tempatLahir"> field is required.</label>
                                    <input class="form-control" id="tempatLahir" placeholder="Tempat Lahir" name="tempatLahir" type="text" autofocus>
                                </div> 
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Tanggal Lahir</label> &nbsp;&nbsp;
                                    <label class="error" id="error_tgllahir"> field is required.</label>
                                    <input class="form-control" id="tgllahir" placeholder=">Date Birthday" name="tgllahir" type="text" autofocus>
                                </div> 
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Alamat</label> &nbsp;&nbsp;
                                    <label class="error" id="error_alamat"> field is required.</label>
                                    <input class="form-control" id="alamat" placeholder="Alamat" name="alamat" type="text" autofocus>
                                </div> 
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Jabatan</label> &nbsp;&nbsp;
                                    <label class="error" id="error_jabatan"> field is required.</label>
                                    <input class="form-control" id="jabatan" placeholder="Jabatan" name="jabatan" type="text" autofocus>
                                </div> 
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Agama</label> &nbsp;&nbsp;
                                    <label class="error" id="error_agama"> field is required.</label>
                                    <input class="form-control" id="agama" placeholder="Agama" name="agama" type="text" autofocus>
                                </div> 
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Jenis Kelamin</label> &nbsp;&nbsp;
                                    <label class="error" id="error_jenisKelamin"> field is required.</label>
                                    <input class="form-control" id="jenisKelamin" placeholder="Jenis Kelamin" name="jenisKelamin" type="text" autofocus>
                                </div> 
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Status</label> &nbsp;&nbsp;
                                    <label class="error" id="error_status"> field is required.</label>
                                    <input class="form-control" id="status" placeholder="Status" name="status" type="text" autofocus>
                                </div> 
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>No Telepon</label> &nbsp;&nbsp;
                                    <label class="error" id="error_noTelp"> field is required.</label>
                                    <input class="form-control" id="noTelp" placeholder="No Telepon" name="noTelp" type="text" autofocus>
                                </div> 
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>Role</label>&nbsp;&nbsp;
                                    <label class="error" id="error_role"> field is required.</label>
                                    <select name="roleid" id="roleid" class="form-control" >
                                        <option value="0" selected="selected">-- SELECT ROLE --</option>
                                        <option value="1">Admin</option>
                                        <option value="2">Pegawai</option>
                                        <option value="3">Approver</option>
                                    </select> 
                                </div>
                            </div>
                      </div>
                        
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">CANCEL</button>
                        <button id="newPegawaiSubmit" type="button" class="btn btn-primary">CREATE</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->


        <div class="modal fade" id="editUser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header modal-blue">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">UPDATE PEGAWAI DETAILS</h4>
                    </div>
                    <div class="modal-body">
                        <input type="hidden"  id="edit-pegawao-id" value=""/>
                        <div class="row">
                             <div class="col-lg-6">
                                <div class="form-group">
                                    <label>NIK</label> &nbsp;&nbsp;
                                    <label class="error" id="edit-error_nik"> field is required.</label>
                                    <label class="error" id="edit-error_nik2"> name must be numeric.</label>
                                    <input class="form-control" id="edit-nik" placeholder="NIK" readonly="readonly" name="nik" type="text" autofocus>
                                </div> 
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Nama Pegawai</label> &nbsp;&nbsp;
                                    <label class="error" id="edit-error_nama"> field is required.</label>
                                    <label class="error" id="edit-error_nama2"> name must be alphanumeric.</label>
                                    <input class="form-control" id="edit-nama_pegawai" placeholder="Nama Pegawai" name="nama_pegawai" type="text" autofocus>
                                </div> 
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Username</label> &nbsp;&nbsp;
                                    <label class="error" id="edit-error_username"> field is required.</label>
                                    <label class="error" id="edit-error_username2"> fullname must be alphanumeric.</label>
                                    <input class="form-control" id="edit-username" placeholder="Username" name="username" type="text" readonly="readonly" autofocus>
                                </div> 
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Email</label> &nbsp;&nbsp;
                                    <label class="error" id="edit-error_email"> field is required.</label>
                                    <label class="error" id="edit-error_email2"> email has already exist.</label>
                                    <label class="error" id="edit-error_email3"> invalid email adress.</label>
                                    <input class="form-control" id="edit-email" placeholder="E-mail" name="email" type="email" autofocus>
                                </div> 
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Tempat Lahir</label> &nbsp;&nbsp;
                                    <label class="error" id="edit-error_tempatLahir"> field is required.</label>
                                    <input class="form-control" id="edit-tempatLahir" placeholder="Tempat Lahir" name="tempatLahir" type="text" autofocus>
                                </div> 
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Tanggal Lahir</label> &nbsp;&nbsp;
                                    <label class="error" id="edit-error_tgllahir"> field is required.</label>
                                    <input class="form-control" id="edit-tgllahir" placeholder=">Date Birthday" name="tgllahir" type="text" autofocus>
                                </div> 
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Alamat</label> &nbsp;&nbsp;
                                    <label class="error" id="edit-error_alamat"> field is required.</label>
                                    <input class="form-control" id="edit-alamat" placeholder="Alamat" name="alamat" type="text" autofocus>
                                </div> 
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Jabatan</label> &nbsp;&nbsp;
                                    <label class="error" id="edit-error_jabatan"> field is required.</label>
                                    <input class="form-control" id="edit-jabatan" placeholder="Jabatan" name="jabatan" type="text" autofocus>
                                </div> 
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Agama</label> &nbsp;&nbsp;
                                    <label class="error" id="edit-error_agama"> field is required.</label>
                                    <input class="form-control" id="edit-agama" placeholder="Agama" name="agama" type="text" autofocus>
                                </div> 
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Jenis Kelamin</label> &nbsp;&nbsp;
                                    <label class="error" id="edit-error_jenisKelamin"> field is required.</label>
                                    <input class="form-control" id="edit-jenisKelamin" placeholder="Jenis Kelamin" name="jenisKelamin" type="text" autofocus>
                                </div> 
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Status</label> &nbsp;&nbsp;
                                    <label class="error" id="edit-error_status"> field is required.</label>
                                    <input class="form-control" id="edit-status" placeholder="Status" name="status" type="text" autofocus>
                                </div> 
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>No Telepon</label> &nbsp;&nbsp;
                                    <label class="error" id="edit-error_noTelp"> field is required.</label>
                                    <input class="form-control" id="edit-noTelp" placeholder="No Telepon" name="noTelp" type="text" autofocus>
                                </div> 
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>Role</label>&nbsp;&nbsp;
                                    <label class="error" id="edit-error_role"> field is required.</label>
                                    <select name="roleid" id="edit-roleid" class="form-control" >
                                        <option value="0" selected="selected">-- SELECT ROLE --</option>
                                        <option value="1">Admin</option>
                                        <option value="2">Pegawai</option>
                                        <option value="3">Approver</option>
                                    </select> 
                                </div>
                            </div>
                        <!-- <div class="row">
                       
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Username</label> &nbsp;&nbsp;
                                    <label class="error" id="edit-error_username"> field is required.</label>
                                    <label class="error" id="edit-error_username2"> name must be alphanumeric.</label>
                                    <input class="form-control" id="edit-username" placeholder="Username" name="edit-username" type="text" autofocus>
                                </div> 
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Fullname</label> &nbsp;&nbsp;
                                    <label class="error" id="edit-error_fullname"> field is required.</label>
                                    <label class="error" id="edit-error_fullname2"> name must be alphanumeric.</label>
                                    <input class="form-control" id="edit-fullname" placeholder="Fullname" name="edit-fullname" type="text" autofocus>
                                </div> 
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Email</label> &nbsp;&nbsp;
                                    <label class="error" id="edit-error_email"> field is required.</label>
                                    <label class="error" id="edit-error_email2"> email has already exist.</label>
                                    <label class="error" id="edit-error_email3"> invalid email adress.</label>
                                    <input class="form-control" id="edit-email" placeholder="E-mail" name="edit-email" type="email" autofocus>
                                </div> 
                            </div>
                      </div>
                      <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>Role</label>&nbsp;&nbsp;
                                    <label class="error" id="edit-error_roleid"> field is required.</label>
                                    <select name="roleid" id="edit-roleid" class="form-control" >
                                    </select> 
                                </div>
                            </div> -->
                      </div>
                        
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">CANCEL</button>
                        <button id="editPegawaiSubmit" type="button" class="btn btn-primary">UPDATE</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
       
        <!-- /#page-wrapper -->
        <?php $this->load->view('frame/footer_view')?>
        <script src="<?=base_url()?>assets/js/view/pegawai_list.js"></script>