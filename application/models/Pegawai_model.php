<?php

    /******************************************
    *      Codeigniter 3 Simple Login         *
    *   Developer  :  rudiliucs1@gmail.com    *
    *        Copyright © 2017 Rudi Liu        *
    *******************************************/

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Pegawai_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }


    function get_pegawai_list(){
        $this->db->select('*');
        $this->db->from('tblpegawai');
        $this->db->where('updatedStat', 1);
        $query=$this->db->get();
        return $query->result();
    }

    function ajax_get_pegawai_list(){
        return $this->db->query('select * from tblpegawai where updatedStat = 1 and roleid != 1')->result_array();
    }

    function get_pegawai_by_id_pegawai($id_pegawai){
        return $this->db->query('select * from tblpegawai where updatedStat = 1 and roleid != 1 and id_pegawai = '.$id_pegawai)->result_array();
    }

    function ajax_get_approver_list(){
        $this->db->select('id_pegawai, nik, nama_pegawai, jabatan');
        $this->db->from('tblpegawai');
        $this->db->where('jabatan', "Project Manager");
        $this->db->where('updatedStat', "1");
        $this->db->where('roleid != ', "1");
        $query=$this->db->get();
        return $query->result_array();
    }

    function ajax_list_non_approver(){
        $this->db->select('id_pegawai, nik, nama_pegawai, jabatan');
        $this->db->from('tblpegawai');
        $this->db->where('jabatan != ', "Project Manager");
        $this->db->where('updatedStat', "1");
        $this->db->where('roleid != ', "1");
        $this->db->order_by('jabatan','desc');
        $query=$this->db->get();
        return $query->result_array();
    }

    function get_pegawai_by_id($nik){
        $this->db->select('*');
        $this->db->from('tblpegawai');
        $this->db->where('nik', $nik);
        $query=$this->db->get();
        return $query->result_array();
    }

    function get_pegawai_by_column($column, $key){
        return $this->db->query('select * from tblpegawai where \''.$column.'\' = \''.$key.'\'')->result_array();
    }

    function get_pegawai_by_id_user($key){
        return $this->db->query('select * from tblpegawai where id_pegawai = '.$key)->result_array();
    }

    function get_pegawai_team_by_idProyek($key){
        return $this->db->query('SELECT b.id_pegawai, b.nama_pegawai, b.jabatan 
                                    FROM tbljoin_projek_pegawai a join tblpegawai b on a.idPegawai = b.id_pegawai 
                                    where a.idProjek = '.$key.' and updatedStat != 0')->result_array();
    }

    function get_pegawai_team_by_idProyek_edit_menu($key){
        return $this->db->query('select b.id_pegawai, b.nama_pegawai, b.jabatan, a.idProjek
from tblpegawai b left join (select * from tbljoin_projek_pegawai where idProjek = '.$key.') a on b.id_pegawai = a.idPegawai where b.updatedStat != 0 and b.roleid != 1')->result_array();
    }

    function get_last_id(){
        return $this->db->query('SELECT IFNULL(MAX(id_pegawai),0) + 1 AS id FROM tblpegawai')->result_array()[0];
    }

    function validate_email($postData){
        $this->db->where('username', $postData ->username);
        $this->db->where('updatedStat', 1);
        $this->db->from('tblpegawai');
        $query=$this->db->get();

        if ($query->num_rows() == 0)
            return true;
        else
            return false;
    }

    function insert_pegawai($postData){

        $validate = $this->validate_email($postData);
        if($validate){
            // $password = $this->generate_password();
            $data = array(
                'nik' => $postData->nik,
                'nama_pegawai' => $postData->nama_pegawai,
                'username' => $postData->username,
                'password' => md5($postData->username),
                'email' => $postData->email,
                'tempat_lahir' => $postData->tempatLahir,
                'tanggal_lahir' =>   $postData->tgllahir,
                'alamat' => $postData->alamat,
                'jabatan' => $postData->jabatan,
                'agama' => $postData->agama,
                'jenis_kelamin' => $postData->jenisKelamin,
                'status' => $postData->status,
                'no_telepon' => $postData->noTelp,
                'updatedStat' => 1,
                'roleid' => $postData->roleid,
                'created_at' => date('Y\-m\-d\ H:i:s A'),
            );
            $this->db->insert('tblpegawai', $data);

            $password = $this->generate_password();
            $data = array(
                'username' => $postData->username,
                'email' => $postData->email,
                'fullname' => $postData->nama_pegawai,
                'roleid' => $postData->roleid,
                'password' => md5($postData->username),
                'created_at' => date('Y\-m\-d\ H:i:s A'),
            );
            // $this->db->insert('tbluser', $data);

            $message = "Here is your account details:<br><br>Email: ".$postData->username."<br>Tempolary password: ".$postData->username."<br>Please change your password after login.<br><br> you can login at ".base_url().".";
            $subject = "New Account Creation";
            $this->send_email($message,$subject,$postData->email);

            $module = "Pegawai Management";
            $activity = "add new Pegawai ".$postData->email;
            
            return array('status' => 'success', 'message' => 'Pegawai '.$postData->nama_pegawai.' has been successfully created!');

        }else{
            return array('status' => 'exist', 'message' => 'Cannot add this pegawai');
        }

    }

    function update_pegawai_details($postData){

        $oldData = $this->get_pegawai_by_id_pegawai($postData->id_pegawai);

        if($oldData[0]['id_pegawai'] == $postData->id_pegawai)
            $validate = true;
        // else
        //     $validate = $this->validate_email($postData);

        if($validate){
            $data = array(
                'nama_pegawai' => $postData->nama_pegawai,
                'email' => $postData->email,
                'username' => $postData->username,
                'tempat_lahir' => $postData->tempatLahir,
                'tanggal_lahir' => $postData->tgllahir,
                'alamat' => $postData->alamat,
                'jabatan' => $postData->jabatan,
                'agama' => $postData->agama,
                'jenis_kelamin' => $postData->jenisKelamin,
                'status' => $postData->status,
                'updatedStat' => 1,
                'no_telepon' => $postData->noTelp,
                'roleid' => $postData->roleid
            );
            $this->db->where('id_pegawai', $postData->id_pegawai);
            $this->db->update('tblpegawai', $data);

            // $record = "(".$oldData[0]['nama_pegawai']." to ".$postData->nama_pegawai."
            // , ".$oldData[0]['email']." to ".$postData->email."
            // , ".$oldData[0]['username']." to ".$postData->username."
            // ,".$oldData[0]['tempat_lahir']." to ".$postData->tempatLahir."
            // ,".$oldData[0]['tanggal_lahir']." to ".$postData->tgllahir."
            // ,".$oldData[0]['alamat']." to ".$postData->alamat."
            // ,".$oldData[0]['agama']." to ".$postData->agama."
            // ,".$oldData[0]['jabatan']." to ".$postData->jabatan."
            // ,".$oldData[0]['jenis_kelamin']." to ".$postData->jenisKelamin."
            // ,".$oldData[0]['status']." to ".$postData->status."
            // ,".$oldData[0]['updatedStat']." to 1
            // ,".$oldData[0]['no_telepon']." to ".$postData->noTelp."
            // ,".$oldData[0]['roleid']." to ".$postData->roleid.")";

            // $record = "(".$oldData[0]['fullname']." to ".$postData->nama_pegawai."
            // , ".$oldData[0]['email']." to ".$postData->email."
            // , ".$oldData[0]['username']." to ".$postData->username."
            // ,".$oldData[0]['roleid']." to ".$postData->roleid.")";

            // $module = "Pegawai Management";
            // $activity = "update pegawai ".$oldData[0]['nama_pegawai']."`s details ".$record;
            // $this->insert_log($activity, $module);
            return array('status' => 'success', 'message' => 'Succes update '.$postData->nik);
        }else{
            return array('status' => 'exist', 'message' => '');
        }

    }

    function ajax_deactivate_pegawai($postData){

        $data = array(
            'updatedStat' => 0,
        );
        $this->db->where('id_pegawai', $postData->id);
        $this->db->update('tblpegawai', $data);

        // $arr_result = $this->deactivate_user($postData->username);

        // if ($arr_result['status'] == 'success') {
            
            $module = "Pegawai Management";
            $activity = "delete Pegawai ".$postData->username;
            // $this->insert_log($activity, $module);
            return array('status' => 'success', 'message' => 'Pegawai '.$postData->nik.' has been deactivate');
        // }

    }

    function deactivate_pegawai($username,$id_pegawai){

        $data = array(
            'status' => 0,
        );
        $this->db->where('id_pegawai', $id_pegawai);
        $this->db->update('tblpegawai', $data);

        $arr_result = $this->deactivate_user($username,$id_pegawai);

        if ($arr_result['status'] == 'success') {
            
            $module = "Pegawai Management";
            $activity = "delete Pegawai ".$username;
            // $this->insert_log($activity, $module);
            return array('status' => 'success', 'message' => 'Pegawai has been deactivate');
        }

    }


    // function deactivate_user($username){

    //     $data = array(
    //         'status' => 0,
    //     );
    //     $this->db->where('username', $username);
    //     $this->db->update('tbluser', $data);

    //     $module = "Pegawai Management";
    //     $activity = "delete Pegawai ".$username;
    //     // $this->insert_log($activity, $module);
    //     return array('status' => 'success', 'message' => '');

    // }

    function reset_pegawai_password($email,$id){

        $password = $this->generate_password();
        $data = array(
            'password' => md5($password),
        );
        $this->db->where('user_id', $id);
        $this->db->update('user', $data);

        $message = "Your account password has been reset.<br><br>Email: ".$email."<br>Tempolary password: ".$password."<br>Please change your password after login.<br><br> you can login at ".base_url().".";
        $subject = "Password Reset";
        $this->send_email($message,$subject,$email);

        $module = "User Management";
        $activity = "reset user ".$email."`s password";
        $this->insert_log($activity, $module);
        return array('status' => 'success', 'message' => '');

    }

    function ajax_reset_pegawai_password($postData){
        $password = $this->generate_password();
        $get_data_pegawai = $this->get_pegawai_by_id_pegawai($postData->idPegawai)[0];
        $data = array(
            'password' => md5($password),
        );
        $this->db->where('id_pegawai', $postData->idPegawai);
        $this->db->update('tblpegawai', $data);

        $message = "Your account password has been reset.<br><br>Email: ".$get_data_pegawai['email']."<br>Tempolary password: ".$password."<br>Please change your password after login.<br><br> you can login at ".base_url().".";
        $subject = "Password Reset";
        $this->send_email($message,$subject,$get_data_pegawai['email']);

        // $module = "User Management";
        // $activity = "reset user ".$email."`s password";
        // $this->insert_log($activity, $module);
        return array('status' => 'success', 'message' => 'Password successfully reset');
    }

    function ajax_update_pegawai_password($postData){
        $get_data_pegawai = $this->get_pegawai_by_id_pegawai($postData->id_pegawai)[0];
        $data = array(
            'password' => md5($postData->newPassword),
        );
        $this->db->where('id_pegawai', $postData->id_pegawai);
        $this->db->update('tblpegawai', $data);

        $message = "Your account password has been change.<br><br>Email: ".$get_data_pegawai['email']."<br> you can login at ".base_url().".";
        $subject = "Password Change";
        $this->send_email($message,$subject,$get_data_pegawai['email']);

        // $module = "User Management";
        // $activity = "reset user ".$email."`s password";
        // $this->insert_log($activity, $module);
        return array('status' => 'success', 'message' => 'Password successfully updated');
    }

    function generate_password(){
        $chars = "abcdefghjkmnopqrstuvwxyzABCDEFGHJKMNOPQRSTUVWXYZ023456789!@#$%^&*()_=";
        $password = substr( str_shuffle( $chars ), 0, 10 );

        return $password;
    }

    // function insert_log($activity, $module){
    //     $id = $this->session->userdata('user_id');

    //     $data = array(
    //         'fk_user_id' => $id,
    //         'activity' => $activity,
    //         'module' => $module,
    //         'created_at' => date('Y\-m\-d\ H:i:s A')
    //     );
    //     $this->db->insert('activity_log', $data);
    // }

    // function get_activity_log(){
    //    /* Array of database columns which should be read and sent back to DataTables. Use a space where
    //      * you want to insert a non-database field (for example a counter or static image)
    //      */
        
    //     $aColumns = array('date_time', 'activity', 'email', 'module');
    //     $aColumnsWhere = array('activity_log.created_at', 'activity', 'email', 'module');
    //     $aColumnsJoin = array('activity_log.created_at as date_time', 'activity', 'email', 'module');

    //     // DB table to use
    //     $sTable = 'activity_log';
    
    //     $iDisplayStart = $this->input->get_post('iDisplayStart', true);
    //     $iDisplayLength = $this->input->get_post('iDisplayLength', true);
    //     $iSortCol_0 = $this->input->get_post('iSortCol_0', true);
    //     $iSortingCols = $this->input->get_post('iSortingCols', true);
    //     $sSearch = $this->input->get_post('sSearch', true);
    //     $sEcho = $this->input->get_post('sEcho', true);
    
    //     // Paging
    //     if(isset($iDisplayStart) && $iDisplayLength != '-1')
    //     {
    //         $this->db->limit($this->db->escape_str($iDisplayLength), $this->db->escape_str($iDisplayStart));
    //     }
        
    //     // Ordering
    //     if(isset($iSortCol_0))
    //     {
    //         for($i=0; $i<intval($iSortingCols); $i++)
    //         {
    //             $iSortCol = $this->input->get_post('iSortCol_'.$i, true);
    //             $bSortable = $this->input->get_post('bSortable_'.intval($iSortCol), true);
    //             $sSortDir = $this->input->get_post('sSortDir_'.$i, true);
    
    //             if($bSortable == 'true')
    //             {
                    
    //                 $this->db->order_by($aColumns[intval($this->db->escape_str($iSortCol))], $this->db->escape_str($sSortDir));
                    
    //             }
    //         }
    //     }
        
    //     /* 
    //      * Filtering
    //      * NOTE this does not match the built-in DataTables filtering which does it
    //      * word by word on any field. It's possible to do here, but concerned about efficiency
    //      * on very large tables, and MySQL's regex functionality is very limited
    //      */
    //     if(isset($sSearch) && !empty($sSearch))
    //     {
    //         for($i=0; $i<count($aColumns); $i++)
    //         {
    //             $bSearchable = $this->input->get_post('bSearchable_'.$i, true);
                
    //             // Individual column filtering
    //             if(isset($bSearchable) && $bSearchable == 'true')
    //             {
    //                 $this->db->or_like($aColumnsWhere[$i], $this->db->escape_like_str($sSearch));

    //             }
    //         }
    //     }
        
    //     // Select Data
    //     $this->db->join('user', 'activity_log.fk_user_id = user.user_id', 'left');
    //     $this->db->select('SQL_CALC_FOUND_ROWS '.str_replace(' , ', ' ', implode(', ', $aColumnsJoin)), false);
    //     $rResult = $this->db->get($sTable);
    
    //     // Data set length after filtering
    //     $this->db->select('FOUND_ROWS() AS found_rows');
    //     $iFilteredTotal = $this->db->get()->row()->found_rows;
    
    //     // Total data set length
    //     $iTotal = $this->db->count_all($sTable);
    
    //     // Output
    //     $output = array(
    //         'sEcho' => intval($sEcho),
    //         'iTotalRecords' => $iTotal,
    //         'iTotalDisplayRecords' => $iFilteredTotal,
    //         'aaData' => array()
    //     );
        
    //     foreach($rResult->result_array() as $aRow)
    //     {
    //         $row = array();
            
    //         foreach($aColumns as $col)
    //         {
    //             if($col == 'date_time') $aRow[$col] = preg_replace('/\s/','<br />',$aRow[$col]);
    //             $row[] = $aRow[$col];
    //         }
    
    //         $output['aaData'][] = $row;
    //     }
    
    //     return $output;
    // }


    function send_email($message,$subject,$sendTo){
        require_once APPPATH.'libraries/mailer/class.phpmailer.php';
        require_once APPPATH.'libraries/mailer/class.smtp.php';
        require_once APPPATH.'libraries/mailer/mailer_config.php';
        include APPPATH.'libraries/mailer/template/template.php';
        
        $mail = new PHPMailer(true);
        $mail->IsSMTP();
        try
        {
            $mail->SMTPDebug = 1;  
            $mail->SMTPAuth = true; 
            $mail->SMTPSecure = 'ssl'; 
            $mail->Host = HOST;
            $mail->Port = PORT;  
            $mail->Username = GUSER;  
            $mail->Password = GPWD;     
            $mail->SetFrom(GUSER, 'Administrator');
            $mail->Subject = "DO NOT REPLY - ".$subject;
            $mail->IsHTML(true);   
            $mail->WordWrap = 0;


            $hello = '<h1 style="color:#333;font-family:Helvetica,Arial,sans-serif;font-weight:300;padding:0;margin:10px 0 25px;text-align:center;line-height:1;word-break:normal;font-size:38px;letter-spacing:-1px">Hello, &#9786;</h1>';
            $thanks = "<br><br><i>This is autogenerated email please do not reply.</i><br/><br/>Thanks,<br/>Admin<br/><br/>";

            $body = $hello.$message.$thanks;
            $mail->Body = $header.$body.$footer;
            $mail->AddAddress($sendTo);

            if(!$mail->Send()) {
                $error = 'Mail error: '.$mail->ErrorInfo;
                return array('status' => false, 'message' => $error);
            } else { 
                return array('status' => true, 'message' => '');
            }
        }
        catch (phpmailerException $e)
        {
            $error = 'Mail error: '.$e->errorMessage();
            return array('status' => false, 'message' => $error);
        }
        catch (Exception $e)
        {
            $error = 'Mail error: '.$e->getMessage();
            return array('status' => false, 'message' => $error);
        }
        
    }

    // function get_pegawai_list(){
    //     $this->db->select('*');
    //     $this->db->from('tblpegawai');
    //     $this->db->where('status', 1);
    //     $query=$this->db->get();
    //     return $query->result();
    // }

}

/* End of file */
