<?php

    /******************************************
    *      Codeigniter 3 Simple Login         *
    *   Developer  :  rudiliucs1@gmail.com    *
    *        Copyright © 2017 Rudi Liu        *
    *******************************************/

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Proyek_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }


    function get_proyek_list(){
        $this->db->select('*');
        $this->db->from('tblproyek');
        $this->db->where('status', 1);
        $query=$this->db->get();
        return $query->result();
    }

    function ajax_proyek_list(){
        $this->db->select('*');
        $this->db->from('tblproyek');
        $this->db->where('status', 1);
        $query=$this->db->get();
        return $query->result_array();
    }

    function ajax_proyek_list_byIdPegawai($key){
        return $this->db->query('SELECT b.id, b.id_proyek, b.nama_proyek 
                                    FROM tbljoin_projek_pegawai a join tblproyek b on a.idProjek = b.id 
                                    where status != 0 and a.idPegawai = '.$key)->result_array();
    }
    
    function get_proyek_by_id($id_proyek){
        $this->db->select('*');
        $this->db->from('tblproyek');
        $this->db->where('id_proyek', $id_proyek);
        $this->db->where('status', 1);
        $query=$this->db->get();
        return $query->result_array();
    }

    function get_list_project_byApprover($id_approver){
        $this->db->select('*');
        $this->db->from('tblproyek');
        $this->db->where('report_user', $id_approver);
        $this->db->where('status', 1);
        $query=$this->db->get();
        return $query->result_array();
    }

    function get_last_id(){
        return $this->db->query('SELECT IFNULL(MAX(id),0) + 1 AS id FROM tblproyek')->result_array()[0];
    }

    function validate_proyek($postData){
        $this->db->where('id_proyek', $postData->id_proyek);
        $this->db->where('status', 1);
        $this->db->from('tblproyek');
        $query=$this->db->get();

        if ($query->num_rows() == 0)
            return true;
        else
            return false;
    }

    function insert_proyek($postData){
        $data = array(
            'id_proyek' => $postData->id_proyek,
            'nama_proyek' => $postData->nama_proyek,
            'customer' => $postData->customer,
            'report_user' => $postData->approver,
            'flagStatus' => 1,
            'status' => 1,
            'start_proyek' => $postData->startProject,
            'end_proyek' => $postData->endProject,
            'created_at' => date('Y\-m\-d\ H:i:s A'),
        );

        $this->db->insert('tblproyek', $data);
        
        $get_id_proyek_last_insert = $this->get_last_id()['id']-1;
        
        foreach ($postData->team as $id_pegawai) {
            $data_insert = array (
                'idProjek' => $get_id_proyek_last_insert,
                'idPegawai' => $id_pegawai,
            );    
            $this->db->insert('tbljoin_projek_pegawai', $data_insert);
        }
        

        // $message = "Here is your account details:<br><br>Email: ".$postData->id_proyek."<br>Tempolary password: ".$postData->username."<br>Please change your password after login.<br><br> you can login at ".base_url().".";
        // $subject = "New Account Creation";
        // $this->send_email($message,$subject,$postData->email);

        // $module = "Proyek Management";
        // $activity = "add new user ".$postData->id_proyek;
        
        return array('status' => 'success', 'message' => 'Proyek '.$postData->nama_proyek.' has been added');
    }

    function update_proyek($postData){
        $data = array(
            'id_proyek' => $postData->id_proyek,
            'nama_proyek' => $postData->nama_proyek,
            'customer' => $postData->customer,
            'report_user' => $postData->approver,
            'flagStatus' => 1,
            'status' => 1,
            'start_proyek' => $postData->startProject,
            'end_proyek' => $postData->endProject,
            'created_at' => date('Y\-m\-d\ H:i:s A'),
        );

        $this->db->where('id', $postData->id);
        $this->db->update('tblproyek', $data);
        
        $this->db->where('idProjek', $postData->id);
        $this->db->delete('tbljoin_projek_pegawai');
        
        foreach ($postData->team as $id_pegawai) {
            $data_insert = array (
                'idProjek' => $postData->id,
                'idPegawai' => $id_pegawai,
            );    
            $this->db->insert('tbljoin_projek_pegawai', $data_insert);
        }
        

        // $message = "Here is your account details:<br><br>Email: ".$postData->id_proyek."<br>Tempolary password: ".$postData->username."<br>Please change your password after login.<br><br> you can login at ".base_url().".";
        // $subject = "New Account Creation";
        // $this->send_email($message,$subject,$postData->email);

        // $module = "Proyek Management";
        // $activity = "add new user ".$postData->id_proyek;
        
        return array('status' => 'success', 'message' => 'Proyek '.$postData->nama_proyek.' has been updated');
    }
    
    function update_proyek_details($postData){

        $oldData = $this->get_proyek_by_id($postData['id_proyek']);

        if($oldData[0]['id_proyek'] == $postData['id_proyek'])
            $validate = true;
        // else
        //     $validate = $this->validate_email($postData);

        if($validate){
            $data = array(
                'id_proyek' => $postData['id_proyek'],
                'nama_proyek' => $postData['nama_proyek'],
                'customer' => $postData['customer'],
                'report_user' => $postData['report_user'],
                'flagStatus' => $postData['flagStatus'],
            );
            $this->db->where('id_proyek', $postData['id_proyek']);
            $this->db->update('tblproyek', $data);

            $record = "(".$oldData[0]['id_proyek']." to ".$postData['id_proyek'].", ".$oldData[0]['nama_proyek']." to ".$postData['nama_proyek'].",".$oldData[0]['customer']." to ".$postData['customer']."
            ,".$oldData[0]['report_user']." to ".$postData['report_user'].",".$oldData[0]['flagStatus']." to ".$postData['flagStatus'].")";

            $module = "Proyek Management";
            $activity = "update user ".$oldData[0]['id_proyek']."`s details ".$record;
            // $this->insert_log($activity, $module);
            return array('status' => 'success', 'message' => $record);
        }else{
            return array('status' => 'exist', 'message' => '');
        }

    }


    function deactivate_proyek($id_proyek,$id){

        $data = array(
            'status' => 0,
        );
        $this->db->where('id', $id);
        $this->db->update('tblproyek', $data);

        $module = "Proyek Management";
        $activity = "delete user ".$tblproyek;
        // $this->insert_log($activity, $module);
        return array('status' => 'success', 'message' => '');

    }

    function ajax_deactivate_proyek($postData){

        $data = array(
            'status' => 0,
        );
        $this->db->where('id', $postData->id);
        $this->db->update('tblproyek', $data);

        return array('status' => 'success', 'message' => 'Proyek '.$postData->nama_proyek.' has been deleted');

    }

    function reset_proyek_password($email,$id){

        $password = $this->generate_password();
        $data = array(
            'password' => md5($password),
        );
        $this->db->where('user_id', $id);
        $this->db->update('user', $data);

        $message = "Your account password has been reset.<br><br>Email: ".$email."<br>Tempolary password: ".$password."<br>Please change your password after login.<br><br> you can login at ".base_url().".";
        $subject = "Password Reset";
        $this->send_email($message,$subject,$email);

        $module = "User Management";
        $activity = "reset user ".$email."`s password";
        $this->insert_log($activity, $module);
        return array('status' => 'success', 'message' => '');

    }

    function generate_password(){
        $chars = "abcdefghjkmnopqrstuvwxyzABCDEFGHJKMNOPQRSTUVWXYZ023456789!@#$%^&*()_=";
        $password = substr( str_shuffle( $chars ), 0, 10 );

        return $password;
    }

    function insert_log($activity, $module){
        $id = $this->session->userdata('user_id');

        $data = array(
            'fk_user_id' => $id,
            'activity' => $activity,
            'module' => $module,
            'created_at' => date('Y\-m\-d\ H:i:s A')
        );
        $this->db->insert('activity_log', $data);
    }

    function get_activity_log(){
       /* Array of database columns which should be read and sent back to DataTables. Use a space where
         * you want to insert a non-database field (for example a counter or static image)
         */
        
        $aColumns = array('date_time', 'activity', 'email', 'module');
        $aColumnsWhere = array('activity_log.created_at', 'activity', 'email', 'module');
        $aColumnsJoin = array('activity_log.created_at as date_time', 'activity', 'email', 'module');

        // DB table to use
        $sTable = 'activity_log';
    
        $iDisplayStart = $this->input->get_post('iDisplayStart', true);
        $iDisplayLength = $this->input->get_post('iDisplayLength', true);
        $iSortCol_0 = $this->input->get_post('iSortCol_0', true);
        $iSortingCols = $this->input->get_post('iSortingCols', true);
        $sSearch = $this->input->get_post('sSearch', true);
        $sEcho = $this->input->get_post('sEcho', true);
    
        // Paging
        if(isset($iDisplayStart) && $iDisplayLength != '-1')
        {
            $this->db->limit($this->db->escape_str($iDisplayLength), $this->db->escape_str($iDisplayStart));
        }
        
        // Ordering
        if(isset($iSortCol_0))
        {
            for($i=0; $i<intval($iSortingCols); $i++)
            {
                $iSortCol = $this->input->get_post('iSortCol_'.$i, true);
                $bSortable = $this->input->get_post('bSortable_'.intval($iSortCol), true);
                $sSortDir = $this->input->get_post('sSortDir_'.$i, true);
    
                if($bSortable == 'true')
                {
                    
                    $this->db->order_by($aColumns[intval($this->db->escape_str($iSortCol))], $this->db->escape_str($sSortDir));
                    
                }
            }
        }
        
        /* 
         * Filtering
         * NOTE this does not match the built-in DataTables filtering which does it
         * word by word on any field. It's possible to do here, but concerned about efficiency
         * on very large tables, and MySQL's regex functionality is very limited
         */
        if(isset($sSearch) && !empty($sSearch))
        {
            for($i=0; $i<count($aColumns); $i++)
            {
                $bSearchable = $this->input->get_post('bSearchable_'.$i, true);
                
                // Individual column filtering
                if(isset($bSearchable) && $bSearchable == 'true')
                {
                    $this->db->or_like($aColumnsWhere[$i], $this->db->escape_like_str($sSearch));

                }
            }
        }
        
        // Select Data
        $this->db->join('user', 'activity_log.fk_user_id = user.user_id', 'left');
        $this->db->select('SQL_CALC_FOUND_ROWS '.str_replace(' , ', ' ', implode(', ', $aColumnsJoin)), false);
        $rResult = $this->db->get($sTable);
    
        // Data set length after filtering
        $this->db->select('FOUND_ROWS() AS found_rows');
        $iFilteredTotal = $this->db->get()->row()->found_rows;
    
        // Total data set length
        $iTotal = $this->db->count_all($sTable);
    
        // Output
        $output = array(
            'sEcho' => intval($sEcho),
            'iTotalRecords' => $iTotal,
            'iTotalDisplayRecords' => $iFilteredTotal,
            'aaData' => array()
        );
        
        foreach($rResult->result_array() as $aRow)
        {
            $row = array();
            
            foreach($aColumns as $col)
            {
                if($col == 'date_time') $aRow[$col] = preg_replace('/\s/','<br />',$aRow[$col]);
                $row[] = $aRow[$col];
            }
    
            $output['aaData'][] = $row;
        }
    
        return $output;
    }


    function send_email($message,$subject,$sendTo){
        require_once APPPATH.'libraries/mailer/class.phpmailer.php';
        require_once APPPATH.'libraries/mailer/class.smtp.php';
        require_once APPPATH.'libraries/mailer/mailer_config.php';
        include APPPATH.'libraries/mailer/template/template.php';
        
        $mail = new PHPMailer(true);
        $mail->IsSMTP();
        try
        {
            $mail->SMTPDebug = 1;  
            $mail->SMTPAuth = true; 
            $mail->SMTPSecure = 'ssl'; 
            $mail->Host = HOST;
            $mail->Port = PORT;  
            $mail->Username = GUSER;  
            $mail->Password = GPWD;     
            $mail->SetFrom(GUSER, 'Administrator');
            $mail->Subject = "DO NOT REPLY - ".$subject;
            $mail->IsHTML(true);   
            $mail->WordWrap = 0;


            $hello = '<h1 style="color:#333;font-family:Helvetica,Arial,sans-serif;font-weight:300;padding:0;margin:10px 0 25px;text-align:center;line-height:1;word-break:normal;font-size:38px;letter-spacing:-1px">Hello, &#9786;</h1>';
            $thanks = "<br><br><i>This is autogenerated email please do not reply.</i><br/><br/>Thanks,<br/>Admin<br/><br/>";

            $body = $hello.$message.$thanks;
            $mail->Body = $header.$body.$footer;
            $mail->AddAddress($sendTo);

            if(!$mail->Send()) {
                $error = 'Mail error: '.$mail->ErrorInfo;
                return array('status' => false, 'message' => $error);
            } else { 
                return array('status' => true, 'message' => '');
            }
        }
        catch (phpmailerException $e)
        {
            $error = 'Mail error: '.$e->errorMessage();
            return array('status' => false, 'message' => $error);
        }
        catch (Exception $e)
        {
            $error = 'Mail error: '.$e->getMessage();
            return array('status' => false, 'message' => $error);
        }
        
    }
}

/* End of file */
