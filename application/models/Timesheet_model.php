<?php

    /******************************************
    *      Codeigniter 3 Simple Login         *
    *   Developer  :  rudiliucs1@gmail.com    *
    *        Copyright © 2017 Rudi Liu        *
    *******************************************/

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Timesheet_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function check_last_timesheet($id_pegawai){
        $get_monday_system = $this->get_monday_sunday(date('d-m-Y'));
        $get_monday_db = $this->db->query('select start_date from tbltimesheet_week where id_pegawai = '.$id_pegawai.' ORDER BY start_date desc limit 0,1')->result_array();
        if($get_monday_db == null){ // jika dia terdeteksi pegawai baru
            return true;
        } else if ($get_monday_system['monday'] != $get_monday_db[0]['start_date']) {
            return true;
        } else {
            return false;
        }
    }

    function get_list_weekly_timesheet($id_pegawai)
    {
        $this->db->select('*');
        $this->db->from('tbltimesheet_week');
        $this->db->where('id_pegawai', $id_pegawai);
        $query=$this->db->get();
        return $query->result_array();
    }

    function get_weekly_timesheet_byIdWeeklyTimehseet($id_weekly_timesheet)
    {
        $this->db->select('status');
        $this->db->from('tbltimesheet_week');
        $this->db->where('id', $id_weekly_timesheet);
        $query=$this->db->get();
        return $query->result_array();
    }

    function get_list_day_timesheet($id_pegawai, $id_weekly_timesheet)
    {
        $this->db->select('*');
        $this->db->from('tbltimesheet_day');
        $this->db->where('id_timesheet_week', $id_weekly_timesheet);
        $this->db->where('id_pegawai', $id_pegawai);
        $query=$this->db->get();
        return $query->result_array();
    }

    function get_list_weekly_timesheet_byidPegawai_idProyek($id_pegawai, $id_proyek)
    {
        $get_day_timesheet = $this->db->query('SELECT * FROM `tbltimesheet_day` where id_project = '.$id_proyek.' and id_pegawai = '.$id_pegawai)->result_array();
        if (count($get_day_timesheet) > 0) {
            $value = "";
            foreach ($get_day_timesheet as $key) {
                $value .= $key['id_timesheet_week'].",";
            }
            $value = substr($value, 0, -1);
            $where = "(".$value.")";
            return $this->db->query('SELECT id, start_date, end_date, status FROM `tbltimesheet_week` where id in '.$where)->result_array();       
        }
        else {
            return null;
        }
    }

    function get_submited_pegawai_proyek_timesheet($id_pegawai, $id_proyek)
    {   
        return $this->db->query('SELECT * FROM `tbltimesheet_day` where id_project = '.$id_proyek.' and id_pegawai = '.$id_pegawai.' and status = 1 ')->result_array();
    }

    function get_timesheet_detail_list($id_weekly_timesheet)
    {
        return $this->db->query('select b.id_proyek, b.nama_proyek, a.* 
                                    from tbltimesheet_day a join tblproyek b on a.id_project = b.id
                                    where id_timesheet_week = '.$id_weekly_timesheet)->result_array();
    }

    function get_timesheet_detail_list_approver($id_weekly_timesheet, $id_pegawai, $id_proyek)
    {
        return $this->db->query('select b.id_proyek, b.nama_proyek, a.* 
                                    from tbltimesheet_day a join tblproyek b on a.id_project = b.id
                                    where id_timesheet_week = '.$id_weekly_timesheet.' and id_pegawai = '.$id_pegawai.' and id_project = '.$id_proyek)->result_array();
    }

    function get_timesheet_detail($id_timehseet_day)
    {
        return $this->db->query('SELECT * FROM tbltimesheet_day
                                    where id = '.$id_timehseet_day)->result_array();
    }

    function get_cuti_timesheet($postData)
    {
        return $this->db->query('SELECT * FROM tblcuti_timesheet
                                    where id_pegawai = '.$postData->id_pegawai.' and date between \''.date('Y-m-d', strtotime($postData->startDate)).'\' and \''.date('Y-m-d', strtotime($postData->endDate)).'\'')->result_array();
    }

    function get_reason_by_timesheet_day($id_timehseet_day)
    {
        $this->db->select('reason');
        $this->db->from('tbltimesheet_day');
        $this->db->where('id', $id_timehseet_day);
        $query=$this->db->get();
        return $query->result_array();
    }
    
    function add_weekly_timesheet($id_pegawai)
     {
        $current_date = date('Y-m-d') ;
        $setDate = $this->get_monday_sunday($current_date);
        $data = array(
                'id_pegawai' => $id_pegawai,
                'start_date' => $setDate['monday'],
                'end_date' => $setDate['sunday'],
                'status' => 0,
                'created_at' => date('Y\-m\-d\ H:i:s A'),
            );
            $this->db->insert('tbltimesheet_week', $data);
        return array('status' => 'success', 'message' => '');
     }

    function ajax_add_day_timesheet($postData)
     {
        $data = array(
                'id_timesheet_week' => $postData->idTimesheetWeek,
                'id_pegawai' => $postData->id_pegawai,
                'id_project' => $postData->pid,
                'category' => $postData->category,
                'stage' => $postData->stage,
                'date' => $postData->tanggal,
                'task' => $postData->task,
                'output' => $postData->output,
                'start_time' => $postData->startTime,
                'end_time' => $postData->endTime,
                'total_hours' => strval((strtotime($postData->endTime)-strtotime($postData->startTime))/3600),
                'status' => 0,
                'created_at' => date('Y\-m\-d\ H:i:s A'),
            );
        $this->db->insert('tbltimesheet_day', $data);
        return array('status' => 'success', 'message' => '');
     }

     function ajax_add_day_timesheet_fromCutiImport($postData)
     {
        $data = array(
                'id_timesheet_week' => $postData->idTimesheetWeek,
                'id_pegawai' => $postData->id_pegawai,
                'id_project' => $postData->pid,
                'category' => $postData->category,
                'stage' => $postData->stage,
                'date' => $postData->tanggal,
                'task' => $postData->task,
                'output' => $postData->output,
                'start_time' => $postData->startTime,
                'end_time' => $postData->endTime,
                'total_hours' => 24,
                'status' => 2,
                'created_at' => date('Y\-m\-d\ H:i:s A'),
            );
        $this->db->insert('tbltimesheet_day', $data);
        return array('status' => 'success', 'message' => '');
     }  

    function ajax_update_day_timesheet($postData)
    {
        $data = array(
                'id_timesheet_week' => $postData->idTimesheetWeek,
                'id_pegawai' => $postData->id_pegawai,
                'id_project' => $postData->pid,
                'category' => $postData->category,
                'stage' => $postData->stage,
                'date' => $postData->tanggal,
                'task' => $postData->task,
                'output' => $postData->output,
                'start_time' => $postData->startTime,
                'end_time' => $postData->endTime,
                'total_hours' => strval((strtotime($postData->endTime)-strtotime($postData->startTime))/3600),
                'status' => 0,
                'last_updated' => date('Y\-m\-d\ H:i:s A'),
            );
            $this->db->where('id', $postData->idTimesheetDay);
            $this->db->update('tbltimesheet_day', $data);

            return array('status' => 'success', 'message' => 'Data successfully updated');
    }

    function ajax_delete_day_timesheet($postData){
        $this->db->where('id', $postData->id);
        $this->db->delete('tbltimesheet_day');

        return array('status' => 'success', 'message' => 'Data successfully deleted');

    } 

    function ajax_submit_week_timesheet($postData)
    {
        // yhouga 20191009 sudah jadi per masing-masing hari dicek apakah dia statusnya approved atau bukan
        $get_list_day_timesheet = $this->get_list_day_timesheet($this->session->userdata('id'), $postData->idTimesheetWeek);
        foreach ($get_list_day_timesheet as $key) {
            if ($key['status'] != 2) {
                $data_update = array(
                    'status' => 1,
                    'last_updated' => date('Y\-m\-d\ H:i:s A'),
                );    
                $this->db->where('id_timesheet_week', $postData->idTimesheetWeek);
                $this->db->where('id', $key['id']);
                $this->db->where('id_pegawai', $key['id_pegawai']);
                $this->db->update('tbltimesheet_day', $data_update);
            }
        }
        $data = array(
                'status' => 1,
                'last_updated' => date('Y\-m\-d\ H:i:s A'),
            );
        // sudah jadi per masing-masing hari dicek apakah dia statusnya approved atau bukan
            // $this->db->where('id_timesheet_week', $postData->idTimesheetWeek);
            // $this->db->update('tbltimesheet_day', $data);

            $this->db->where('id', $postData->idTimesheetWeek);
            $this->db->update('tbltimesheet_week', $data);

            return array('status' => 'success', '');
    }

    function approve_day_timesheet($postData)
    {
        $data = array(
            'status' => 2,
            'last_updated' => date('Y\-m\-d\ H:i:s A'),
        );
        $this->db->where('id', $postData->idTimesheetDetail);
        $this->db->where('id_timesheet_week', $postData->idTimesheetWeek);
        $this->db->update('tbltimesheet_day', $data);

        return array('status' => 'success', '');
    }

    function reject_day_timesheet($postData)
    {
        $data = array(
            'reason' => $postData->reason,
            'status' => 3,
            'last_updated' => date('Y\-m\-d\ H:i:s A'),
        );
        $this->db->where('id', $postData->idTimesheetDetail);
        $this->db->where('id_timesheet_week', $postData->idTimesheetWeek);
        $this->db->update('tbltimesheet_day', $data);

        return array('status' => 'success', '');
    }

    // additional function
    function get_monday_sunday($date){
        $dates_array = array();

        if(date('N', strtotime($date)) == 7){

            $dates_array['monday'] = date("Y-m-d", strtotime('Monday last week', strtotime($date)));   
            $dates_array['sunday'] =  date("Y-m-d", strtotime('Sunday last week', strtotime($date)));   

        }else{

            $dates_array['monday'] = date("Y-m-d", strtotime('Monday this week', strtotime($date)));   
            $dates_array['sunday'] =  date("Y-m-d", strtotime('Sunday this week', strtotime($date)));   
        }
        return $dates_array;
    }

}

/* End of file */
