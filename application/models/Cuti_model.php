<?php

    /******************************************
    *      Codeigniter 3 Simple Login         *
    *   Developer  :  rudiliucs1@gmail.com    *
    *        Copyright © 2017 Rudi Liu        *
    *******************************************/

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Cuti_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    // get_cuti_list
    function insert_cuti($postData){
       // $rawStartDate = explode('/', $postData->startLeave);
       // $postData->start_leave = date("Y-m-d", $rawStartDate);
       // $rawEndDate = explode('/', $postData->endLeave);
       // $postData->end_leave = date("Y-m-d", $rawEndDate);
       $data = array(
                'id_project' => $postData->idProject,
                'id_pegawai' => $postData->idPegawai,
                'total' => $postData->totalLeave,
                'start_leave' => date_format(date_create_from_format("m/d/Y", $postData->startLeave),"Y-m-d"),
                'end_leave' => date_format(date_create_from_format("m/d/Y", $postData->endLeave),"Y-m-d"),
                'reason' => $postData->reason,
                'status' => 0,
                'created_at' => date('Y\-m\-d\ H:i:s A'),
            );
            $this->db->insert('tblcuti', $data);

        return array('status' => 'success', 'message' => 'Leave has been submited');
    }

    function cuti_approval($postData){
       $data = array(
                'status' => $postData->status,
                'created_at' => date('Y\-m\-d\ H:i:s A'),
            );
            $this->db->where('id', $postData->idCuti);
            $this->db->update('tblcuti', $data);

        if ($postData->status == 2) {
            $this->db->select('*');    
            $this->db->from('tblcuti');            
            $this->db->where('id', $postData->idCuti);
            $data_cuti = $this->db->get()->result_array()[0];

            $count_weekend = 0;
            for ($i=0; $i < $data_cuti['total']; $i++) { 
                $date_insert = date('Y-m-d', strtotime($data_cuti['start_leave']. ' + '.($i+$count_weekend).' days'));
                if (!$this->isWeekend($date_insert)) {
                    $data = array(
                            'id_pegawai' => $data_cuti['id_pegawai'],
                            'id_project' => $data_cuti['id_project'],
                            'category' => 1,
                            'stage' => 5,
                            'date' => $date_insert,
                            'task' => "CUTI",
                            'output' => "CUTI",
                            'start_time' => "00:00",
                            'end_time' => "24:00",
                            'total_hours' => "24",
                        );
                    $this->db->insert('tblcuti_timesheet', $data);
                } else {
                    $count_weekend++;
                    $i = $i-1;
                }
            }
        }

        return array('status' => 'success', 'message' => '');
    }

    function count_total_leave($data_pegawai)
    {
        return $this->get_month_diff(strtotime($data_pegawai['created_at']));
    }

    function count_used_leave($data_pegawai)
    {
        return $this->db->query('SELECT total from tblcuti where id_pegawai = '.$data_pegawai['id_pegawai'].' and status = 2')->result_array();
    }

    function get_cuti_list($id_pegawai)
    {
        return $this->db->query('SELECT 
                                    c.id as id_cuti, p.nik as id_pegawai, p.nama_pegawai, pr.nama_proyek, c.start_leave, c.end_leave, c.total, c.status
                                FROM 
                                    `tblcuti` c join tblpegawai p on c.id_pegawai = p.id_pegawai
                                    join tblproyek pr on c.id_project = pr.id where c.id_pegawai = '.$id_pegawai)->result_array();
    }

    function get_cuti_list_by_date_between($id_pegawai, $start_date, $end_date)
    {
        return $this->db->query('SELECT 
                                    c.id as id_cuti, p.nik as id_pegawai, p.nama_pegawai, pr.nama_proyek, c.start_leave, c.end_leave, c.total, c.status
                                FROM 
                                    `tblcuti` c join tblpegawai p on c.id_pegawai = p.id_pegawai
                                    join tblproyek pr on c.id_project = pr.id where c.id_pegawai = '.$id_pegawai)->result_array();
    }

    function get_cuti_list_approval($id_pegawai, $id_proyek)
    {
        return $this->db->query('SELECT 
                                    c.id as id_cuti, p.nik as id_pegawai, p.nama_pegawai, pr.nama_proyek, c.start_leave, c.end_leave, c.total, c.status
                                FROM 
                                    `tblcuti` c join tblpegawai p on c.id_pegawai = p.id_pegawai and c.id_pegawai = '.$id_pegawai.' 
                                    join tblproyek pr on c.id_project = pr.id and pr.id = '.$id_proyek)->result_array();
    }

    function get_submited_pegawai_proyek_cuti($id_pegawai, $id_proyek)
    {   
        return $this->db->query('SELECT * FROM `tblcuti` where id_project = '.$id_proyek.' and id_pegawai = '.$id_pegawai.' and status = 0 ')->result_array();
    }

    function get_month_diff($start, $end = FALSE)
    {
        $end OR $end = time();
        $start = new DateTime("@$start");
        $end   = new DateTime("@$end");
        $diff  = $start->diff($end);
        return $diff->format('%y') * 12 + $diff->format('%m');
    }

    function delete_cuti($id_cuti)
    {
        $this->db->where('id', $id_cuti);
        $this->db->delete('tblcuti');
        return array('status' => 'success', 'message' => 'Leave has been deleted');

    }

    function delete_cuti_timesheet($id_cuti_timesheet)
    {
        $this->db->where('id', $id_cuti_timesheet);
        $this->db->delete('tblcuti_timesheet');
        return array('status' => 'success', 'message' => '');

    }

    function isWeekend($date) {
        $weekDay = date('w', strtotime($date));
        return ($weekDay == 0 || $weekDay == 6);
    }
}

/* End of file */
