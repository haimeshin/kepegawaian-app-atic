-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 30, 2019 at 03:54 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.1.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbapp`
--

-- --------------------------------------------------------

--
-- Table structure for table `activity_log`
--

CREATE TABLE `activity_log` (
  `id` bigint(20) NOT NULL,
  `fk_user_id` varchar(255) DEFAULT NULL,
  `activity` varchar(1000) DEFAULT NULL,
  `module` varchar(1000) DEFAULT NULL,
  `created_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `activity_log`
--

INSERT INTO `activity_log` (`id`, `fk_user_id`, `activity`, `module`, `created_at`) VALUES
(1, NULL, 'reset user yhouga.13@gmail.com`s password', 'User Management', '2019-08-29');

-- --------------------------------------------------------

--
-- Table structure for table `tblcuti`
--

CREATE TABLE `tblcuti` (
  `id` int(11) NOT NULL,
  `id_project` int(11) NOT NULL,
  `id_pegawai` int(11) NOT NULL,
  `start_leave` date NOT NULL,
  `end_leave` date NOT NULL,
  `total` int(11) NOT NULL,
  `reason` text NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblcuti`
--

INSERT INTO `tblcuti` (`id`, `id_project`, `id_pegawai`, `start_leave`, `end_leave`, `total`, `reason`, `status`, `created_at`) VALUES
(3, 1, 7, '2019-09-10', '2019-09-11', 1, 'asd', 0, '2019-09-27 17:33:12');

-- --------------------------------------------------------

--
-- Table structure for table `tbljabatan`
--

CREATE TABLE `tbljabatan` (
  `id` int(11) NOT NULL,
  `jabatan` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbljabatan`
--

INSERT INTO `tbljabatan` (`id`, `jabatan`) VALUES
(1, 'Programmer (Front End)'),
(2, 'Programmer (Back End)'),
(3, 'Business Analyst'),
(4, 'Tester');

-- --------------------------------------------------------

--
-- Table structure for table `tbljoin_projek_pegawai`
--

CREATE TABLE `tbljoin_projek_pegawai` (
  `idProjek` int(11) NOT NULL,
  `idPegawai` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbljoin_projek_pegawai`
--

INSERT INTO `tbljoin_projek_pegawai` (`idProjek`, `idPegawai`) VALUES
(1, 1),
(1, 2),
(1, 6),
(2, 7),
(2, 1),
(2, 2),
(2, 3),
(2, 6),
(3, 9),
(3, 7),
(3, 2),
(3, 3),
(3, 6);

-- --------------------------------------------------------

--
-- Table structure for table `tblpegawai`
--

CREATE TABLE `tblpegawai` (
  `id_pegawai` int(11) NOT NULL,
  `nik` varchar(255) NOT NULL,
  `nama_pegawai` varchar(500) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `tempat_lahir` varchar(255) NOT NULL,
  `tanggal_lahir` varchar(255) DEFAULT NULL,
  `alamat` varchar(500) NOT NULL,
  `jabatan` varchar(255) NOT NULL,
  `agama` varchar(255) NOT NULL,
  `jenis_kelamin` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `no_telepon` int(11) NOT NULL,
  `updatedStat` tinyint(4) NOT NULL,
  `roleid` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblpegawai`
--

INSERT INTO `tblpegawai` (`id_pegawai`, `nik`, `nama_pegawai`, `username`, `password`, `email`, `tempat_lahir`, `tanggal_lahir`, `alamat`, `jabatan`, `agama`, `jenis_kelamin`, `status`, `no_telepon`, `updatedStat`, `roleid`, `created_at`) VALUES
(1, 'ATIC-1', 'Update', 'yhouga13', '9315de29b738d191c4821ab2af72a30e', 'yhouga.13@gmail.com', 'Blitar', '13/06/1995', 'jl. alamat2', 'Front End Programmer', 'Islam', 'L', 'Menikah', 2147483647, 0, 2, '2019-09-14 12:07:59'),
(2, 'ATIC-2', 'Update 1', 'username2', 'fdf65f9ebc8ff131074ad20499655755', 'yhouga.13@gmail.com', 'tempat2', '13/06/1995', 'alamat2', 'Back End Programmer', 'Islam', 'P', 'Belum Menikah', 1231234, 0, 2, '2019-09-14 12:36:04'),
(3, 'ATIC-3', 'Bagus', 'usernamebaru6update', '55266da88cf1e95678cab8cea9e5d73b', 'yhouga.13@gmail.com', 'Malang', '13/06/1995', 'alamat6', 'Business Analyst', 'Islam', 'L', 'Belum Menikah', 1351231, 1, 2, '2019-09-14 17:17:37'),
(4, 'ATIC-4', 'Rahajeng Iga', 'rahajengiga', '6abeb58397ae334d5887ce0163723907', 'yhouga.13@gmail.com', 'Salatiga', '13/06/1987', 'Jl. Anabatic', 'Project Manager', 'Islam', 'P', 'Menikah', 21309123, 1, 2, '2019-09-14 20:02:48'),
(5, 'ATIC-5', 'Rocky', 'rocky', '5bab541acd761a3093d7c4202b6e1da9', 'yhouga.13@gmail.com', 'Jakarta', '13/01/1987', 'Jl. Anabatic2', 'Project Manager', 'Katholik', 'L', 'Menikah', 1231092812, 1, 2, '2019-09-14 20:32:12'),
(6, 'ATIC-6', 'tester 1', 'tester1', '72a3dcef165d9122a45decf13ae20631', 'yhouga.13@gmail.com', 'Malang', '13/06/1995', 'jl. tester', 'Tester', 'Islam', 'L', 'Belum Menikah', 12309123, 1, 2, '2019-09-16 06:50:43'),
(7, 'ATIC-7', 'yhouga beta evantio', 'yhouga123', '5c294b7f2ab259ccf39ea238ff017825', 'yhouga.13@gmail.com', 'Malang', '13/06/1995', 'Jl. alamat new2', 'Front End Programmer', 'Islam', 'L', 'Menikah', 12312312, 1, 2, '2019-07-08 06:34:25'),
(8, '000000000', 'Administrator', 'administrator', 'c4ca4238a0b923820dcc509a6f75849b', 'yhouga.13@gmail.com', 'a', 'a', 'a', 'Administrator', 'a', '1', '1', 1, 1, 1, '2019-09-11 00:00:00'),
(9, 'ATIC-9', 'Yh', 'username1', '2222b0104b5621b7a68474f2741bcbf1', 'yhouga.13@gmail.com', 'Malang', '13/06/1995', 'Ala', 'Front End Programmer', 'Islam', 'L', 'Belum Menikah', 213321, 1, 2, '2019-09-25 19:29:51');

-- --------------------------------------------------------

--
-- Table structure for table `tblpegawai_temp`
--

CREATE TABLE `tblpegawai_temp` (
  `id` int(11) NOT NULL,
  `nik` int(11) NOT NULL,
  `nama_pegawai` varchar(255) NOT NULL,
  `alamat` varchar(500) NOT NULL,
  `jabatan` varchar(255) NOT NULL,
  `tempat_lahir` varchar(255) NOT NULL,
  `tanggal_lahir` varchar(25) NOT NULL,
  `status` tinyint(1) DEFAULT '1',
  `dibuat_oleh` varchar(25) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AVG_ROW_LENGTH=8192 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tblproyek`
--

CREATE TABLE `tblproyek` (
  `id` int(11) NOT NULL,
  `id_proyek` varchar(255) NOT NULL,
  `nama_proyek` varchar(500) NOT NULL,
  `customer` varchar(255) NOT NULL,
  `report_user` varchar(255) NOT NULL,
  `flagStatus` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `start_proyek` date NOT NULL,
  `end_proyek` date NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblproyek`
--

INSERT INTO `tblproyek` (`id`, `id_proyek`, `nama_proyek`, `customer`, `report_user`, `flagStatus`, `status`, `start_proyek`, `end_proyek`, `created_at`) VALUES
(1, 'ATIC-PROJEK-1', 'Casa', 'Bank Mandiri', '4', 1, 1, '2019-09-03', '2019-09-19', '2019-09-18 18:19:24'),
(2, 'ATIC-PROJEK-2', 'Pocket Bank', 'Mandiri', '5', 1, 1, '2019-09-08', '2019-09-28', '2019-09-29 13:28:06'),
(3, 'ATIC-PROJEK-3', 'Omnichannel', 'Mandiri', '4', 1, 1, '2019-09-01', '2019-09-28', '2019-09-29 14:45:48');

-- --------------------------------------------------------

--
-- Table structure for table `tbltimesheet_day`
--

CREATE TABLE `tbltimesheet_day` (
  `id` int(11) NOT NULL,
  `id_timesheet_week` int(11) NOT NULL,
  `id_pegawai` int(11) NOT NULL,
  `id_project` int(11) NOT NULL,
  `category` int(11) NOT NULL,
  `stage` varchar(225) NOT NULL,
  `date` date NOT NULL,
  `task` text NOT NULL,
  `output` text NOT NULL,
  `start_time` varchar(225) NOT NULL,
  `end_time` varchar(255) NOT NULL,
  `total_hours` varchar(225) NOT NULL,
  `status` int(11) NOT NULL,
  `reason` varchar(225) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbltimesheet_day`
--

INSERT INTO `tbltimesheet_day` (`id`, `id_timesheet_week`, `id_pegawai`, `id_project`, `category`, `stage`, `date`, `task`, `output`, `start_time`, `end_time`, `total_hours`, `status`, `reason`, `created_at`, `last_updated`) VALUES
(1, 1, 7, 3, 1, '2', '2019-09-18', 'updated 2', 'updated ', '09:00', '19:00', '10', 2, 'tidak sama\n', '2019-09-22 19:24:47', '2019-09-30 10:42:29'),
(2, 1, 7, 3, 1, '2', '2019-09-16', 'asd', 'asd', '08:00', '17:00', '9', 2, '', '2019-09-22 12:13:13', '2019-09-30 10:23:24'),
(3, 2, 7, 3, 1, '1', '2019-09-24', 'asd', 'asd', '06:00', '15:00', '9', 2, '', '2019-09-30 08:20:42', '2019-09-30 10:44:17'),
(4, 2, 7, 1, 1, '2', '2019-09-24', 'asd', 'asd', '09:00', '18:00', '9', 1, '', '2019-09-30 08:28:25', '2019-09-30 08:28:29');

-- --------------------------------------------------------

--
-- Table structure for table `tbltimesheet_week`
--

CREATE TABLE `tbltimesheet_week` (
  `id` int(11) NOT NULL,
  `id_pegawai` int(11) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `last_updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbltimesheet_week`
--

INSERT INTO `tbltimesheet_week` (`id`, `id_pegawai`, `start_date`, `end_date`, `status`, `created_at`, `last_updated`) VALUES
(1, 7, '2019-09-16', '2019-09-22', 1, '2019-09-21 00:00:00', '2019-09-22 19:46:36'),
(2, 7, '2019-09-23', '2019-09-29', 1, '2019-09-24 17:23:25', '2019-09-30 08:28:29'),
(3, 7, '2019-09-30', '2019-10-06', 0, '2019-09-30 15:36:04', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activity_log`
--
ALTER TABLE `activity_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblcuti`
--
ALTER TABLE `tblcuti`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbljabatan`
--
ALTER TABLE `tbljabatan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblpegawai`
--
ALTER TABLE `tblpegawai`
  ADD PRIMARY KEY (`id_pegawai`);

--
-- Indexes for table `tblpegawai_temp`
--
ALTER TABLE `tblpegawai_temp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblproyek`
--
ALTER TABLE `tblproyek`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbltimesheet_day`
--
ALTER TABLE `tbltimesheet_day`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbltimesheet_week`
--
ALTER TABLE `tbltimesheet_week`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activity_log`
--
ALTER TABLE `activity_log`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tblcuti`
--
ALTER TABLE `tblcuti`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbljabatan`
--
ALTER TABLE `tbljabatan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tblpegawai`
--
ALTER TABLE `tblpegawai`
  MODIFY `id_pegawai` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tblpegawai_temp`
--
ALTER TABLE `tblpegawai_temp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblproyek`
--
ALTER TABLE `tblproyek`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbltimesheet_day`
--
ALTER TABLE `tbltimesheet_day`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbltimesheet_week`
--
ALTER TABLE `tbltimesheet_week`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
