-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 14, 2019 at 04:45 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.1.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbapp`
--

-- --------------------------------------------------------

--
-- Table structure for table `activity_log`
--

CREATE TABLE `activity_log` (
  `id` bigint(20) NOT NULL,
  `fk_user_id` varchar(255) DEFAULT NULL,
  `activity` varchar(1000) DEFAULT NULL,
  `module` varchar(1000) DEFAULT NULL,
  `created_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `activity_log`
--

INSERT INTO `activity_log` (`id`, `fk_user_id`, `activity`, `module`, `created_at`) VALUES
(1, NULL, 'reset user yhouga.13@gmail.com`s password', 'User Management', '2019-08-29');

-- --------------------------------------------------------

--
-- Table structure for table `tbljabatan`
--

CREATE TABLE `tbljabatan` (
  `id` int(11) NOT NULL,
  `jabatan` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbljabatan`
--

INSERT INTO `tbljabatan` (`id`, `jabatan`) VALUES
(1, 'Programmer (Front End)'),
(2, 'Programmer (Back End)'),
(3, 'Business Analyst'),
(4, 'Tester');

-- --------------------------------------------------------

--
-- Table structure for table `tblpegawai`
--

CREATE TABLE `tblpegawai` (
  `id_pegawai` int(11) NOT NULL,
  `nik` varchar(255) NOT NULL,
  `nama_pegawai` varchar(500) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `tempat_lahir` varchar(255) NOT NULL,
  `tanggal_lahir` varchar(255) DEFAULT NULL,
  `alamat` varchar(500) NOT NULL,
  `jabatan` varchar(255) NOT NULL,
  `agama` varchar(255) NOT NULL,
  `jenis_kelamin` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `no_telepon` int(11) NOT NULL,
  `updatedStat` tinyint(4) NOT NULL,
  `roleid` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblpegawai`
--

INSERT INTO `tblpegawai` (`id_pegawai`, `nik`, `nama_pegawai`, `username`, `password`, `email`, `tempat_lahir`, `tanggal_lahir`, `alamat`, `jabatan`, `agama`, `jenis_kelamin`, `status`, `no_telepon`, `updatedStat`, `roleid`, `created_at`) VALUES
(1, 'ATIC-1', 'yhouga beta evantio update 5', 'yhouga13update5', '9315de29b738d191c4821ab2af72a30e', 'yhouga.13@gmail.com', 'Blitar', '13/06/1995', 'jl. alamat2', 'Programmer', 'Islam', 'L', 'Menikah', 2147483647, 1, 2, '2019-09-14 12:07:59'),
(2, 'ATIC-2', 'nama lengkap pegawai 2', 'username2', 'fdf65f9ebc8ff131074ad20499655755', 'yhouga.13@gmail.com', 'tempat2', '13/06/1995', 'alamat2', 'Programmer', 'Islam', 'P', 'Belum Menikah', 1231234, 0, 2, '2019-09-14 12:36:04');

-- --------------------------------------------------------

--
-- Table structure for table `tblpegawai_temp`
--

CREATE TABLE `tblpegawai_temp` (
  `id` int(11) NOT NULL,
  `nik` int(11) NOT NULL,
  `nama_pegawai` varchar(255) NOT NULL,
  `alamat` varchar(500) NOT NULL,
  `jabatan` varchar(255) NOT NULL,
  `tempat_lahir` varchar(255) NOT NULL,
  `tanggal_lahir` varchar(25) NOT NULL,
  `status` tinyint(1) DEFAULT '1',
  `dibuat_oleh` varchar(25) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AVG_ROW_LENGTH=8192 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tblproyek`
--

CREATE TABLE `tblproyek` (
  `id` int(11) NOT NULL,
  `id_proyek` varchar(255) NOT NULL,
  `nama_proyek` varchar(500) NOT NULL,
  `customer` varchar(255) NOT NULL,
  `report_user` varchar(255) NOT NULL,
  `flagStatus` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblproyek`
--

INSERT INTO `tblproyek` (`id`, `id_proyek`, `nama_proyek`, `customer`, `report_user`, `flagStatus`, `status`, `created_at`) VALUES
(1, '1', 'test', 'test', 'test', 0, 0, '2019-07-28 17:30:06'),
(2, '1', 'atic', 'atic', 'atic', 1, 0, '2019-09-03 19:07:40'),
(3, 'a', 'a', 'a', 'a', 1, 0, '2019-09-03 19:07:59');

-- --------------------------------------------------------

--
-- Table structure for table `tbluser`
--

CREATE TABLE `tbluser` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `fullname` varchar(50) DEFAULT NULL,
  `roleid` varchar(25) NOT NULL,
  `status` tinyint(1) DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AVG_ROW_LENGTH=8192 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbluser`
--

INSERT INTO `tbluser` (`id`, `username`, `password`, `email`, `fullname`, `roleid`, `status`, `created_at`, `updated_at`) VALUES
(1, 'administrator', 'c4ca4238a0b923820dcc509a6f75849b', 'yhouga.13@gmail.com', 'administrator', '1', 1, '2017-01-12 12:07:57', '2019-08-29 17:03:06'),
(37, 'yhouga13update5', 'cf2fdb3a4006bf99026415f48d0214fd', 'yhouga.13@gmail.com', 'yhouga beta evantio update 5', '2', 1, '2019-09-14 12:07:59', '2019-09-14 14:40:33'),
(38, 'username2', 'fdf65f9ebc8ff131074ad20499655755', 'yhouga.13@gmail.com', 'nama lengkap pegawai 2', '2', 0, '2019-09-14 12:36:04', '2019-09-14 14:32:29');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activity_log`
--
ALTER TABLE `activity_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbljabatan`
--
ALTER TABLE `tbljabatan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblpegawai`
--
ALTER TABLE `tblpegawai`
  ADD PRIMARY KEY (`id_pegawai`);

--
-- Indexes for table `tblpegawai_temp`
--
ALTER TABLE `tblpegawai_temp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblproyek`
--
ALTER TABLE `tblproyek`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbluser`
--
ALTER TABLE `tbluser`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activity_log`
--
ALTER TABLE `activity_log`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbljabatan`
--
ALTER TABLE `tbljabatan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tblpegawai`
--
ALTER TABLE `tblpegawai`
  MODIFY `id_pegawai` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tblpegawai_temp`
--
ALTER TABLE `tblpegawai_temp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblproyek`
--
ALTER TABLE `tblproyek`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbluser`
--
ALTER TABLE `tbluser`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
