-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 05, 2019 at 02:20 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbapp`
--

-- --------------------------------------------------------

--
-- Table structure for table `tblpegawai`
--

CREATE TABLE `tblpegawai` (
  `id_pegawai` int(11) NOT NULL,
  `nik` varchar(255) NOT NULL,
  `nama_pegawai` varchar(500) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `tempat_lahir` varchar(255) NOT NULL,
  `tanggal_lahir` varchar(255) DEFAULT NULL,
  `alamat` varchar(500) NOT NULL,
  `jabatan` varchar(255) NOT NULL,
  `agama` varchar(255) NOT NULL,
  `jenis_kelamin` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `no_telepon` int(11) NOT NULL,
  `updatedStat` tinyint(4) NOT NULL,
  `roleid` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblpegawai_temp`
--

CREATE TABLE `tblpegawai_temp` (
  `id` int(11) NOT NULL,
  `nik` int(11) NOT NULL,
  `nama_pegawai` varchar(255) NOT NULL,
  `alamat` varchar(500) NOT NULL,
  `jabatan` varchar(255) NOT NULL,
  `tempat_lahir` varchar(255) NOT NULL,
  `tanggal_lahir` varchar(25) NOT NULL,
  `status` tinyint(1) DEFAULT '1',
  `dibuat_oleh` varchar(25) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AVG_ROW_LENGTH=8192 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tblproyek`
--

CREATE TABLE `tblproyek` (
  `id` int(11) NOT NULL,
  `id_proyek` varchar(255) NOT NULL,
  `nama_proyek` varchar(500) NOT NULL,
  `customer` varchar(255) NOT NULL,
  `report_user` varchar(255) NOT NULL,
  `flagStatus` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblproyek`
--

INSERT INTO `tblproyek` (`id`, `id_proyek`, `nama_proyek`, `customer`, `report_user`, `flagStatus`, `status`, `created_at`) VALUES
(1, '1', 'test', 'test', 'test', 0, 0, '2019-07-28 17:30:06');

-- --------------------------------------------------------

--
-- Table structure for table `tbluser`
--

CREATE TABLE `tbluser` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `fullname` varchar(50) DEFAULT NULL,
  `roleid` varchar(25) NOT NULL,
  `status` tinyint(1) DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AVG_ROW_LENGTH=8192 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbluser`
--

INSERT INTO `tbluser` (`id`, `username`, `password`, `email`, `fullname`, `roleid`, `status`, `created_at`, `updated_at`) VALUES
(1, 'administrator', 'c4ca4238a0b923820dcc509a6f75849b', 'administrator@admin.com', 'administrator', '1', 1, '2017-01-12 12:07:57', '2017-01-12 05:07:59'),
(2, 'user', 'c4ca4238a0b923820dcc509a6f75849b', 'user@user.com', 'user', '1', 1, '2017-01-12 04:14:51', '2017-01-11 21:23:26'),
(18, 'a', '0cc175b9c0f1b6a831c399e269772661', 'shintiayuliang@gmail.com', 'a', '1', 0, '2019-07-17 15:09:57', '2019-07-17 15:22:24'),
(19, 's', '03c7c0ace395d80182db07ae2c30f034', 'shintiayuliang@gmail.com', 's', '1', 0, '2019-07-17 15:10:42', '2019-07-17 15:22:31'),
(20, 'e', 'e1671797c52e15f763380b45e841ec32', 'shintiayuliang@gmail.com', 'e', '1', 0, '2019-07-17 15:12:54', '2019-07-17 15:22:28'),
(21, 'SuperUser1', '282984faf9ac4b7200897a4dfaf29044', 'shintiayuliang@gmail.com', 'tes', '2', 1, '2019-07-28 17:32:25', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tblpegawai`
--
ALTER TABLE `tblpegawai`
  ADD PRIMARY KEY (`id_pegawai`);

--
-- Indexes for table `tblpegawai_temp`
--
ALTER TABLE `tblpegawai_temp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblproyek`
--
ALTER TABLE `tblproyek`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbluser`
--
ALTER TABLE `tbluser`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tblpegawai`
--
ALTER TABLE `tblpegawai`
  MODIFY `id_pegawai` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tblpegawai_temp`
--
ALTER TABLE `tblpegawai_temp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tblproyek`
--
ALTER TABLE `tblproyek`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbluser`
--
ALTER TABLE `tbluser`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
