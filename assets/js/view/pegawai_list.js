    
    window.onload = hideErrorMessages();

    function hideErrorMessages(){
        $("#error_nik").hide();
        $("#error_nik2").hide();
        $("#error_nama").hide();
        $("#error_nama2").hide();
        $("#error_username").hide();
        $("#error_username2").hide();
        $("#error_email").hide();
        $("#error_email2").hide();
        $("#error_email3").hide();
        $("#error_tempatLahir").hide();
        $("#error_tgllahir").hide();
        $("#error_alamat").hide();
        $("#error_jabatan").hide();
        $("#error_agama").hide();
        $("#error_jenisKelamin").hide();
        $("#error_status").hide();
        $("#error_noTelp").hide();
        $("#error_role").hide();
        $("#edit-error_nik").hide();
        $("#edit-error_nik2").hide();
        $("#edit-error_nama").hide();
        $("#edit-error_nama2").hide();
        $("#edit-error_username").hide();
        $("#edit-error_username2").hide();
        $("#edit-error_email").hide();
        $("#edit-error_email2").hide();
        $("#edit-error_email3").hide();
        $("#edit-error_tempatLahir").hide();
        $("#edit-error_tgllahir").hide();
        $("#edit-error_alamat").hide();
        $("#edit-error_jabatan").hide();
        $("#edit-error_agama").hide();
        $("#edit-error_jenisKelamin").hide();
        $("#edit-error_status").hide();
        $("#edit-error_noTelp").hide();
        $("#edit-error_role").hide();
        // $("#edit-error_email").hide();
        // $("#edit-error_email2").hide();
        // $("#edit-error_email3").hide();
        // $("#edit-error_username").hide();
        // $("#edit-error_username2").hide();
        // $("#edit-error_fullname").hide();
        // $("#edit-error_fullname2").hide();
        // $("#edit-error_roleid").hide();
        hide_loading();
    }

    $(document).ready( function () {

        //$('#dataTables-user-log').DataTable();
        $('#dataTables-pegawai-list').DataTable({
            "bFilter": true,
            "paging":   false,
            //"iDisplayLength": 20,
            "order": [[ 0, "asc" ]]
            //"bDestroy": true,
        });
     } );

    function edit_pegawai_popup(nik,nama_pegawai,username,email,tempatLahir,tgllahir,alamat,jabatan,agama,jenisKelamin,status,noTelp,roleid){
        $( "#edit-nik" ).val(nik);
        $( "#edit-nama_pegawai" ).val(nama_pegawai);
        $( "#edit-username" ).val(username);
        $( "#edit-email" ).val(email);
        $( "#edit-tempatLahir" ).val(tempatLahir);
        $( "#edit-tgllahir" ).val(tgllahir);
        $( "#edit-alamat" ).val(alamat);
        $( "#edit-jabatan" ).val(jabatan);
        $( "#edit-agama" ).val(agama);
        $( "#edit-jenisKelamin" ).val(jenisKelamin);
        $( "#edit-status" ).val(status);
        $( "#edit-noTelp" ).val(noTelp);
        $( "#edit-roleid" ).val(roleid);
        if(roleid=='1')
            roleOption = "<option value='1' selected>Admin</option><option value='2'>Pegawai</option><option value='3'>Approval</option>";
        if(roleid=='2')
            roleOption = "<option value='1'>Admin</option><option value='2' selected>Pegawai</option><option value='3'>Approval</option>";
        if(roleid=='3')
            roleOption = "<option value='1'>Admin</option><option value='2' >Pegawai</option><option value='3' selected>Approval</option>";

        $( "#edit-roleid" ).html(roleOption);
        console.log(username); 
        $('#editPegawaiSubmit').attr('onClick','update_pegawai_details("'+nik+'")');
    }

    function deactivate_confirmation(username,id){
        $( "#user-username" ).html(username);
        $('#deactivateYesButton').attr("onclick","deactivate_submit('"+username+"',"+id+")");
    }

    function reset_confirmation(email,id){
        $( "#reset-user-email" ).html(email);
        $('#resetYesButton').attr("onclick","reset_submit('"+email+"',"+id+")");
    }

    function deactivate_submit(username,id){
        show_loading();
            $.ajax({
            url: $("#base-url").val()+"admin/deactivate_user/"+username+"/"+id,
            cache: false,
            success: function (hasil) {
                var result = JSON.parse(hasil);
                if(result.status=="success"){
                    location.reload();
                }
                else{
                    alert("Oops there is something wrong!");
                }
            },
            error: ajax_error_handling
        });
    }

    function reset_submit(email,id){
        show_loading();
            $.ajax({
            url: $("#base-url").val()+"admin/reset_user_password/"+email+"/"+id,
            cache: false,
            success: function (result) {
                var result = $.parseJSON(result);
                if(result.status=='success'){
                    location.reload();
                }
                else{
                    alert("Oops there is something wrong!");
                }
            },
            error: ajax_error_handling
        });
    }

    function update_pegawai_details(username){
        
        console.log('aaaa'+username);

        hideErrorMessages();
        show_loading();
        var i=0;
        var nik = $('#edit-nik').val();
        var nama_pegawai = $('#edit-nama_pegawai').val();
        var username = $('#edit-username').val();
        var email = $('#edit-email').val();
        var tempatLahir = $('#edit-tempatLahir').val();
        var tgllahir = $('#edit-tgllahir').val();
        var alamat = $('#edit-alamat').val();
        var jabatan = $('#edit-jabatan').val();
        var agama = $('#edit-agama').val();
        var jenisKelamin = $('#edit-jenisKelamin').val();
        var status = $('#edit-status').val();
        var noTelp = $('#edit-noTelp').val();
        var roleid = $('#edit-roleid').val();

        if(nik == ""){
            $("#edit-error_nik").show();
            i++;
        }
        else if (!nik.match(/^[0-9]+$/)) {
            $("#edit-error_nik2").show();
            i++;
        }

        if(nama_pegawai == ""){
            $("#edit-error_nama").show();
            i++;
        }else if (!nama_pegawai.match(/^[A-Za-z0-9\s]+$/)) {
            $("#edit-error_nama2").show();
            i++;
        }
        if(username == ""){
            $("#edit-error_username").show();
            i++;
        }else if (!username.match(/^[A-Za-z0-9\s]+$/)) {
            $("#edit-error_username2").show();
            i++;
        }
        if(email == ""){
            $("#edit-error_email").show();
            i++;
        }
        else if (!email.match(/^[\w -._]+@[\-0-9a-zA-Z_.]+?\.[a-zA-Z]{2,3}$/)) {
            $("#edit-error_email3").show();
            i++;
        }

        if(tempatLahir == ""){
            $("#edit-error_tempatLahir").show();
            i++;
        }
        if(tgllahir == ""){
            $("#edit-error_tgllahir").show();
            i++;
        }
        if(alamat == ""){
            $("#edit-error_alamat").show();
            i++;
        }
        if(jabatan == ""){
            $("#edit-error_jabatan").show();
            i++;
        }
        if(agama == ""){
            $("#edit-error_agama").show();
            i++;
        }
        if(jenisKelamin == ""){
            $("#edit-error_jenisKelamin").show();
            i++;
        }
        if(status == ""){
            $("#edit-error_status").show();
            i++;
        }
        if(noTelp == ""){
            $("#edit-error_noTelp").show();
            i++;
        }
        if(roleid == 0){
            $("#edit-error_role").show();
            i++;
        }


        if(i == 0){
            $.ajax({
                url: $("#base-url").val()+"pegawai/update_pegawai_details/",
                traditional: true,
                type: "post",
                dataType: "text",
                // data: {email: email, id:id, name:name, role:role},
                data: {sendData : JSON.stringify({nik:nik,nama_pegawai:nama_pegawai, username:username, email:email
                    , tempatLahir:tempatLahir, tgllahir:tgllahir, jabatan:jabatan,agama:agama,alamat:alamat, jenisKelamin:jenisKelamin, status:status, noTelp:noTelp, roleid:roleid})},
                success: function (hasil) {
                    // var result = $.parseJSON(result);

                    var result = JSON.parse(hasil);
                    if(result.status=="success"){
                        location.reload();
                    }
                    else{
                        alert("Oops there is something wrong!");
                    }
                },
                error: ajax_error_handling
            });
        }
    }






    $( "#newPegawaiSubmit" ).click(function() {
        hideErrorMessages();
        show_loading();
        var i=0;
        var nik = $('#nik').val();
        var nama_pegawai = $('#nama_pegawai').val();
        var username = $('#username').val();
        var email = $('#email').val();
        var tempatLahir = $('#tempatLahir').val();
        var tgllahir = $('#tgllahir').val();
        var alamat = $('#alamat').val();
        var jabatan = $('#jabatan').val();
        var agama = $('#agama').val();
        var jenisKelamin = $('#jenisKelamin').val();
        var status = $('#status').val();
        var noTelp = $('#noTelp').val();
        var roleid = $('#roleid').val();

        if(nik == ""){
            $("#error_nik").show();
            i++;
        }
        else if (!nik.match(/^[0-9]+$/)) {
            $("#error_nik2").show();
            i++;
        }

        if(nama_pegawai == ""){
            $("#error_nama").show();
            i++;
        }else if (!nama_pegawai.match(/^[A-Za-z0-9\s]+$/)) {
            $("#error_nama2").show();
            i++;
        }
        if(username == ""){
            $("#error_username").show();
            i++;
        }else if (!username.match(/^[A-Za-z0-9\s]+$/)) {
            $("#error_username2").show();
            i++;
        }
        if(email == ""){
            $("#error_email").show();
            i++;
        }
        else if (!email.match(/^[\w -._]+@[\-0-9a-zA-Z_.]+?\.[a-zA-Z]{2,3}$/)) {
            $("#error_email3").show();
            i++;
        }

        if(tempatLahir == ""){
            $("#error_tempatLahir").show();
            i++;
        }
        if(tgllahir == ""){
            $("#error_tgllahir").show();
            i++;
        }
        if(alamat == ""){
            $("#error_alamat").show();
            i++;
        }
        if(jabatan == ""){
            $("#error_jabatan").show();
            i++;
        }
        if(agama == ""){
            $("#error_agama").show();
            i++;
        }
        if(jenisKelamin == ""){
            $("#error_jenisKelamin").show();
            i++;
        }
        if(status == ""){
            $("#error_status").show();
            i++;
        }
        if(noTelp == ""){
            $("#error_noTelp").show();
            i++;
        }
        if(roleid == 0){
            $("#error_role").show();
            i++;
        }


        if(i == 0){
            // var serial = serialize 
            $.ajax({
                url: $("#base-url").val() + "pegawai/add_pegawai",
                traditional: true,
                type: "post",
                dataType: "text",
                data: {sendData : JSON.stringify({nik:nik,nama_pegawai:nama_pegawai, username:username, email:email
                    , tempatLahir:tempatLahir, tgllahir:tgllahir, jabatan:jabatan,agama:agama,alamat:alamat, jenisKelamin:jenisKelamin, status:status, noTelp:noTelp, roleid:roleid})},
                success: function (hasil) { 
                    var result = JSON.parse(hasil);
                    if(result.status=="success"){
                        location.reload();
                    }
                    else{
                        alert("Oops there is something wrong!");
                    }
                  
                },
                error: ajax_error_handling
            });
        }else{
            hide_loading();
        }
            
    });


